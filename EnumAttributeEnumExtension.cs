﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Rozszerzenie klasy EnumAttribute - metody pobierające wartości atrybutu
	/// </summary>
	public static class EnumAttributeEnumExtension
	{
		private static Dictionary<Enum,EnumAttribute> atrybuty = new Dictionary<Enum, EnumAttribute>();


		/// <summary>
		/// Metoda zwraca obiekt atrybutu ustawionego dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static EnumAttribute GetEnumAttribute(this Enum value)
		{
			if (!EnumAttributeEnumExtension.atrybuty.ContainsKey(value))
			{
				var type = value.GetType();
				var name = Enum.GetName(type, value);
				EnumAttributeEnumExtension.atrybuty.Add(value, type.GetField(name).GetCustomAttributes(false).OfType<EnumAttribute>().SingleOrDefault());
			}

			return EnumAttributeEnumExtension.atrybuty[value];
		}

		/// <summary>
		/// Metoda zwraca kod z atrybutu ustawionego dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetKod(this Enum value)
		{
			var enumAttribute = value.GetEnumAttribute();

			if (enumAttribute != null)
			{
				return enumAttribute.Kod;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Metoda zwraca nazwę z atrybutu ustawionego dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetNazwa(this Enum value)
		{
			var enumAttribute = value.GetEnumAttribute();

			if (enumAttribute != null)
			{
				return enumAttribute.Nazwa;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Metoda zwraca opis z atrybutu ustawionego dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetOpis(this Enum value)
		{
			var enumAttribute = value.GetEnumAttribute();

			if (enumAttribute != null)
			{
				return enumAttribute.Opis;
			}
			else
			{
				return null;
			}
		}
	}
}
