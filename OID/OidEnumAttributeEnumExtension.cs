﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Klasa rozszerzeń dla wyliczeń OID
	/// </summary>
	public static class OidEnumAttributeEnumExtension
	{

		/// <summary>
		/// Metoda zwraca obiekt atrybutu ustawionego dla wyliczenia 
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		internal static OidEnumAttribute GetOidEnumAttribute(this Enum value)
		{
			var type = value.GetType();
			var name = Enum.GetName(type, value);
			return type.GetField(name)
					.GetCustomAttributes(false)
					.OfType<OidEnumAttribute>()
					.SingleOrDefault();
		}

		/// <summary>
		/// Metoda zwraca nadzbiór wartości z atrybutu ustawionego dla wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static NadzbiorZbioruWartosci? GetNadzbiorZbioruWartosci(this Enum value)
		{
			return value.GetNadzbiorZbioruWartosci(false);
		}

		/// <summary>
		/// Metoda zwraca nadzbiór wartości z atrybutu ustawionego dla wyliczenia, parametr returnTopMostElement decyduje o ...?...
		/// </summary>
		/// <param name="value"></param>
		/// <param name="returnTopMostElement"></param>
		/// <returns></returns>
		public static NadzbiorZbioruWartosci? GetNadzbiorZbioruWartosci(this Enum value, bool returnTopMostElement)
		{
			var enumAttribute = value.GetOidEnumAttribute();

			NadzbiorZbioruWartosci? returnValue = null;

			if (enumAttribute != null && enumAttribute is EnumAttribute)
			{
				returnValue = enumAttribute.NadzbioruWartosci;

				if (returnValue.HasValue && returnTopMostElement)
				{
					returnValue = NadzbiorZbioruWartosciHelper.GetTopMostElement(returnValue.Value);
				}
			}

			return returnValue;
		}

		public static bool CzyJestZZakresu(this Enum value, NadzbiorZbioruWartosci nadzbiorZbioruWartosci)
		{
			var retVal = false;

			//var kodPoli = value.GetKod();
			//var kodZakresu = zakresPoliWartosci.GetKod();

			//if (kodPoli != null && kodZakresu != null)
			//{
			//  retVal = string.Compare(kodPoli.Substring(0, kodZakresu.Length), kodZakresu) == 0;
			//}

			var valueOidEnumAttribute = value.GetOidEnumAttribute();
			if (valueOidEnumAttribute != null)
			{
				var valueZakresPoliWartosci = valueOidEnumAttribute.NadzbioruWartosci;
				if (valueZakresPoliWartosci.HasValue)
				{
					if (valueZakresPoliWartosci == nadzbiorZbioruWartosci)
					{
						retVal = true;
					}
					else
					{
						return nadzbiorZbioruWartosci.CzyJestZZakresu(nadzbiorZbioruWartosci);
					}
				}
			}

			return retVal;
		}

		public static bool CzyJestZZakresu(this Enum value, NadzbiorZbioruWartosci[] nadzbioryZbioruWartosci)
		{
			var retVal = false;

			foreach (var ajtem in nadzbioryZbioruWartosci)
			{
				if (value.CzyJestZZakresu(ajtem))
				{
					retVal = true;
					break;
				}
			}

			return retVal;
		}
	}
}
