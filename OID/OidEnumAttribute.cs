﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.Common;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Klasa atrybutu dla wyliczenia jako całości - pozwala powiązać typ wyliczeniowy ze słownikiem CSIOZ (zbiorem wartości i/lub systemem kodowania)
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum, AllowMultiple = false)]
	public class OidEnumAttribute : EnumAttribute
	{
		/// <summary>
		/// Nadzbior wartości - identyfikator w wykazie węzłów w drzewie OID
		/// </summary>
		internal NadzbiorZbioruWartosci? NadzbioruWartosci { get; private set; }
		
		/// <summary>
		/// System kodowania - identyfikator z listy słowników utworzonych celowo na potrzeby IG
		/// </summary>
		internal SystemKodowania? SystemKodowania { get; private set; }



		internal OidEnumAttribute(string root) : base(root, null, null) { }

		internal OidEnumAttribute(string root, string nazwa) : base(root, nazwa, nazwa) { }

		internal OidEnumAttribute(NadzbiorZbioruWartosci nadzbiorZbioruWartosci, SystemKodowania systemKodowania, string root, string nazwa)
			: this(nadzbiorZbioruWartosci, !string.IsNullOrEmpty(root) ? root : systemKodowania.GetKod(), nazwa, nazwa)
		{
			this.SystemKodowania = systemKodowania;
		}

		internal OidEnumAttribute(NadzbiorZbioruWartosci nadzbiorZbioruWartosci, SystemKodowania systemKodowania, string root, string nazwa, string opis)
			: this(nadzbiorZbioruWartosci, root, nazwa, opis)
		{
			this.SystemKodowania = systemKodowania;
		}


		internal OidEnumAttribute(SystemKodowania systemKodowania, string root, string nazwa)
			: this(root, nazwa, nazwa)
		{
			this.SystemKodowania = systemKodowania;
		}

		internal OidEnumAttribute(SystemKodowania systemKodowania, string root, string nazwa, string opis)
			: this(root, nazwa, opis)
		{
			this.SystemKodowania = systemKodowania;
		}

		internal OidEnumAttribute(SystemKodowania systemKodowania, ZbiorWartosci zbiorWartosci)
			: this(zbiorWartosci.GetKod(), zbiorWartosci.GetNazwa(), zbiorWartosci.GetOpis())
		{
			this.SystemKodowania = systemKodowania;
		}

		//internal OidEnumAttribute(string kod, string nazwaKoduSystemu, string nazwa) : base(kod, nazwaKoduSystemu, nazwa) { }
		internal OidEnumAttribute(string root, string nazwa, string opis) : base(root, nazwa, opis) { }

		//internal OidEnumAttribute(ZakresPoliWartosci zakresPoliWartosci, string kod) : this(zakresPoliWartosci, kod, null, null) { }

		internal OidEnumAttribute(NadzbiorZbioruWartosci nadzbiorZbioruWartosci, string root, string nazwa) : this(nadzbiorZbioruWartosci, root, nazwa, nazwa) { }

		internal OidEnumAttribute(NadzbiorZbioruWartosci nadzbiorZbioruWartosci, string root, string nazwa, string opis)
			: base(root, nazwa, opis)
		{
			this.NadzbioruWartosci = nadzbiorZbioruWartosci;

			var kodZaresu = nadzbiorZbioruWartosci.GetKod();
			//if (string.Compare(root.Substring(0, kodZaresu.Length), kodZaresu) != 0) // tego typu kontrola chyba nie jest dobry pomysłem ze względu na wyjątki 
			//{
			//  throw new ErrorException("Błędy kod zbioru (póli) identyfikatorów! Podany kod musi zawierać się w kodzie zakresu!");
			//}

			var parentWszystkichParentow = nadzbiorZbioruWartosci.GetNadzbiorZbioruWartosci(true);
			if (parentWszystkichParentow.HasValue && parentWszystkichParentow.Value == OID.NadzbiorZbioruWartosci.WezelUslugodawcy)
			{
				this.Kod = this.Kod.Replace("{x}", Library.NumerKontaP1Swiadczeniodawcy);
			}
		}
	}
}
