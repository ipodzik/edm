﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	[HL7XmlName("id")]
	public class IdentyfikatorPacjenta : IdentyfikatorInstancji
	{
		public ZbiorWartosci? ZbiorWartosci
		{
			get { return this.zbiorWartosci; }
			//set { this.zbiorWartosci = value; }
		}



		public IdentyfikatorPacjenta() : base(NadzbiorZbioruWartosci.IdentyfikatoryOsob, true) { }



		public void UstawWartosc(ZbiorWartosci? zbiorWartosci, string wartosc)
		{
			this.UstawZbiorWartosci(zbiorWartosci);
			if (zbiorWartosci.HasValue)
			{
				this.Extension = wartosc;
			}
			else
			{
				this.Extension = null;
			}
		}
	}
}
