﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Wykaz węzłów w drzewie OID - grup zbiorów wartości
	/// </summary>
	/// <remarks>PIK HL7 CDA - zakładka identyfikatory - Rejestr OID prowadzony przez CSIOZ (arkusz xls) - wyższe poziomy drzewa</remarks>
	public enum NadzbiorZbioruWartosci
	{
		//+ Idetyfikatory osób
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.1", "Idetyfikatory osób")]
		IdentyfikatoryOsob,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryOsob, "2.16.840.1.113883.3.4424.1.1", "Krajowe idetyfikatory osób w państwach UE i strefy Schengen")]
		KrajoweIdentyfikatoryOsobUe,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.6
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryOsob, "2.16.840.1.113883.3.4424.1.6", "Numery Prawa Wykonywania Zawodu")]
		NumeryPrawaWykonywaniaZawodu,


		//+ Identyfikatory przedsiębiorstw
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.2", "Identyfikatory przedsiębiorstw")]
		IdentyfikatoryPrzedsiebiorstwa,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.3", "Numery księgi rejestrowej")]
		NumeryKsiegiRejestrowej,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.4", "Numery wpisów do rejestru realizowane przez izby lekarskie")]
		RejestrIzbyLekarskie,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.4", "Numery wpisów do rejestru realizowane przez izby lekarskie - miejsce udzielania świadczeń")]
		RejestrIzbyLekarskieMiejsceUdzielaniaSwiadczen,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.5
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.5", "Numery wpisów do rejestru realizowane przez izby pielęgniarek i położnych")]
		RejestrIzbyPielegniarekPoloznych,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.5
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.5", "Numery wpisów do rejestru realizowane przez izby pielęgniarek i położnych - miejsce udzielania świadczeń")]
		RejestrIzbyPielegniarekPoloznychMiejsceUdzielaniaSwiadczen,

		//+ Węzły nadawany usługodawcom  
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.2.7", "Węzły OID nadawane usługodawcom zgodnie z Polityką stosowania OID")]
		WezelUslugodawcy,


		//+ identyfikatory płatników 
		/// <summary>
		/// 2.16.840.1.113883.3.4424.3
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.3", "Identyfikatory płatników")]
		IdentyfikatoryPlatnikow,


		// {x} jest podmieniany na wartość Library.NumerKontaP1Swiadczeniodawcy (parametr systemu)
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}", "Węzeł OID konta usługodawcy w P1, stosowany przez usługodawcę zarejestrowanego w P1")]
		WezelKontaUslugodawcyWP1,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}.1", "Węzeł własny usługodawcy do zastosowań wewnętrznych w przypadku gdy usługodawca nie posiada własnego węzła OID")]
		WezelWlasnyUslugodawcy,
	
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1.1  EDM wytwarzana w systemie MedInf
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelWlasnyUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}.1.1", "EDM wytwarzana w systemie usługodawcy")]
		SystemUslugodawcyEDM,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1.2  węzeł na różne identyfikatory z systemu MedInf
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelWlasnyUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}.1.2", "węzeł na różne identyfikatory z systemu MedInf")]
		SystemUslugodawcyMedInf,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}.2", "Recepty wystawiane w systemie usługodawcy")]
		SystemUslugodawcyWystawioneRecepty,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.13
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}.13", "Wnioski o udostępnienie EDM wystawiane w systemie usługodawcy")]
		SystemUslugodawcyWystawioneWnioskioUdostępnienieEDM,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.14
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelUslugodawcy, "2.16.840.1.113883.3.4424.2.7.{x}.14", "Dokumenty realizacji udostępnienia EDM wystawiane w systemie usługodawcy")]
		SystemUslugodawcyRealizacjeUdostępnieniaEDM,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.6
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.6", "Leki")]
		Leki,



		/// <summary>
		/// 2.16.840.1.113883.3.4424.7
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.7", "Identyfikatory nadawane przez P1")]
		IdentyfikatoryNadawanePrzezP1,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.7.2
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.7.2", "Identyfikatory nadawane przez P1 - recepty")]
		IdentyfikatoryNadawanePrzezP1Recepty,




		/// <summary>
		/// 2.16.840.1.113883.3.4424.8
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.8", "Identyfikatory dokumentów zewnętrznych")]
		IdentyfikatoryDokumentowZewnetrznych,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryDokumentowZewnetrznych, "2.16.840.1.113883.3.4424.8.6", "Numery umów NFZ")]
		NumeryUmowZNfz,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfz, "2.16.840.1.113883.3.4424.8.6.1", "Numery umów świadczeniodawców z NFZ zawierane w oddziale")]
		NumeryUmowZNfzSwiadczeniodawcow,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfz, "2.16.840.1.113883.3.4424.8.6.2", "Numery umów lekarzy z NFZ zawierane w oddziale")]
		NumeryUmowZNfzlekarzy,



		//+ Słowniki
		[OidEnum("2.16.840.1.113883.3.4424.11", "Słowniki i zbiory wartości")]
		Slowniki,
		[OidEnum(NadzbiorZbioruWartosci.Slowniki, "2.16.840.1.113883.3.4424.11.1", "P1")]
		SlownikiP1,
		[OidEnum(NadzbiorZbioruWartosci.Slowniki, "2.16.840.1.113883.3.4424.11.2", "RSK")]
		SlownikiRSK,
		[OidEnum(NadzbiorZbioruWartosci.Slowniki, "2.16.840.1.113883.3.4424.11.3", "Zewnętrzne")]
		SlownikiZewnetrzne,



		//+ PIK 
		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.11
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.13.11", "Typy wartości")]
		PIK,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.11.5
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.13.11.5", "Systemy kodowania")]
		SystemyKodowania,


		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.11
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.PIK, "2.16.840.1.113883.3.4424.13.11", "Typy wartości")]
		TypyWartosci
	}
}
