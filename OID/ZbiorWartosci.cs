﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Wykaz zbiorów wartości używanych w IG, gdzie źródłami są akty prawne definiujące tego typu zbiory i słowniki
	/// </summary>
	/// <remarks>PIK HL7 CDA - zakładka identyfikatory - Rejestr OID prowadzony przez CSIOZ (arkusz xls) - ostatni poziom drzewa</remarks>
	public enum ZbiorWartosci
	{
		//+ Krajowe idetyfikatory osób w państwach UE i strefy Schengen
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.1.616
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.KrajoweIdentyfikatoryOsobUe, "2.16.840.1.113883.3.4424.1.1.616", "Polska", "Numer PESEL")]
		IdentyfikatoryOsobUePolska,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.1.276
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.KrajoweIdentyfikatoryOsobUe, "2.16.840.1.113883.3.4424.1.1.276", "Niemcy", "Niemiecki odpowiednik numeru PESEL")]
		IdentyfikatoryOsobUeNiemcy,


		//+ Numery Prawa Wykonywania Zawodu
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.6.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryPrawaWykonywaniaZawodu, "2.16.840.1.113883.3.4424.1.6.1", "Farmaceuci")]
		NpwzFarmaceutow,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.6.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryPrawaWykonywaniaZawodu, "2.16.840.1.113883.3.4424.1.6.2", "Lekarze, dentyści i felczerzy")]
		NpwzLekarzyDentystowFelczerow,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.6.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryPrawaWykonywaniaZawodu, "2.16.840.1.113883.3.4424.1.6.3", "Pielęgniarki i położne")]
		NpwzPielegniarekPoloznych,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.1.6.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryPrawaWykonywaniaZawodu, "2.16.840.1.113883.3.4424.1.6.4", "Diagności laboratoryjni")]
		NpwzDiagnostowLaboratoryjnych,


		//+ Numery księgi rejestrowej RPWDL
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.3.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryKsiegiRejestrowej, "2.16.840.1.113883.3.4424.2.3.1", "Podmiot", "Podmiot (cz. I kodu resortowego)")]
		PodmiotKodResortowyI,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.3.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryKsiegiRejestrowej, "2.16.840.1.113883.3.4424.2.3.2", "Jednostki organizacyjne", "Jednostki organizacyjne (cz. I i V kodu resortowego)")]
		JednostkiOrganizacyjne,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.3.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryKsiegiRejestrowej, "2.16.840.1.113883.3.4424.2.3.3", "Komórki organizacyjne", "Komórki organizacyjne (cz. I i VII kodu resortowego)")]
		KomorkiOrganizacyjne,



		//+ Identyfikatory przedsiębiorstw
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.1", "NIP")]
		Nip,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.2.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.2.1", "REGON 9 znaków")]
		Regon9,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.2.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPrzedsiebiorstwa, "2.16.840.1.113883.3.4424.2.2.2", "REGON 14 znaków")]
		Regon14,


		//+ Izby lekarskie 
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.4.52
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyLekarskie, "2.16.840.1.113883.3.4424.2.4.52", "Numery wpisów Bydgoskiej Izby Lekarskiej w Bydgoszczy")]
		BydgoszczIzbaLekarska,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.4.53
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyLekarskie, "2.16.840.1.113883.3.4424.2.4.53", "Numery wpisów Bydgoskiej Izby Lekarskiej w Gdańsku")]
		GdanskIzbaLekarska,
		//+ Izby lekarskie - miejsce wykonywania
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.4.52.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyLekarskieMiejsceUdzielaniaSwiadczen, "2.16.840.1.113883.3.4424.2.4.52.1", "Miejsca udzielania świadczeń w ramach wpisów Bydgoskiej Izby Lekarskiej w Bydgoszczy")]
		BydgoszczIzbaLekarskaMiejsceUdzielaniaSwiadczen,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.4.53.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyLekarskieMiejsceUdzielaniaSwiadczen, "2.16.840.1.113883.3.4424.2.4.53.1", "Miejsca udzielania świadczeń w ramach wpisów Okręgowej Izby Lekarskiej w Gdańsku")]
		GdanskIzbaLekarskaMiejsceUdzielaniaSwiadczen,


		//+ Izby pielęgniarek i położnych 
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.5.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyPielegniarekPoloznych, "2.16.840.1.113883.3.4424.2.5.4", "Numery wpisów Okręgowej Izby Pielęgniarek i Położnych w Bygdoszczy")]
		BydgoszczIzbaPielegniarekPoloznych,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.5.9
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyPielegniarekPoloznych, "2.16.840.1.113883.3.4424.2.5.9", "Numery wpisów Okręgowej Izby Pielęgniarek i Położnych w Gdańsku")]
		GdanskIzbaPielegniarekPoloznych,
		//+ Izby pielęgniarek i położnych - miejce wykonywania 
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.5.4.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyPielegniarekPoloznychMiejsceUdzielaniaSwiadczen, "2.16.840.1.113883.3.4424.2.5.4.1", "Miejsca udzielania świadczeń w ramach wpisów Okręgowej Izby Pielęgniarek i Położnych w Bygdoszczy")]
		BydgoszczIzbaPielegniarekPoloznychMiejsceUdzielaniaSwiadczen,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.5.9.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.RejestrIzbyPielegniarekPoloznychMiejsceUdzielaniaSwiadczen, "2.16.840.1.113883.3.4424.2.5.9.1", "Miejsca udzielania świadczeń w ramach wpisów Okręgowej Izby Pielęgniarek i Położnych w Gdańsku")]
		GdanskIzbaPielegniarekPoloznychMiejsceUdzielaniaSwiadczen,

	  //+ EDM - Elektroniczna Dokumentacja Medyczna 2.16.840.1.113883.3.4424.2.7.{x}.1.1  EDM wytwarzana w systemie MedInf
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1.1.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyEDM, "2.16.840.1.113883.3.4424.2.7.{x}.1.1.1", "Identyfikatory instancji EDM z systemu MedInf")]
		SystemUslugodawcyEDMIdentyfikatorInstancji,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1.1.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyEDM, "2.16.840.1.113883.3.4424.2.7.{x}.1.1.2 ", "Identyfikatory zbiorów wersji EDM z systemu MedInf")]
		SystemUslugodawcyEDMZbiorWersji,

		//+ 2.16.840.1.113883.3.4424.2.7.{x}.1.2  różne identyfikatory z systemu MedInf
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1.2.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyMedInf, "2.16.840.1.113883.3.4424.2.7.{x}.1.2.1", "Hospitalizacje - Identyfikatory pobytu [hospitalizacje.dbo.pobyty.ID_pobytu]")]
		SystemUslugodawcyMedInfHospitalizacjeIdPobytu,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.1.2.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyMedInf, "2.16.840.1.113883.3.4424.2.7.{x}.1.2.2", "Hospitalizacje - nr księgi głównej (prawdop. nr w księdze/nr księgi/rok z hospitalizacje.dbo.pobyty_ksiegi")]
		SystemUslugodawcyMedInfHospitalizacjeNrKsiegiGlownej,

		//+ Recepty  2.16.840.1.113883.3.4424.2.7.{x}.2 Recepty wystawiane w systemie usługodawcy
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.2.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyWystawioneRecepty, "2.16.840.1.113883.3.4424.2.7.{x}.2.1", "Identyfikatory instancji recept")]
		ReceptaInstancja,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.2.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyWystawioneRecepty, "2.16.840.1.113883.3.4424.2.7.{x}.2.2", "Identyfikatory zbiorów wersji recept i dokumentów anulowania recepty")]
		ReceptaZbiorWersjiReceptIDokumentowAnulowania,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.2.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyWystawioneRecepty, "2.16.840.1.113883.3.4424.2.7.{x}.2.3", "Identyfikatory pozycji recepty")]
		ReceptaPozycja,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.2.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyWystawioneRecepty, "2.16.840.1.113883.3.4424.2.7.{x}.2.4", "Identyfikatory sekcji zalecenia leku w recepcie")]
		ReceptySekcjaZleceniaLeku,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.2.9
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyWystawioneRecepty, "2.16.840.1.113883.3.4424.2.7.{x}.2.9", "Identyfikatory instancji dokumentów anulowania recepty")]
		ReceptaInstancjaDokumentowAnulowania,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.13.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyWystawioneWnioskioUdostępnienieEDM, "2.16.840.1.113883.3.4424.2.7.{x}.13.1", "Identyfikatory instancji wniosków o udostępnienie EDM")]
		SystemUslugodawcyIdentyfikatoryInstancjiWnioskowoUdostepnienieEDM,
	
		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.14.1	
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SystemUslugodawcyRealizacjeUdostępnieniaEDM, "2.16.840.1.113883.3.4424.2.7.{x}.14.1", "Identyfikatory instancji dokumentów realizacji udostępnienia EDM")]
		SystemUslugodawcyIdentyfikatorInstancjiRealizacjiUdostępnieniaEDM,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.2.7.{x}.17.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.WezelKontaUslugodawcyWP1, "2.16.840.1.113883.3.4424.2.7.{x}.17.1", "Identyfikator pacjenta w systemie usługodawcy")]
		SystemUslugodawcyIdentyfikatorPacjenta,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.3.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryPlatnikow, "2.16.840.1.113883.3.4424.3.1", "Podmioty zobowiązane do finansowania świadczeń ze środków publicznych")]
		PodmiotFinansujacyZeSrodkowPublicznych,



		/// <summary>
		/// 2.16.840.1.113883.3.4424.7.2.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryNadawanePrzezP1Recepty, "2.16.840.1.113883.3.4424.7.2.1", "Identyfikatory instancji recept nadane przez P1")]
		P1ReceptaInstancja,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.7.2.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryNadawanePrzezP1Recepty, "2.16.840.1.113883.3.4424.7.2.2", "Identyfikatory zbiorów wersji nadane przez P1")]
		P1ReceptaZbiorWersji,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.7.2.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryNadawanePrzezP1Recepty, "2.16.840.1.113883.3.4424.7.2.3", "Identyfikatory pozycji recept nadane przez P1")]
		P1ReceptaPozycji,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.7.2.9
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.IdentyfikatoryNadawanePrzezP1Recepty, "2.16.840.1.113883.3.4424.7.2.9", "Identyfikatory instancji dokumentów anulowania nadanych przez P1")]
		P1ReceptaInstancjaDokumentowAnulowania,
	


		
		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.1", "dolnośląskim")]
		NumeryUmowZNfzSwiadczeniodawcowDolnoslaskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.2
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.2", "kujawsko-pomorskim")]
		NumeryUmowZNfzSwiadczeniodawcowKujawskoPomorskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.3", "lubelskim")]
		NumeryUmowZNfzSwiadczeniodawcowLubelskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.4", "lubuskim")]
		NumeryUmowZNfzSwiadczeniodawcowLubuskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.5
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.5", "łódzkim")]
		NumeryUmowZNfzSwiadczeniodawcowLodzkim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.6
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.6", "małopolskim")]
		NumeryUmowZNfzSwiadczeniodawcowMalopolskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.7
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.7", "mazowieckim")]
		NumeryUmowZNfzSwiadczeniodawcowMazowieckim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.8
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.8", "opolskim")]
		NumeryUmowZNfzSwiadczeniodawcowOpolskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.9
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.9", "podkarpackim")]
		NumeryUmowZNfzSwiadczeniodawcowPodkarpackim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.10
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.10", "podlaskim")]
		NumeryUmowZNfzSwiadczeniodawcowPodlaskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.11
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.11", "pomorskim")]
		NumeryUmowZNfzSwiadczeniodawcowPomorski,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.12
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.12", "śląskim")]
		NumeryUmowZNfzSwiadczeniodawcowSlaskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.13
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.13", "świętokrzyskim")]
		NumeryUmowZNfzSwiadczeniodawcowSwietokrzyskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.14
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.14", "warmińsko-mazurskim")]
		NumeryUmowZNfzSwiadczeniodawcowWarminskoMazurskim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.15
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.15", "wielkoposlkim")]
		NumeryUmowZNfzSwiadczeniodawcowWielkoposlkim,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.8.6.1.16
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow, "2.16.840.1.113883.3.4424.8.6.1.16", "zachodniopomorskim")]
		NumeryUmowZNfzSwiadczeniodawcowZachodniopomorskim,




		[OidEnum(NadzbiorZbioruWartosci.Leki, SystemKodowania.GS1, null, "Identyfikator globalny zgodny z systemem GS1")]
		LekiEAN,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.6.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.Leki, "2.16.840.1.113883.3.4424.6.1", "Identyfikatory leków z bazy produktów leczniczych URPL (rejestr leków P1)")]
		LekiUrplId,

		//[OidEnum(NadzbiorZbioruWartosci.SlownikiP1, SystemKodowania.PolskieKlasyfikatoryHL7v3, "2.16.840.1.113883.3.4424.11.1.25", "Kategoria dostępności leku")]
		//KategorieDostępnosciLekow,


		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.2.4
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SlownikiRSK, "2.16.840.1.113883.3.4424.11.2.4", "Specjalności komórek organizacyjnych (cz. VIII kodu resortowego)")]
		KomorkiOrganizacyjneSpecjalnosc,


		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.3
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SlownikiZewnetrzne, "2.16.840.1.113883.3.4424.11.3.3", "Specjalności lekarskie")]
		SpecjalnosciLekarskie,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.3.1
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SlownikiZewnetrzne, "2.16.840.1.113883.3.4424.11.3.3.1", "Specjalności lekarskie", "Wiele specjalności lekarskich jednej osoby")]
		MnogieSpecjalnosciLekarskich,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.18
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.SlownikiZewnetrzne, "2.16.840.1.113883.3.4424.11.3.18", "Zawody medyczne")]
		SlownikZawodowMedycznych,

		//[OidEnum(NadzbiorZbioruWartosci.SystemyKodowania, SystemKodowania.PolskieKlasyfikatoryHL7v3, "2.16.840.1.113883.3.4424.13.5.1", "Rodzaj leku")]
		//RodzajLeku,


		//[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, Slownik.SystemKodowania.RodzajPraktykiZawodowej, "2.16.840.1.113883.3.4424.13.11.34", "Rodzaj praktyki zawodowej")]
		//RodzajPraktykiZawodowej,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.11.6
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, "2.16.840.1.113883.3.4424.13.11.6", "Kategoria dostępności leku", "Klasyfikacja recept ze względu na kategorię dostępności leku")]
		KategoriaDostepnosciLeku,
		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.11.85
		/// </summary>
		[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, "2.16.840.1.113883.3.4424.13.11.85", "Rodzaj recepty elektronicznej")]
		RodzajReceptyElektronicznej


		//[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, Slownik.SystemKodowania.PolskieKlasyfikatoryHL7v3, "2.16.840.1.113883.3.4424.13.11.7", "Tryb wystawienia recepty")]
		//TrybWystawieniaRecepty,
		//[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, Slownik.SystemKodowania.PolskieKlasyfikatoryHL7v3, "2.16.840.1.113883.3.4424.13.11.8", "Tryb realizacji recepty")]
		//TrybRealizacjiRecepty,
		//[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, Slownik.SystemKodowania.EDQM, "2.16.840.1.113883.3.4424.13.11.81", "Postać dawki leku")]
		//PostacDawkiLeku,
		//[OidEnum(NadzbiorZbioruWartosci.TypyWartosci, Slownik.SystemKodowania.EDQM, "2.16.840.1.113883.3.4424.13.11.82", "Postać opakowania leku")]
		//PostacOpakowaniaLeku 




		//[OidEnum("2.16.840.1.113883.1.3", "HL7")]
		//HL7
		//[OidEnum(ZakresZbioruWartosci., "", "")]
	}
}
