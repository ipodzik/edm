﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.OID
{
	public class UmowaZNfz : IdentyfikatorInstancji
	{
		public string Numer
		{
			get { return this.Extension; }
			set { this.Extension = value; }
		}

		public ZbiorWartosci? OddzialWojewodzki
		{
			get { return this.zbiorWartosci; }
			set { this.UstawZbiorWartosci(value); }
		}



		public UmowaZNfz(string namespacePrefix)
			: base(OID.NadzbiorZbioruWartosci.NumeryUmowZNfz)
		{
			this.NamespacePrefix = namespacePrefix;
		}
	}
}
