﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// I część kodu resortowego 
	/// </summary>
	public class Podmiot : IdentyfikatorInstancji
	{
		public ZbiorWartosci? ZbiorWartosci
		{
			get { return this.zbiorWartosci; }
		}



		public Podmiot() : base(OID.ZbiorWartosci.PodmiotKodResortowyI, true) { }



		public void UstawWartosc(string wartosc)
		{
			this.Extension = wartosc;
		}
	}
}
