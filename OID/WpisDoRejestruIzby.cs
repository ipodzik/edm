﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Wpis do Izby lekarskie lub pielęgniarek i położnych
	/// </summary>
	public class WpisDoRejestruIzby : IdentyfikatorInstancji
	{
		public ZbiorWartosci? ZbiorWartosci // pula wartości 
		{
			get
			{
				return this.zbiorWartosci;
			}
			set
			{
				this.UstawZbiorWartosci(value);
			}
		}



		public WpisDoRejestruIzby() : base(new NadzbiorZbioruWartosci[] { NadzbiorZbioruWartosci.RejestrIzbyLekarskie, NadzbiorZbioruWartosci.RejestrIzbyPielegniarekPoloznych }, true) { }
	}
}
