﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// I i VII część kodu resortowego 
	/// </summary>
	public class KomorkaOrganizacyjna : IdentyfikatorInstancji
	{
		public string KodResortowy1
		{
			get { return this.kodResortowy1; }
			set
			{
				this.kodResortowy1 = value;
				this.UstawWartosc();
			}
		}
		private string kodResortowy1;

		public string KodResortowy7
		{
			get { return this.kodResortowy7; }
			set
			{
				this.kodResortowy7 = value;
				this.UstawWartosc();
			}
		}
		private string kodResortowy7;

		public string Nazwa
		{
			get { return this.nazwa; }
			set
			{
				this.nazwa = value;
				//this.UstawWartosc();
			}
		}
		private string nazwa;

		public string KodResortowy8
		{
			get { return this.kodResortowy8; }
			set
			{
				this.kodResortowy8 = value;
				this.UstawWartosc();
			}
		}
		private string kodResortowy8;

		public KomorkaOrganizacyjna() : base(OID.ZbiorWartosci.KomorkiOrganizacyjne, true) { }



		private void UstawWartosc()
		{
			if (!string.IsNullOrEmpty(this.kodResortowy1) && !string.IsNullOrEmpty(this.kodResortowy7))
			{
				this.Extension = string.Format("{0}-{1}", this.kodResortowy1, this.kodResortowy7);
			}
			else
			{
				this.Extension = null;
			}
		}

		//public void UstawWartosc(KomorkaOrganizacyjna komorkaOrganizacyjna)
		//{
		//  this.KodResortowy1 = komorkaOrganizacyjna.kodResortowy1;
		//  this.KodResortowy7 = komorkaOrganizacyjna.kodResortowy7;
		//  this.UstawWartosc();
		//}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.Extension))
			{
				xmlElement = base.TworzXmlElement(parent);
			}

			return xmlElement;
		}
	}
}
