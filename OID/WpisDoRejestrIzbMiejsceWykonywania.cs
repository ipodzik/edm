﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	public class WpisDoRejestrIzbMiejsceWykonywania : IdentyfikatorInstancji
	{
		public ZbiorWartosci? ZbiorWartosci // pula wartości 
		{
			get
			{
				return this.zbiorWartosci;
			}
			set
			{
				this.UstawZbiorWartosci(value);
			}
		}



		public WpisDoRejestrIzbMiejsceWykonywania() : base(new NadzbiorZbioruWartosci[] { NadzbiorZbioruWartosci.RejestrIzbyLekarskieMiejsceUdzielaniaSwiadczen, NadzbiorZbioruWartosci.RejestrIzbyPielegniarekPoloznychMiejsceUdzielaniaSwiadczen }, true) { }
	}
}
