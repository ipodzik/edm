﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.Common;
using System.Collections.ObjectModel;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Identyfikator instancji (INSTANCE IDENTIFIER) 
	///  - identyfikator obiektu, opisywanego elementem, w ramach którego identyfikator ten jest umieszczony w dokumencie medycznym. 
	///  tu: jako element szablonu
	/// </summary>
	/// <remarks>
	/// OID (ang. Object Identifier) to definiowany przez ISO (standard ISO 9834) sposób stosowania globalnie unikalnych identyfikatorów dowolnych obiektów. 
	///			Globalna unikalność oznacza, że jeden konkretny identyfikator przypisany jest do jednego konkretnego obiektu w skali świata.
	///			Składa się (w konwencji HL7 v3) z dwóch nazwanych wartości: root i extension.
	/// </remarks>
	public class IdentyfikatorInstancji : IElementSzablonu
	{
		/// <summary>
		/// Kolekcja dozwolonych zbiorów wartości (wartości OID) dla danego obiektu 
		/// </summary>
		/// <remarks>ZK: chyba nie używane</remarks>
		//public ReadOnlyCollection<ZbiorWartosci> DozwoloneZbioryWartosci { get { return this.dozwoloneZbioryWartosci; } }
		private readonly ReadOnlyCollection<ZbiorWartosci> dozwoloneZbioryWartosci;

		/// <summary>
		/// Nazwa elementu xml w szablonie tworzonym przez obiekt
		/// </summary>
		internal string XmlElementName { get; set; }
		
		protected string NamespacePrefix { get; set; }

		/// <summary>
		/// Wartość identyfikatora wewnątrz typu/puli identyfikatorów
		/// - w szablonie: wartość atrybutu "extension"
		/// </summary>
		public string Extension { get; set; }

		/// <summary>
		/// Identyfikator typu/puli identyfikatorów
		/// - w szablonie: wartość atrybutu "root"
		/// </summary>
		/// <remarks>
		/// Tworzony na podstawie wartości wyliczenia <see cref="zbiorWartosci"/> - pobierany jest kod z atrybutu ustawionego dla elementu wyliczenia
		/// </remarks>
		public string Root { get { return (this.zbiorWartosci.HasValue) ? this.zbiorWartosci.Value.GetKod() : null; } }

		/// <summary>
		/// Zbiór wartości, z którego pochodzi wartość identyfikatora
		/// OID tego zbioru wartości staje się wartością własności <see cref="Root"/>
		/// </summary>
		protected ZbiorWartosci? zbiorWartosci;

		/// <summary>
		/// Wartość atrybutu displayable dla szablonu
		/// </summary>
		internal bool? CzyWyswietlany { get; set; }


		/// <summary>
		/// Konstruktor bezparametrowy, ustawia domyślną nazwę elementu xml szablonu ("id")
		/// </summary>
		private IdentyfikatorInstancji()
		{
			this.XmlElementName = "id";
			this.NamespacePrefix = null;
		}

		protected IdentyfikatorInstancji(NadzbiorZbioruWartosci[] dozwolonyNadzbiorZbioruWartosci, bool? czyWyswietlany)
			: this()
		{
			var tmp = new List<ZbiorWartosci>();
			foreach (var ajtem in dozwolonyNadzbiorZbioruWartosci)
			{
				tmp.AddRange(ZbiorWartosciHelper.GetValues(ajtem));
			}

			this.dozwoloneZbioryWartosci = tmp.ToList().AsReadOnly();
			this.CzyWyswietlany = czyWyswietlany;
		}

		protected IdentyfikatorInstancji(NadzbiorZbioruWartosci[] dozwolonyNadzbiorZbioruWartosci) : this(dozwolonyNadzbiorZbioruWartosci, null) { }

		protected IdentyfikatorInstancji(NadzbiorZbioruWartosci dozwolonyNadzbiorZbioruWartosci, bool? czyWyswietlany) : this(new NadzbiorZbioruWartosci[] { dozwolonyNadzbiorZbioruWartosci }, czyWyswietlany) { }

		protected IdentyfikatorInstancji(NadzbiorZbioruWartosci dozwolonyNadzbiorZbioruWartosci) : this(new NadzbiorZbioruWartosci[] { dozwolonyNadzbiorZbioruWartosci }, null) { }

		/// <summary>
		/// Konstruktor tworzący obiekt na podstawie tablicy dozwolonych zbiorów wartości, 
		/// przy czym własność <see cref="Extension"/> pozostaje pusta, a własność <see cref="CzyWyswietlany"/> ustawiana jest na null
		/// </summary>
		/// <param name="zbioryWartosci"></param>
		protected IdentyfikatorInstancji(ZbiorWartosci[] zbioryWartosci) : this(zbioryWartosci, null) { }

		/// <summary>
		/// Konstruktor tworzący obiekt, przy czym własność <see cref="Extension"/> pozostaje pusta, a własność <see cref="CzyWyswietlany"/> ustawiana jest na null
		/// </summary>
		/// <param name="zbiorWartosci"></param>
		internal IdentyfikatorInstancji(ZbiorWartosci zbiorWartosci) : this(new ZbiorWartosci[] { zbiorWartosci }) { }

		/// <summary>
		/// Konstruktor tworzący obiekt, przy czym własność <see cref="Extension"/> pozostaje pusta
		/// </summary>
		/// <param name="zbiorWartosci"></param>
		/// <param name="czyWyswietlany"></param>
		internal IdentyfikatorInstancji(ZbiorWartosci zbiorWartosci, bool? czyWyswietlany)
			: this(new ZbiorWartosci[] { zbiorWartosci }, czyWyswietlany)
		{
			this.zbiorWartosci = zbiorWartosci;
		}

		/// <summary>
		/// Konstruktor tworzący obiekt na podstawie tablicy dozwolonych zbiorów wartości, 
		/// przy czym własność <see cref="Extension"/> pozostaje pusta
		/// </summary>
		/// <param name="zbioryWartosci"></param>
		/// <param name="czyWyswietlany"></param>
		protected IdentyfikatorInstancji(ZbiorWartosci[] zbioryWartosci, bool? czyWyswietlany)
			: this()
		{
			this.dozwoloneZbioryWartosci = zbioryWartosci.ToList().AsReadOnly();
			if (this.dozwoloneZbioryWartosci.Count == 1)
			{
				this.zbiorWartosci = this.dozwoloneZbioryWartosci[0];
			}
			this.CzyWyswietlany = czyWyswietlany;
		}


		/// <summary>
		/// Konstruktor podstawowy - ustawia elementy obiektu wprost z parametrów konstruktora
		/// </summary>
		/// <param name="zbiorWartosci">identyfikator puli identyfikatorów z której pochodzi wartość</param>
		/// <param name="wartosc">wartość identyfikatora wewnątrz typu/puli identyfikatorów</param>
		/// <param name="czyWyswietlany">wartość atrybutu displayable dla szablonu</param>
		internal IdentyfikatorInstancji(ZbiorWartosci zbiorWartosci, string wartosc, bool? czyWyswietlany)
			: this()
		{
			this.Extension = wartosc;
			this.zbiorWartosci = zbiorWartosci;
			this.CzyWyswietlany = czyWyswietlany;
		}

		/// <summary>
		/// Konstruktor tworzący obiekt, przy czym własność <see cref="CzyWyswietlany"/> ustawiana jest na null
		/// </summary>
		/// <param name="zbiorWartosci"></param>
		/// <param name="wartosc"></param>
		internal IdentyfikatorInstancji(ZbiorWartosci zbiorWartosci, string wartosc) : this(zbiorWartosci, wartosc, null) { }


		/// <summary>
		/// Metoda tworzy element xml 
		/// - podrzędnie wobec parenta 
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <returns></returns>
		public virtual XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (this.zbiorWartosci.HasValue && !string.IsNullOrEmpty(this.Extension))
			{
				xmlElement = parent.TworzElementXml(this.XmlElementName, this.NamespacePrefix);

				xmlElement.UstawAtrybut("extension", this.Extension);
				xmlElement.SetAttribute("root", this.zbiorWartosci.GetKod());
				if (this.CzyWyswietlany.HasValue)
				{
					xmlElement.SetAttribute("displayable", (this.CzyWyswietlany.Value) ? "true" : "false");
				}
			}

			return xmlElement;
		}

		protected void UstawZbiorWartosci(ZbiorWartosci? zbiorWartosci)
		{
			if (zbiorWartosci.HasValue && this.dozwoloneZbioryWartosci != null && !this.dozwoloneZbioryWartosci.Contains(zbiorWartosci.Value))
			{
				throw new ErrorException(string.Format("Niedozwolony zbiór (pula) wartości '{0}'!", zbiorWartosci.HasValue ? zbiorWartosci.GetKod() : "null"));
			}
			else
			{
				this.zbiorWartosci = zbiorWartosci;
			}
		}
	}
}
