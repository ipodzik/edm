﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	[HL7XmlName("id")]
	public class PodmiotFinansujacyZeSrodkowPublicznych : IdentyfikatorInstancji
	{
		public string Kod
		{
			get { return this.Extension; }
			set { this.Extension = value; }
		}



		public PodmiotFinansujacyZeSrodkowPublicznych() : this(null) { }

		public PodmiotFinansujacyZeSrodkowPublicznych(string namespacePrefix)
			: base(ZbiorWartosci.PodmiotFinansujacyZeSrodkowPublicznych, true)
		{
			this.NamespacePrefix = namespacePrefix;
		}

	}
}
