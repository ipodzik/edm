﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Klasa rozszerzeń dla wyliczeń PolaWartosci 
	/// </summary>
	public static class ZbiorWartosciHelper
	{
		private static List<ZbiorWartosci> allValues;

		

		public static List<ZbiorWartosci> GetValues(NadzbiorZbioruWartosci nadzbiorZbioruWartosci)
		{
			if (ZbiorWartosciHelper.allValues == null)
			{
				ZbiorWartosciHelper.allValues = EnumHelper<ZbiorWartosci>.GetAllValues();
			}

			var nadzbiorZbioruWartosciHashSet = new HashSet<NadzbiorZbioruWartosci>(NadzbiorZbioruWartosciHelper.GetValues(nadzbiorZbioruWartosci));

			if (nadzbiorZbioruWartosciHashSet.Count == 0) // znaczy, że sam zakres nie jest zakresem nadrzędnym dla innych zakresów 
			{
				nadzbiorZbioruWartosciHashSet.Add(nadzbiorZbioruWartosci);
			}

			return ZbiorWartosciHelper.allValues.Where(x => nadzbiorZbioruWartosciHashSet.Contains(x.GetNadzbiorZbioruWartosci().Value)).ToList();
		}
	}
}
