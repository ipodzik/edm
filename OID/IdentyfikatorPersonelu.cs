﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	[HL7XmlName("id")]
	public class IdentyfikatorPersonelu : IdentyfikatorInstancji
	{
		public ZbiorWartosci? ZbiorWartosci
		{
			get { return this.zbiorWartosci; }
		}


		public IdentyfikatorPersonelu() : base(new ZbiorWartosci[] { OID.ZbiorWartosci.NpwzFarmaceutow, OID.ZbiorWartosci.NpwzLekarzyDentystowFelczerow, OID.ZbiorWartosci.NpwzPielegniarekPoloznych, OID.ZbiorWartosci.IdentyfikatoryOsobUePolska }, true) { }



		public void UstawWartosc(ZbiorWartosci? zbiorWartosci, string wartosc)
		{
			this.UstawZbiorWartosci(zbiorWartosci);
			if (zbiorWartosci.HasValue)
			{
				this.Extension = wartosc;
			}
			else
			{
				this.Extension = null;
			}
		}
	}
}
