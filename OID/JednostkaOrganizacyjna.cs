﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// I i V część kodu resortowego 
	/// </summary>
	public class JednostkaOrganizacyjna : IdentyfikatorInstancji
	{
		public string KodResortowy1
		{
			get { return this.kodResortowy1; }
			set
			{
				if (this.kodResortowy1 != value)
				{
					this.kodResortowy1 = value;
					this.UstawWartosc();
				}
			}
		}
		private string kodResortowy1;
		public string KodResortowy5
		{
			get { return this.kodResortowy5; }
			set
			{
				if (this.kodResortowy5 != value)
				{
					this.kodResortowy5 = value;
					this.UstawWartosc();
				}
			}
		}
		private string kodResortowy5;



		public JednostkaOrganizacyjna() : base(OID.ZbiorWartosci.JednostkiOrganizacyjne, true) { }



		private void UstawWartosc()
		{
			if (!string.IsNullOrEmpty(this.kodResortowy1) && !string.IsNullOrEmpty(this.kodResortowy5))
			{
				this.Extension = string.Format("{0}-{1}", this.kodResortowy1, this.kodResortowy5);
			}
			else
			{
				this.Extension = null;
			}
		}

		public override XmlElement TworzXmlElement(System.Xml.XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.Extension))
			{
				xmlElement = base.TworzXmlElement(parent);
			}

			return xmlElement;
		}
	}
}
