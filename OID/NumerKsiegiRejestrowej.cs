﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	public class NumerKsiegiRejestrowej : IdentyfikatorInstancji
	{
		public NumerKsiegiRejestrowej() : base(NadzbiorZbioruWartosci.NumeryKsiegiRejestrowej) { }
	}
}
