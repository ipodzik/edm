﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// Klasa pomocnicza dla wyliczenia NadzbiorZbioruWartosci 
	/// </summary>
	public static class NadzbiorZbioruWartosciHelper
	{
		private static List<NadzbiorZbioruWartosci> allValues;



		/// <summary>
		/// Dla wskazanego węzła w drzewie OID metoda zwraca listę węzłów ... <ZK?>nadrzędnych<ZK?>...
		/// </summary>
		/// <param name="nadzbiorZbioruWartosci"></param>
		/// <returns></returns>
		public static List<NadzbiorZbioruWartosci> GetValues(NadzbiorZbioruWartosci nadzbiorZbioruWartosci)
		{
			if (NadzbiorZbioruWartosciHelper.allValues == null)
			{
				NadzbiorZbioruWartosciHelper.allValues = EnumHelper<NadzbiorZbioruWartosci>.GetAllValues();
			}

			// takie rozbicie chiba edzie szybciejsze w działaniu 
			var parentNadzbioruZbioruWartosci = NadzbiorZbioruWartosciHelper.allValues.Select(x => new KeyValuePair<NadzbiorZbioruWartosci, NadzbiorZbioruWartosci?>(x, x.GetNadzbiorZbioruWartosci())).ToArray();
			var wartosci = parentNadzbioruZbioruWartosci.Where(x => x.Value.HasValue && x.Value == nadzbiorZbioruWartosci).Select(x => x.Key).ToList();

			if (wartosci.Count > 0)
			{
				var tmp = new List<NadzbiorZbioruWartosci>();
				foreach (var ajtem in wartosci)
				{
					tmp.AddRange(NadzbiorZbioruWartosciHelper.GetValues(ajtem));
				}
				wartosci.AddRange(tmp);
			}

			return wartosci;
		}


		/// <summary>
		/// Dla wskazanego węzła w drzewie OID metoda zwraca jego najwyższy węzeł nadrzędny
		/// </summary>
		/// <param name="nadzbioruZbioruWartosci"></param>
		/// <returns></returns>
		public static NadzbiorZbioruWartosci GetTopMostElement(NadzbiorZbioruWartosci nadzbioruZbioruWartosci)
		{
			var parent = nadzbioruZbioruWartosci.GetNadzbiorZbioruWartosci();
			if (parent.HasValue)
			{
				return NadzbiorZbioruWartosciHelper.GetTopMostElement(parent.Value);
			}
			else
			{
				return nadzbioruZbioruWartosci;
			}
		}
	}
}
