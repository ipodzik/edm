﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.OID
{
	/// <summary>
	/// REGON 9 lub 14 cyfr
	/// </summary>
	public class Regon : IdentyfikatorInstancji
	{
		public ZbiorWartosci? ZbiorWartosci // pula wartości 
		{
			get
			{
				return this.zbiorWartosci;
			}
			//set
			//{
			//  this.UstawZbiorWartosci(value);
			//}
		}



		public Regon() : base(new ZbiorWartosci[] { OID.ZbiorWartosci.Regon9, OID.ZbiorWartosci.Regon14 }, true) { }

		public Regon(ZbiorWartosci zbiorWartosci)
			: this()
		{
			this.UstawZbiorWartosci(zbiorWartosci);
		}




		public void UstawWartosc(ZbiorWartosci? zbiorWartosci, string wartosc)
		{
			this.UstawZbiorWartosci(zbiorWartosci);
			if (zbiorWartosci.HasValue)
			{
				this.Extension = wartosc;
			}
			else
			{
				this.Extension = null;
			}
		}
	}
}
