﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BIUINF.Lib4.Common;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Klasa pomocnicza dla wyliczeń
	/// </summary>
	/// <typeparam methodName="TEnum">TEnum - wyliczenie</typeparam>
	public static class EnumHelper<TEnum>
		where TEnum : struct, IConvertible, IComparable, IFormattable
	{
		/// <summary>
		/// Metoda zwraca listę wszystkich wartości wyliczenia
		/// </summary>
		/// <returns></returns>
		public static List<TEnum> GetAllValues()
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new ErrorException("TEnum musi być wyliczeniem!");
			}

			return Enum.GetValues(typeof(TEnum)).Cast<TEnum>().ToList();
		}

		/// <summary>
		/// Metoda zwraca niestandardowy atrybut zastosowany do wskazanego wyliczenia
		/// </summary>
		/// <typeparam name="TEnumAttribute"></typeparam>
		/// <returns></returns>
		public static TEnumAttribute GetCustomAttribute<TEnumAttribute>()
			where TEnumAttribute : EnumAttribute
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new ErrorException("TEnum musi być wyliczeniem!");
			}

			return AttributeHelper<TEnum>.GetCustomAttribute<TEnumAttribute>();
		}

		/// <summary>
		/// Metoda zwraca pozycję wyliczenia posiadającą wskazany kod
		/// </summary>
		/// <param name="kod"></param>
		/// <returns></returns>
		public static TEnum GetValue(string kod)
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new ErrorException("TEnum musi być wyliczeniem!");
			}

			var type = typeof(TEnum);
			var names = typeof(TEnum).GetEnumNames();

			FieldInfo fieldInfo = null;

			foreach (var name in names)
			{
				var enumAttribute = type.GetField(name).GetCustomAttributes(false).OfType<EnumAttribute>().SingleOrDefault();
				if (enumAttribute != null)
				{
					if (enumAttribute.Kod == kod)
					{
						fieldInfo = type.GetField(name);
						break;
					}
				}
			}

			if (fieldInfo == null)
			{
				throw new ErrorException("Brak pozycji wyliczenia '" + type.Name + "' posiadającej kod o wartości '" + kod + "'");
			}

			return (TEnum)fieldInfo.GetValue(null);
		}
	}
}
