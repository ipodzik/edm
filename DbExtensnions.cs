﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;



namespace BIUINF.Lib4.HL7CDA
{
	static class DbExtensnions
	{
		internal static DbCommand CreateCommand(this DbTransaction value, CommandType commandType)
		{
			return value.CreateCommand(commandType, 30);
		}

		internal static DbCommand CreateCommand(this DbTransaction value, CommandType commandType, int commandTimeout)
		{
			var cmd = value.Connection.CreateCommand();
			cmd.Transaction = value;
			cmd.CommandType = commandType;
			cmd.CommandTimeout = commandTimeout;

			return cmd;
		}


		internal static DbCommand CreateCommand(this DbConnection value, CommandType commandType)
		{
			return value.CreateCommand(commandType, 30);
		}

		internal static DbCommand CreateCommand(this DbConnection value, CommandType commandType, int commandTimeout)
		{
			var cmd = value.CreateCommand();
			cmd.CommandType = commandType;
			cmd.CommandTimeout = commandTimeout;

			return cmd;
		}
	}
}
