﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Xml
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_CD
	*/
	/// <summary>
	/// Typ danych CD 
	/// </summary>
	internal class ConceptDescriptor : XmlElement
	{
		internal string Code { get { return this.GetAttribute("code"); } set { this.UstawAtrybut("code", value); } }
		internal string CodeSystem { get { return this.GetAttribute("codeSystem"); } set { this.UstawAtrybut("codeSystem", value); } }
		internal string CodeSystemName { get { return this.GetAttribute("codeSystemName"); } set { this.UstawAtrybut("codeSystemName", value); } }
		internal string DisplayName { get { return this.GetAttribute("displayName"); } set { this.UstawAtrybut("displayName", value); } }
		internal string NullFlavor { get { return this.GetAttribute("nullFlavor"); } set { this.UstawAtrybut("nullFlavor", value); } }



		internal ConceptDescriptor(XmlElement parent, string name, string namespacePrefix)
			: base(namespacePrefix ?? string.Empty, name, parent.OwnerDocument.GetNamespaceOfPrefix(namespacePrefix ?? string.Empty), parent.OwnerDocument) { }

		internal ConceptDescriptor(XmlElement parent, string name)
			: this(parent, name, null) { }
	}




	internal class ConceptDescriptorBuilder
	{
		private readonly List<Action<ConceptDescriptor>> actions = new List<Action<ConceptDescriptor>>();
		private readonly XmlElement parent;
		private readonly string name;
		private readonly string namespacePrefix;



		internal ConceptDescriptorBuilder(XmlElement parent, string name, string namespacePrefix)
		{
			this.parent = parent;
			this.name = name;
			this.namespacePrefix = namespacePrefix;
		}

		internal ConceptDescriptorBuilder(XmlElement parent) : this(parent, "code", null) { }

		internal ConceptDescriptorBuilder(XmlElement parent, string name) : this(parent, name, null) { }



		internal void AddAction(Action<ConceptDescriptor> action)
		{
			this.actions.Add(action);
		}

		internal ConceptDescriptor Build()
		{
			var ajtem = new ConceptDescriptor(this.parent, this.name, this.namespacePrefix);
			this.actions.ForEach(a => a(ajtem));
			return ajtem;
		}
	}




	internal static class ConceptDescriptorBuilderExtensnion
	{
		internal static ConceptDescriptorBuilder SetCode(this ConceptDescriptorBuilder builder, string value)
		{
			builder.AddAction(a => a.Code = value);
			return builder;
		}

		internal static ConceptDescriptorBuilder SetCodeSystem(this ConceptDescriptorBuilder builder, string value)
		{
			builder.AddAction(a => a.CodeSystem = value);
			return builder;
		}

		internal static ConceptDescriptorBuilder SetCodeSystemName(this ConceptDescriptorBuilder builder, string value)
		{
			builder.AddAction(a => a.CodeSystemName = value);
			return builder;
		}

		internal static ConceptDescriptorBuilder SetDisplayName(this ConceptDescriptorBuilder builder, string value)
		{
			builder.AddAction(a => a.DisplayName = value);
			return builder;
		}

		internal static ConceptDescriptorBuilder SetNullFlavor(this ConceptDescriptorBuilder builder, string value)
		{
			builder.AddAction(a => a.NullFlavor = value);
			return builder;
		}
	}
}
