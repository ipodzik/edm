﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Xml
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_CS
	*/
	/// <summary>
	/// Typ danych CS 
	/// </summary>
	internal class CodedSimpleValue : XmlElement
	{
		internal string Code { get { return this.GetAttribute("code"); } set { this.UstawAtrybut("code", value); } }
		internal string NullFlavor { get { return this.GetAttribute("nullFlavor"); } set { this.UstawAtrybut("nullFlavor", value); } }



		internal CodedSimpleValue(XmlElement parent, string name, string namespacePrefix)
			: base(namespacePrefix ?? string.Empty, name, parent.OwnerDocument.GetNamespaceOfPrefix(namespacePrefix ?? string.Empty), parent.OwnerDocument) { }

		internal CodedSimpleValue(XmlElement parent, string name)
			: this(parent, name, null) { }
	}




	internal class CodedSimpleValueBuilder
	{
		private readonly List<Action<CodedSimpleValue>> actions = new List<Action<CodedSimpleValue>>();
		private readonly XmlElement parent;
		private readonly string name;
		private readonly string namespacePrefix;



		internal CodedSimpleValueBuilder(XmlElement parent, string name, string namespacePrefix)
		{
			this.parent = parent;
			this.name = name;
			this.namespacePrefix = namespacePrefix;
		}

		internal CodedSimpleValueBuilder(XmlElement parent) : this(parent, "code", null) { }

		internal CodedSimpleValueBuilder(XmlElement parent, string name) : this(parent, name, null) { }



		internal void AddAction(Action<CodedSimpleValue> action)
		{
			this.actions.Add(action);
		}

		internal CodedSimpleValue Build()
		{
			var ajtem = new CodedSimpleValue(this.parent, this.name, this.namespacePrefix);
			this.actions.ForEach(a => a(ajtem));
			return ajtem;
		}
	}




	internal static class CodedSimpleValueBuilderExtensnion
	{
		internal static CodedSimpleValueBuilder SetCode(this CodedSimpleValueBuilder builder, string value)
		{
			builder.AddAction(a => a.Code = value);
			return builder;
		}

		internal static CodedSimpleValueBuilder SetNullFlavor(this CodedSimpleValueBuilder builder, string value)
		{
			builder.AddAction(a => a.NullFlavor = value);
			return builder;
		}
	}
}
