﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Xml
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_ST
	*/
	/// <summary>
	/// Typ danych ST 
	/// </summary>
	internal class CharacterString : XmlElement
	{
		internal string Language { get { return this.GetAttribute("language"); } set { this.UstawAtrybut("language", value); } }
		internal string NullFlavor { get { return this.GetAttribute("nullFlavor"); } set { this.UstawAtrybut("nullFlavor", value); } }

		internal string InnerText { get { return this.InnerText; } set { this.InnerText = value; } }



		internal CharacterString(XmlElement parent, string name, string namespacePrefix)
			: base(namespacePrefix ?? string.Empty, name, parent.OwnerDocument.GetNamespaceOfPrefix(namespacePrefix ?? string.Empty), parent.OwnerDocument) { }

		internal CharacterString(XmlElement parent, string name)
			: this(parent, name, null) { }
	}




	internal class CharacterStringBuilder
	{
		private readonly List<Action<CharacterString>> actions = new List<Action<CharacterString>>();
		private readonly XmlElement parent;
		private readonly string name;
		private readonly string namespacePrefix;



		internal CharacterStringBuilder(XmlElement parent, string name, string namespacePrefix)
		{
			this.parent = parent;
			this.name = name;
			this.namespacePrefix = namespacePrefix;
		}

		internal CharacterStringBuilder(XmlElement parent, string name) : this(parent, name, null) { }



		internal void AddAction(Action<CharacterString> action)
		{
			this.actions.Add(action);
		}

		internal CharacterString Build()
		{
			var ajtem = new CharacterString(this.parent, this.name, this.namespacePrefix);
			this.actions.ForEach(a => a(ajtem));
			return ajtem;
		}
	}




	internal static class CharacterStringBuilderExtensnion
	{
		internal static CharacterStringBuilder SetLanguage(this CharacterStringBuilder builder, string value)
		{
			builder.AddAction(a => a.Language = value);
			return builder;
		}

		internal static CharacterStringBuilder SetInnerText(this CharacterStringBuilder builder, string value)
		{
			builder.AddAction(a => a.InnerText = value);
			return builder;
		}

		internal static CharacterStringBuilder SetNullFlavor(this CharacterStringBuilder builder, string value)
		{
			builder.AddAction(a => a.NullFlavor = value);
			return builder;
		}
	}
}
