﻿using System;
using System.Collections.Generic;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon;



namespace BIUINF.Lib4.HL7CDA.Xml
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_II 
	*/
	/// <summary>
	/// Typ danych II 
	/// </summary>
	public class InstanceIdentifier : XmlElement
	{
		internal string Root { get { return this.GetAttribute("root"); } set { this.UstawAtrybut("root", value); } }
		internal string Extension { get { return this.GetAttribute("extension"); } set { this.UstawAtrybut("extension", value); } }
		internal string Displayable { get { return this.GetAttribute("displayable"); } set { this.UstawAtrybut("displayable", value); } }
		internal string NullFlavor { get { return this.GetAttribute("nullFlavor"); } set { this.UstawAtrybut("nullFlavor", value); } }



		internal InstanceIdentifier(XmlElement parent, string name, string namespacePrefix)
			: base(namespacePrefix ?? string.Empty, name, parent.OwnerDocument.GetNamespaceOfPrefix(namespacePrefix ?? string.Empty), parent.OwnerDocument) { }

		internal InstanceIdentifier(XmlElement parent, string name)
			: this(parent, name, null) { }

		internal InstanceIdentifier(XmlElement parent)
			: this(parent, "id", null) { }
	}




	internal class InstanceIdentifierBuilder
	{
		private readonly List<Action<InstanceIdentifier>> actions = new List<Action<InstanceIdentifier>>();
		private readonly XmlElement parent;
		private readonly string name;
		private readonly string namespacePrefix;



		internal InstanceIdentifierBuilder(XmlElement parent, string name, string namespacePrefix)
		{
			this.parent = parent;
			this.name = name;
			this.namespacePrefix = namespacePrefix;
		}

		internal InstanceIdentifierBuilder(XmlElement parent) : this(parent, "id", null) { }

		internal InstanceIdentifierBuilder(XmlElement parent, Szablon.Szablon szablon) : this(parent, "templateId", null) 
		{
			this.SetRoot(szablon.GetAttribute<SzablonAttribute>().Id);
		}		

		internal InstanceIdentifierBuilder(XmlElement parent, string name) : this(parent, name, null) { }



		internal void AddAction(Action<InstanceIdentifier> action)
		{
			this.actions.Add(action);
		}

		internal InstanceIdentifier Build()
		{
			var ajtem = new InstanceIdentifier(this.parent, this.name, this.namespacePrefix);
			this.actions.ForEach(a => a(ajtem));
			return ajtem;
		}
	}




	internal static class InstanceIdentifierBuilderExtensnion
	{
		internal static InstanceIdentifierBuilder SetRoot(this InstanceIdentifierBuilder builder, string value)
		{
			builder.AddAction(a => a.Root = value);
			return builder;
		}

		internal static InstanceIdentifierBuilder SetRoot(this InstanceIdentifierBuilder builder, Szablon.Szablon szablon)
		{
			builder.SetRoot(szablon.GetAttribute<SzablonAttribute>().Id);
			return builder;
		}

		internal static InstanceIdentifierBuilder SetExtension(this InstanceIdentifierBuilder builder, string value)
		{
			builder.AddAction(a => a.Extension = value);
			return builder;
		}

		internal static InstanceIdentifierBuilder SetDisplayable(this InstanceIdentifierBuilder builder, bool? value)
		{
			if (value.HasValue)
			{
				builder.AddAction(a => a.Displayable = value.Value ? "true" : "false");
			}
			return builder;
		}

		internal static InstanceIdentifierBuilder SetNullFlavor(this InstanceIdentifierBuilder builder, string value)
		{
			builder.AddAction(a => a.NullFlavor = value);
			return builder;
		}
	}
}
