﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Xml
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_TS
	*/
	/// <summary>
	/// Typ danych TS 
	/// </summary>
	internal class TimeStamp : XmlElement
	{
		internal string Value { get { return this.GetAttribute("value"); } set { this.UstawAtrybut("value", value); } }




		internal TimeStamp(XmlElement parent, string name, string namespacePrefix)
			: base(namespacePrefix ?? string.Empty, name, parent.OwnerDocument.GetNamespaceOfPrefix(namespacePrefix ?? string.Empty), parent.OwnerDocument) { }

		internal TimeStamp(XmlElement parent, string name)
			: this(parent, name, null) { }
	}





	internal class TimeStampBuilder
	{
		private readonly List<Action<TimeStamp>> actions = new List<Action<TimeStamp>>();
		private readonly XmlElement parent;
		private readonly string name;
		private readonly string namespacePrefix;



		internal TimeStampBuilder(XmlElement parent, string name, string namespacePrefix)
		{
			this.parent = parent;
			this.name = name;
			this.namespacePrefix = namespacePrefix;
		}

		internal TimeStampBuilder(XmlElement parent, string name) : this(parent, name, null) { }



		internal void AddAction(Action<TimeStamp> action)
		{
			this.actions.Add(action);
		}

		internal TimeStamp Build()
		{
			var ajtem = new TimeStamp(this.parent, this.name, this.namespacePrefix);
			this.actions.ForEach(a => a(ajtem));
			return ajtem;
		}
	}



	internal static class TimeStampBuilderExtensnion
	{
		internal static TimeStampBuilder SetValue(this TimeStampBuilder builder, DateTime value)
		{
			builder.AddAction(a => a.Value = value.ToString("yyyyMMdd"));
			return builder;
		}

	}
}
