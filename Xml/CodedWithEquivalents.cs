﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Xml
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_CE
	*/
	/// <summary>
	/// Typ danych CE 
	/// </summary>
	internal class CodedWithEquivalents : XmlElement
	{
		internal string Code { get { return this.GetAttribute("code"); } set { this.UstawAtrybut("code", value); } }
		internal string CodeSystem { get { return this.GetAttribute("codeSystem"); } set { this.UstawAtrybut("codeSystem", value); } }
		internal string CodeSystemName { get { return this.GetAttribute("codeSystemName"); } set { this.UstawAtrybut("codeSystemName", value); } }
		internal string DisplayName { get { return this.GetAttribute("displayName"); } set { this.UstawAtrybut("displayName", value); } }
		internal string NullFlavor { get { return this.GetAttribute("nullFlavor"); } set { this.UstawAtrybut("nullFlavor", value); } }



		internal CodedWithEquivalents(XmlElement parent, string name, string namespacePrefix)
			: base(namespacePrefix ?? string.Empty, name, parent.OwnerDocument.GetNamespaceOfPrefix(namespacePrefix ?? string.Empty), parent.OwnerDocument) { }

		internal CodedWithEquivalents(XmlElement parent, string name)
			: this(parent, name, null) { }
	}




	internal class CodedWithEquivalentsBuilder
	{
		private readonly List<Action<CodedWithEquivalents>> actions = new List<Action<CodedWithEquivalents>>();
		private readonly XmlElement parent;
		private readonly string name;
		private readonly string namespacePrefix;



		internal CodedWithEquivalentsBuilder(XmlElement parent, string name, string namespacePrefix)
		{
			this.parent = parent;
			this.name = name;
			this.namespacePrefix = namespacePrefix;
		}

		internal CodedWithEquivalentsBuilder(XmlElement parent) : this(parent, "code", null) { }

		internal CodedWithEquivalentsBuilder(XmlElement parent, string name) : this(parent, name, null) { }



		internal void AddAction(Action<CodedWithEquivalents> action)
		{
			this.actions.Add(action);
		}

		internal CodedWithEquivalents Build()
		{
			var ajtem = new CodedWithEquivalents(this.parent, this.name, this.namespacePrefix);
			this.actions.ForEach(a => a(ajtem));
			return ajtem;
		}
	}




	internal static class CodedWithEquivalentsBuilderExtensnion
	{
		internal static CodedWithEquivalentsBuilder SetCode(this CodedWithEquivalentsBuilder builder, string value)
		{
			builder.AddAction(a => a.Code = value);
			return builder;
		}

		internal static CodedWithEquivalentsBuilder SetCodeSystem(this CodedWithEquivalentsBuilder builder, string value)
		{
			builder.AddAction(a => a.CodeSystem = value);
			return builder;
		}

		internal static CodedWithEquivalentsBuilder SetCodeSystemName(this CodedWithEquivalentsBuilder builder, string value)
		{
			builder.AddAction(a => a.CodeSystemName = value);
			return builder;
		}

		internal static CodedWithEquivalentsBuilder SetDisplayName(this CodedWithEquivalentsBuilder builder, string value)
		{
			builder.AddAction(a => a.DisplayName = value);
			return builder;
		}

		internal static CodedWithEquivalentsBuilder SetNullFlavor(this CodedWithEquivalentsBuilder builder, string value)
		{
			builder.AddAction(a => a.NullFlavor = value);
			return builder;
		}
	}
}
