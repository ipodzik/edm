﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Klasa atrybutu dla wartości wyliczenia 
	/// - pozwala powiązać element wyliczenia z pozycją słownika CSIOZ przypisując mu zestaw (Kod - Nazwa - Opis)
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum, AllowMultiple = false)]
	public class EnumAttribute : Attribute
	{
		/// <summary>
		/// Tekst wykorzystywany przez tooltip
		/// </summary>
		public string Opis { get; protected set; }

		/// <summary>
		/// Wartość kodu dla pozycji wyliczenia 
		/// </summary>
		public string Kod { get; protected set; }

		/// <summary>
		/// Pełna nazwa, wykorzystywana przez przeciążoną metodaę ToString()
		/// </summary>
		public string Nazwa { get; protected set; }



		internal EnumAttribute(string kod) : this(kod, null, null) { }

		internal EnumAttribute(string kod, string nazwa) : this(kod, nazwa, nazwa) { }

		internal EnumAttribute(string kod, string nazwa, string opis)
		{
			this.Kod = kod;
			this.Nazwa = nazwa;
			this.Opis = opis;
		}
	}
}
