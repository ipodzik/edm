﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Wygląda że nieużywana
	/// </summary>
	public static class ObjectAttributeExtension
	{
		public static TAttribute GetAttribute<TAttribute>(this object value)
			where TAttribute : Attribute
		{
			if (value != null)
			{
				var type = value.GetType();
				return type.GetCustomAttributes(false).OfType<TAttribute>().SingleOrDefault();
			}
			else
			{
				return null;
			}
		}

		public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this object value)
			where TAttribute : Attribute
		{
			if (value != null)
			{
				var type = value.GetType();
				return type.GetCustomAttributes(false).OfType<TAttribute>().ToArray();
			}
			else
			{
				return null;
			}
		}
	}
}
