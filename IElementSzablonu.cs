﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Interfejs elementu szablonu
	/// </summary>
	public interface IElementSzablonu
	{
		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <returns></returns>
		XmlElement TworzXmlElement(XmlElement parent);
	}
}
