﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Klasa atrybutu dla powiązania obiektu programowego z nazwą HL7
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class HL7XmlNameAttribute : Attribute
	{
		public string Name { get; set; }
		public string Format { get; private set; }



		public HL7XmlNameAttribute(string name)
		{
			this.Name = name;
		}

		public HL7XmlNameAttribute(string name, string format)
		{
			this.Name = name;
			this.Format = format;
		}
	}
}
