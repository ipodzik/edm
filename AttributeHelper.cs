﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Klasa pomocnicza dla atrybutów
	/// </summary>	
	public class AttributeHelper<TType>
	{
		/// <summary>
		/// Metoda zwraca niestandardowy atrybut zastosowany do wskazanego elementu 
		/// </summary>
		/// <typeparam name="TAttribute"></typeparam>
		/// <returns></returns>
		public static TAttribute GetCustomAttribute<TAttribute>()
			where TAttribute : Attribute
		{
			var type = typeof(TType);
			return type.GetCustomAttributes(false).OfType<TAttribute>().SingleOrDefault();
		}

		/// <summary>
		/// Metoda zwraca tablicę wszystkich niestandardowych atrybutów zastosowanych do wskazanego elementu 
		/// </summary>
		/// <typeparam name="TAttribute"></typeparam>
		/// <returns></returns>
		public static IEnumerable<TAttribute> GetCustomAttributes<TAttribute>()
			where TAttribute : Attribute
		{
			var type = typeof(TType);
			return type.GetCustomAttributes(false).OfType<TAttribute>();
		}
	}
}
