﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon;



namespace BIUINF.Lib4.HL7CDA
{
	public static class XmlElementTworzElementXml
	{
		/// <summary>
		/// Tworzy XmlElement dla danego szablonu 
		/// </summary>
		/// <param name="szablon">obiekt szablonu</param>
		public static XmlElement TworzElementXml(this XmlElement value, Szablon.SzablonSkladowy szablon)
		{
			return value.TworzElementXml(szablon.GetAttribute<SzablonAttribute>().XmlElementNazwa, szablon.GetAttribute<SzablonAttribute>().NamespacePrefix);
		}

		public static XmlElement TworzElementXml(this XmlElement value, IElementSzablonu elementSzablonu)
		{
			return value.TworzElementXml(elementSzablonu.GetAttribute<HL7XmlNameAttribute>().Name, null);
		}

		public static XmlElement TworzElementXml(this XmlElement value, IElementSzablonu elementSzablonu, string namespacePrefix)
		{
			return value.TworzElementXml(elementSzablonu.GetAttribute<HL7XmlNameAttribute>().Name, namespacePrefix);
		}

		public static XmlElement TworzElementXml(this XmlElement value, string nazwa)
		{
			return value.TworzElementXml(nazwa, null);
		}

		public static XmlElement TworzElementXml(this XmlElement value, string nazwa, string namespacePrefix)
		{
			if (!string.IsNullOrEmpty(namespacePrefix))
			{
				return value.OwnerDocument.CreateElement(namespacePrefix, nazwa, value.OwnerDocument.DocumentElement.GetNamespaceOfPrefix(namespacePrefix));
			}
			else
			{
				return value.OwnerDocument.CreateElement(nazwa);
			}
		}
	}
}
