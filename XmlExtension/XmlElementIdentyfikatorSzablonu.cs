﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	public static class XmlElementIdentyfikatorSzablonu
	{
		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący identyfikatorem szablonu (templateId)
		/// </summary>
		/// <param name="szablon">obiekt szablonu</param>
		internal static XmlElement DodajIdentyfikatorSzablonu(this XmlElement value, Szablon.Szablon szablon)
		{
			return value.DodajIdentyfikatorSzablonu(szablon, null);
		}

		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący identyfikatorem szablonu (templateId)
		/// </summary>
		/// <param name="szablon">obiekt szablonu</param>
		internal static XmlElement DodajIdentyfikatorSzablonu(this XmlElement value, Szablon.Szablon szablon, string namespacePrefix)
		{
			var xmlElement = value.TworzElementXml("templateId", namespacePrefix);
			xmlElement.SetAttribute("root", szablon.GetAttribute<SzablonAttribute>().Id);
			value.AppendChild(xmlElement);
			return xmlElement;
		}



		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący identyfikatorem szablonu (templateId)
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="root">zakres identyfikatora obiektu</param>
		internal static XmlElement DodajIdentyfikatorSzablonu(this XmlElement value, XmlElement parent, string root)
		{
			var xmlElement = parent.OwnerDocument.CreateElement("templateId");
			xmlElement.SetAttribute("root", root);
			value.AppendChild(xmlElement);
			return xmlElement;
		}

		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący identyfikatorem szablonu (templateId)
		/// </summary>
		/// <param name="root">zakres identyfikatora obiektu</param>
		internal static XmlElement DodajIdentyfikatorSzablonu(this XmlElement value, string root)
		{
			return value.DodajIdentyfikatorSzablonu(value.OwnerDocument.DocumentElement, root);
		}
	}
}
