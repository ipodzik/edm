﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Coded With Equivalents
	/// </summary>
	public static class XmlExtensionTypCE
	{
		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będacy typem danych CE (Coded With Equivalents)
		/// </summary>
		/// <param name="code">wartość code</param>
		/// <param name="codeSystem">wartość codeSystem</param>
		/// <param name="codeSystemName">wartość codeSystemName</param>
		/// <param name="displayName">nazwa wyświetlana</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement DodajTypCE(this XmlElement v, string code, string codeSystem, string codeSystemName, string displayName, XmlElement xmlElementTranslation)
		{
			var xmlElement = v.TworzElementXml("code").UstawAtrybut("code", code).UstawAtrybut("codeSystem", codeSystem).UstawAtrybut("codeSystemName", codeSystemName).UstawAtrybut("displayName", displayName);
			if (xmlElementTranslation != null)
			{
				xmlElement.AppendChild(xmlElementTranslation);
			}
			
			v.AppendChild(xmlElement);
			
			return xmlElement;
		}
	}
}
