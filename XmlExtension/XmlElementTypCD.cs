﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Concept Descriptor
	/// </summary>
	public static class XmlExtensionTypCD
	{
		/// <summary>
		/// Tworzy element xml będacy typem danych CD (Concept Descriptor)
		/// </summary>
		/// <param name="elementName">nazwa elementu</param>
		/// <param name="code">wartość code</param>
		/// <param name="codeSystem">wartość codeSystem</param>
		/// <param name="codeSystemName">wartość codeSystemName</param>
		/// <param name="displayName">nazwa wyświetlana</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement TworzTypCD(this XmlElement v, string elementName, string code, string codeSystem, string codeSystemName, string displayName)
		{
			return v.TworzElementXml(elementName).UstawAtrybut("code", code).UstawAtrybut("codeSystem", codeSystem).UstawAtrybut("codeSystemName", codeSystemName).UstawAtrybut("displayName", displayName);
		}


		/// <summary>
		/// Tworzy i dodaje jako podrzędny, element xml będacy typem danych CD (Concept Descriptor)
		/// </summary>
		/// <param name="code">wartość code</param>
		/// <param name="codeSystem">wartość codeSystem</param>
		/// <param name="codeSystemName">wartość codeSystemName</param>
		/// <param name="displayName">nazwa wyświetlana</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement DodajTypCD(this XmlElement v, string code, string codeSystem, string codeSystemName, string displayName)
		{
			var xmlElement = v.TworzTypCD("code", code, codeSystem, codeSystemName, displayName);
			v.AppendChild(xmlElement);
			return xmlElement;
		}

		/// <summary>
		/// Dodaje element xml będacy typem danych CD (Concept Descriptor)
		/// </summary>
		/// <param name="code">wartość code</param>
		/// <param name="codeSystem">wartość codeSystem</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement DodajTypCD(this XmlElement v, string code, string codeSystem)
		{
			return v.DodajTypCD(code, codeSystem, null, null);
		}
	}
}
