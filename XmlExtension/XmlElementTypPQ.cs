﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Pysical Value 
	/// </summary>
	public static class XmlExtensionTypPQ
	{
		/// <summary>
		/// Tworzy i dodaje jako podrzędny, element xml będacy typem danych PQ (Pysical Value)
		/// </summary>
		/// <param name="xmlElementName">nazwa elementu xml</param>
		/// <param name="value">ilość</param>
		/// <param name="unit">jednostka</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement DodajTypPQ(this XmlElement v, string xmlElementName, string value, string unit)
		{
			var xmlElement = v.TworzElementXml(xmlElementName).UstawAtrybut("type", "xsi", "PQ").UstawAtrybut("value", value).UstawAtrybut("unit", unit);
			v.AppendChild(xmlElement);
			return xmlElement;
		}

		/// <summary>
		/// Tworzy i dodaje jako podrzędny, element xml będacy typem danych PQ (Pysical Value)
		/// </summary>
		/// <param name="xmlElementName">nazwa elementu xml</param>
		/// <param name="value">ilość</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement DodajTypPQ(this XmlElement v, string xmlElementName, string value)
		{
			return v.DodajTypPQ(xmlElementName, value, null);
		}
	}
}
