﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	/*
				 https://art-decor.org/mediawiki/index.php?title=DTr1_CV
	*/
	/// <summary>
	/// Coded Value 
	/// </summary>
	public static class XmlExtensionTypCV
	{
		/// <summary>
		/// Tworzy element xml będacy typem danych CV (Coded Value)
		/// </summary>
		/// <param name="elementName">nazwa elementu</param>
		/// <param name="code">wartość code</param>
		/// <param name="codeSystem">wartość codeSystem</param>
		/// <param name="codeSystemName">wartość codeSystemName</param>
		/// <param name="displayName">nazwa wyświetlana</param>
		/// <returns>utworzony element xml</returns>
		internal static XmlElement TworzTypCV(this XmlElement v, string elementName, string code, string codeSystem, string codeSystemName, string displayName)
		{
			return v.TworzElementXml(elementName).UstawAtrybut("code", code).UstawAtrybut("codeSystem", codeSystem).UstawAtrybut("codeSystemName", codeSystemName).UstawAtrybut("displayName", displayName);
		}

		
	}
}
