﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	public static class XmlExtensionDodajElement
	{
		/// <summary>
		/// Tworzy i dodaje podrzędny element z wartością podaną w parametrze o ile wartość jest niepusta
		/// </summary>
		/// <param name="nazwa">nazwa dodawanego elementu xml</param>
		internal static XmlElement DodajElement(this XmlElement value, string nazwa)
		{
			return value.DodajElement(value.OwnerDocument.DocumentElement, nazwa, null, null);
		}

		/// <summary>
		/// Dodaje podrzędny element z wartością podaną w parametrze o ile wartość jest niepusta
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="nazwa">nazwa dodawanego elementu xml</param>
		/// <param name="innerText">tekst wewnątrz elementu xml</param>
		internal static XmlElement DodajElement(this XmlElement value, XmlElement parent, string nazwa, string innerText)
		{
			return value.DodajElement(parent, nazwa, innerText, null);
		}

		/// <summary>
		/// Dodaje podrzędny element z tekstem wewnętrznym podanym w parametrze o ile innerText jest niepusta
		/// </summary>
		/// <param name="nazwa">nazwa dodawanego elementu xml</param>
		/// <param name="innerText">tekst wewnątrz elementu xml</param>
		internal static XmlElement DodajElement(this XmlElement value, string nazwa, string innerText)
		{
			return value.DodajElement(value.OwnerDocument.DocumentElement, nazwa, innerText, null);
		}

		/// <summary>
		/// Dodaje podrzędny element z tekstem wewnętrznym podanym w parametrze o ile innerText jest niepusta
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="nazwa">nazwa dodawanego elementu xml</param>
		/// <param name="innerText">tekst wewnątrz elementu xml</param>
		/// <param name="namespacePrefix">prefix namespace'a</param>
		internal static XmlElement DodajElement(this XmlElement value, XmlElement parent, string nazwa, string innerText, string namespacePrefix)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(nazwa))
			{
				if (!string.IsNullOrEmpty(namespacePrefix))
				{
					xmlElement = parent.OwnerDocument.CreateElement(namespacePrefix, nazwa, parent.GetNamespaceOfPrefix(namespacePrefix));
				}
				else
				{
					xmlElement = parent.OwnerDocument.CreateElement(nazwa);
				}

				if (!string.IsNullOrEmpty(innerText))
				{
					xmlElement.InnerText = innerText;
				}
				if (xmlElement != null) value.AppendChild(xmlElement);
			}

			return xmlElement;
		}

		/// <summary>
		/// Dodaje podrzędny element z tekstem wewnętrznym podanym w parametrze o ile innerText jest niepusta
		/// </summary>
		/// <param name="nazwa">nazwa dodawanego elementu xml</param>
		/// <param name="innerText">tekst wewnątrz elementu xml</param>
		/// <param name="namespacePrefix">prefix namespace'a</param>
		internal static XmlElement DodajElement(this XmlElement value, string nazwa, string innerText, string namespacePrefix)
		{
			return value.DodajElement(value.OwnerDocument.DocumentElement, nazwa, innerText, namespacePrefix);
		}

		/// <summary />
		internal static XmlElement DodajElement<TProperty>(this XmlElement value, XmlElement parent, Expression<Func<TProperty>> property)
		{
			XmlElement xmlElement = null;

			var lambda = (LambdaExpression)property;

			MemberExpression memberExpression;
			if (lambda.Body is UnaryExpression)
			{
				var unaryExpression = (UnaryExpression)lambda.Body;
				memberExpression = (MemberExpression)unaryExpression.Operand;
			}
			else
				memberExpression = (MemberExpression)lambda.Body;

			var att = memberExpression.Member.GetCustomAttributes(typeof(HL7XmlNameAttribute), false).SingleOrDefault() as HL7XmlNameAttribute;

			var propValue = property.Compile()() as string;

			if (att != null && !string.IsNullOrEmpty(propValue))
			{
				if (!string.IsNullOrEmpty(att.Format))
				{
					xmlElement = value.DodajElement(parent, att.Name, string.Format(att.Format, propValue));
				}
				else
				{
					xmlElement = value.DodajElement(parent, att.Name, propValue);
				}
			}

			return xmlElement;
		}

		/// <summary />
		internal static XmlElement DodajElement<TProperty>(this XmlElement value, Expression<Func<TProperty>> property)
		{
			return value.DodajElement(value.OwnerDocument.DocumentElement, property);
		}

		/// <summary />
		internal static XmlElement DodajElement(this XmlElement value, XmlElement parent, IElementSzablonu element, string namespacePrefix)
		{
			XmlElement xmlElement = null;

			if (element != null)
			{
				xmlElement = element.TworzXmlElement(parent);
				if (xmlElement != null) value.AppendChild(xmlElement);
			}

			return xmlElement;
		}

		/// <summary />
		internal static XmlElement DodajElement(this XmlElement value, XmlElement parent, IElementSzablonu element)
		{
			return value.DodajElement(parent, element, null);
		}

		/// <summary />
		internal static XmlElement DodajElement(this XmlElement value, IElementSzablonu element)
		{
			return value.DodajElement(value.OwnerDocument.DocumentElement, element, null);
		}

		/// <summary />
		internal static XmlElement DodajElement(this XmlElement value, IElementSzablonu element, string namespacePrefix)
		{
			return value.DodajElement(value.OwnerDocument.DocumentElement, element, namespacePrefix);
		}
	}
}
