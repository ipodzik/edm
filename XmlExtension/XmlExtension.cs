﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	public static class XmlExtension
	{
		/// <summary>
		/// Dodaje atrybut do elementu xml będący jego identyfikatorem 
		/// </summary>
		/// <param name="parent">element xml</param>
		/// <param name="identyfikatorXml">identyfikator elementu xml</param>
		internal static XmlElement DodajIdentyfikatorElementu(this XmlElement value, string wartosc)
		{
			return value.UstawAtrybut("ID", null, wartosc);
		}



		/// <summary>
		/// Tworzy i dodaje do elementu sekcję CDATA
		/// </summary>
		/// <param name="data">wartość sekcji CDATA</param>
		internal static XmlElement DodajSekcjeCData(this XmlElement value, string data)
		{
			value.AppendChild(value.OwnerDocument.DocumentElement.OwnerDocument.CreateCDataSection(data));
			return value;
		}






		/// <summary>
		/// Dodaje element xml
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="szablon">szablon podrzędny zawarty w danym szablonie</param>
		internal static void DodajSzablon(this XmlElement value, XmlElement parent, Szablon.SzablonSkladowy szablon)
		{
			value.AppendChild(szablon.TworzXmlElement(parent));
		}

		/// <summary>
		/// Dodaje element xml
		/// </summary>
		/// <param name="szablon">szablon podrzędny zawarty w danym szablonie</param>
		internal static void DodajSzablon(this XmlElement value, Szablon.SzablonSkladowy szablon)
		{
			value.AppendChild(szablon.TworzXmlElement(value.OwnerDocument.DocumentElement));
		}
	}
}
