﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	public static class XmlElementAtrybut
	{
		/// <summary>
		/// Ustawia atrybut do elementu xml
		/// </summary>
		/// <param name="parent">element xml</param>
		/// <param name="nazwa">nazwa dodawanego atrybutu</param>
		/// <param name="wartosc">wartość atrybutu xml</param>
		internal static XmlElement UstawAtrybut(this XmlElement value, string nazwa, string wartosc)
		{
			return value.UstawAtrybut(nazwa, null, wartosc);
		}

		/// <summary>
		/// Ustawia atrybut do elementu xml
		/// </summary>
		/// <param name="parent">element xml</param>
		/// <param name="nazwa">nazwa dodawanego atrybutu</param>
		/// <param name="nameSpace">namespace</param>
		/// <param name="wartosc">wartość atrybutu xml</param>
		internal static XmlElement UstawAtrybut(this XmlElement value, string nazwa, string nameSpace, string wartosc)
		{
			if (!string.IsNullOrEmpty(nazwa))
			{
				if (!string.IsNullOrEmpty(wartosc))
				{
					if (!string.IsNullOrEmpty(nameSpace))
					{
						value.SetAttribute(nazwa, value.OwnerDocument.DocumentElement.GetNamespaceOfPrefix(nameSpace), wartosc);
					}
					else
					{
						value.SetAttribute(nazwa, wartosc);
					}
				}
				else
				{
					var attr = value.Attributes[nazwa];
					if (attr != null)
					{
						value.Attributes.Remove(attr);
					}
				}
			}

			return value;
		}


		/// <summary>
		/// Ustawia atrybut do elementu xml
		/// </summary>
		internal static XmlElement UstawAtrybut<TProperty>(this XmlElement value, Expression<Func<TProperty>> property)
		{
			var lambda = (LambdaExpression)property;

			MemberExpression memberExpression;
			if (lambda.Body is UnaryExpression)
			{
				var unaryExpression = (UnaryExpression)lambda.Body;
				memberExpression = (MemberExpression)unaryExpression.Operand;
			}
			else
				memberExpression = (MemberExpression)lambda.Body;

			var att = memberExpression.Member.GetCustomAttributes(typeof(HL7XmlNameAttribute), false).SingleOrDefault() as HL7XmlNameAttribute;

			var propValue = property.Compile()() as string;

			if (att != null && !string.IsNullOrEmpty(propValue))
			{
				value.SetAttribute(att.Name, propValue);
			}

			return value;
		}
	}
}
