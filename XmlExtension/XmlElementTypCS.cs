﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Coded Simple Value
	/// </summary>
	public static class XmlExtensionTypCS
	{
		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będacy typem danych CS (Coded Simple Value)
		/// <code>&lt;status code="completed" /&gt;</code> 
		/// </summary>
		internal static XmlElement DodajTypCS(this XmlElement v)
		{
			var xmlElement = v.TworzElementXml("statusCode").UstawAtrybut("code", "completed");
			v.AppendChild(xmlElement);
			return xmlElement;
		}
	}
}
