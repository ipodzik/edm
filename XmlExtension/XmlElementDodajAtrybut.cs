﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	public static class XmlElementDodajAtrybut
	{
		/// <summary>
		/// Dodaje atrybut do elementu xml
		/// </summary>
		/// <param name="parent">element xml</param>
		/// <param name="nazwa">nazwa dodawanego atrybutu</param>
		/// <param name="wartosc">wartość atrybutu xml</param>
		internal static XmlElement DodajAtrybut(this XmlElement value, string nazwa, string wartosc)
		{
			return value.DodajAtrybut(nazwa, null, wartosc);
		}

		/// <summary>
		/// Dodaje atrybut do elementu xml
		/// </summary>
		/// <param name="parent">element xml</param>
		/// <param name="nazwa">nazwa dodawanego atrybutu</param>
		/// <param name="nameSpace">namespace</param>
		/// <param name="wartosc">wartość atrybutu xml</param>
		internal static XmlElement DodajAtrybut(this XmlElement value, string nazwa, string nameSpace, string wartosc)
		{
			if (!string.IsNullOrEmpty(wartosc) && !string.IsNullOrEmpty(nazwa))
			{
				if (!string.IsNullOrEmpty(nameSpace))
				{
					value.SetAttribute(nazwa, value.OwnerDocument.DocumentElement.GetNamespaceOfPrefix(nameSpace), wartosc);
				}
				else
				{
					value.SetAttribute(nazwa, wartosc);
				}
			}

			return value;
		}


		internal static XmlElement DodajAtrybut<TProperty>(this XmlElement value, Expression<Func<TProperty>> property)
		{
			var lambda = (LambdaExpression)property;

			MemberExpression memberExpression;
			if (lambda.Body is UnaryExpression)
			{
				var unaryExpression = (UnaryExpression)lambda.Body;
				memberExpression = (MemberExpression)unaryExpression.Operand;
			}
			else
				memberExpression = (MemberExpression)lambda.Body;

			var att = memberExpression.Member.GetCustomAttributes(typeof(HL7XmlNameAttribute), false).SingleOrDefault() as HL7XmlNameAttribute;

			var propValue = property.Compile()() as string;

			if (att != null && !string.IsNullOrEmpty(propValue))
			{
				value.SetAttribute(att.Name, propValue);
			}

			return value;
		}
	}
}
