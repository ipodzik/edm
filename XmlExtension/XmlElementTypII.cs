﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon;
using System.Linq.Expressions;



namespace BIUINF.Lib4.HL7CDA
{
	/*
				https://art-decor.org/mediawiki/index.php?title=DTr1_II 
	*/
	/// <summary>
	/// Instance Identifier
	/// </summary>
	public static class XmlExtensionTypII
	{
		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="extension">wartość identyfikatora obiektu</param>
		/// <param name="root">zakres identyfikatora obiektu</param>
		internal static XmlElement DodajTypII(this XmlElement value, XmlElement parent, string extension, string root)
		{
			return value.DodajTypII(parent, extension, root, null, null);
		}

		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="extension">wartość identyfikatora obiektu</param>
		/// <param name="root">zakres identyfikatora obiektu</param>
		internal static XmlElement DodajTypII(this XmlElement value, string extension, string root)
		{
			return value.DodajTypII(value.OwnerDocument.DocumentElement, extension, root, null, null);
		}

		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="extension">wartość identyfikatora obiektu</param>
		/// <param name="root">zakres identyfikatora obiektu</param>
		internal static XmlElement DodajTypII(this XmlElement value, string extension, string root, bool? czyWyswietlany)
		{
			return value.DodajTypII(value.OwnerDocument.DocumentElement, extension, root, null, czyWyswietlany);
		}


		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="extension">wartość identyfikatora obiektu</param>
		/// <param name="root">zakres identyfikatora obiektu</param>
		/// <param name="xmlElementName">Nazwa elementu xml</param>
		internal static XmlElement DodajTypII(this XmlElement value, XmlElement parent, string extension, string root, string xmlElementName, bool? czyWyswietlany)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(!string.IsNullOrEmpty(xmlElementName) ? xmlElementName : "id");
			xmlElement.UstawAtrybut("extension", extension);
			xmlElement.UstawAtrybut("root", root);
			if (czyWyswietlany.HasValue)
			{
				xmlElement.SetAttribute("displayable", (czyWyswietlany.Value) ? "true" : "false");
			}
			value.AppendChild(xmlElement);
			return xmlElement;
		}

		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="extension">wartość identyfikatora obiektu</param>
		/// <param name="root">zakres identyfikatora obiektu</param>
		/// <param name="xmlElementName">Nazwa elementu xml</param>
		internal static XmlElement DodajTypII(this XmlElement value, string extension, string root, string xmlElementName)
		{
			return value.DodajTypII(value.OwnerDocument.DocumentElement, extension, root, xmlElementName, null);
		}


		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="parent">element xml będący parentem (OwnerDocument)</param>
		/// <param name="oid">identyfikator instancji</param>
		internal static XmlElement DodajTypII(this XmlElement value, XmlElement parent, IdentyfikatorInstancji oid)
		{
			var xmlElement = oid.TworzXmlElement(parent);
			value.AppendChild(xmlElement);
			return xmlElement;
		}

		/// <summary>
		/// Tworzy i dodaje jako element podrzędny, element xml będący typem danych II (Instance Identifier)
		/// </summary>
		/// <param name="oid">identyfikator instancji</param>
		internal static XmlElement DodajTypII(this XmlElement value, IdentyfikatorInstancji oid)
		{
			return value.DodajTypII(value.OwnerDocument.DocumentElement, oid);
		}
	}
}
