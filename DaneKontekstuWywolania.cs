﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;
using medinf = BIUINF.Lib4.HL7CDA.MedInf;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary>
	/// Informacja o kontekście wywołania usługi 
	/// </summary>
	public class DaneKontekstuWywolania
	{
		/// <summary>
		/// Identyfikator podmiotu wywołującego usługę 
		/// </summary>
		public Podmiot Podmiot { get { return this.podmiot; } }
		private readonly Podmiot podmiot;

		/// <summary>
		/// Identyfikator pracownika wywołującego usługę 
		/// </summary>
		public IdentyfikatorPersonelu Personel { get { return this.personel; } }
		private readonly IdentyfikatorPersonelu personel;

		/// <summary>
		/// Identyfikator komórki organizacyjnej, w ramach której wywoływana jest usługa 
		/// </summary>
		public KomorkaOrganizacyjna KomorkaOrganizacyjna { get { return this.komorkaOrganizacyjna; } }
		private readonly KomorkaOrganizacyjna komorkaOrganizacyjna;

		/// <summary>
		/// Informacja o roli biznesowej 
		/// </summary>
		public string RolaBiznesowa { get { return this.rolaBiznesowa; } }
		private readonly string rolaBiznesowa;



		internal DaneKontekstuWywolania(IdentyfikatorPersonelu personel, Podmiot podmiot, KomorkaOrganizacyjna komorkaOrganizacyjna)
		{
			if (string.IsNullOrEmpty(personel.Extension))
			{
				throw medinf.BrakDanychMedInfExclamationException.Create("Brak identyfikatora personelu w kontekście wywołania!");
			}
			this.personel = personel;

			if (string.IsNullOrEmpty(podmiot.Extension))
			{
				throw medinf.BrakDanychMedInfExclamationException.Create("Brak identyfikatora podmiotu w kontekście wywołania!");
			}
			this.podmiot = podmiot;

			if (string.IsNullOrEmpty(komorkaOrganizacyjna.Extension))
			{
				throw medinf.BrakDanychMedInfExclamationException.Create("Brak identyfikatora komórki w kontekście wywołania!");
			}
			this.komorkaOrganizacyjna = komorkaOrganizacyjna;

			if (personel.ZbiorWartosci == ZbiorWartosci.NpwzLekarzyDentystowFelczerow)
			{
				this.rolaBiznesowa = "LEKARZ_LEK_DENTYSTA_FELCZER";
			}
			else if (personel.ZbiorWartosci == ZbiorWartosci.NpwzPielegniarekPoloznych)
			{
				this.rolaBiznesowa = "PIELEGNIARKA_POLOZNA";
			}
		}
	}
}
