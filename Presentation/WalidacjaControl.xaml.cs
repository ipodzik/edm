﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BIUINF.Lib4.HL7CDA.Presentation
{
	/// <summary>
	/// Interaction logic for WalidacjaControl.xaml
	/// </summary>
	public partial class WalidacjaControl : UserControl
	{
		public WalidacjaControl()
		{
			InitializeComponent();
		}

		public void UstawPodgladWalidacji(List<string> ValidationErrors)
		{		
			
				ValidationListView.ItemsSource = ValidationErrors;			
			
		}

		private void btnKopiuj_Click(object sender, RoutedEventArgs e)
		{
			
			foreach(var item in ValidationListView.Items)
			{
				//kopiowanie listy błędów do schowka
				Clipboard.SetText(item.ToString());
			}
			
		}
	}
}
