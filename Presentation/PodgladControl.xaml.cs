﻿using System.Text;
using System.Windows.Controls;
using System.Xml;

namespace BIUINF.Lib4.HL7CDA.Presentation
{
	/// <summary>
	/// Interaction logic for PodgladControl.xaml
	/// </summary>
	internal partial class PodgladControl : UserControl
	{

		public PodgladControl()
		{
			InitializeComponent();
		}

		public void UstawDokumentDoPodgladu(XmlDocument eDok)
		{
			StringBuilder html = new StringBuilder();
			
				string htmlRec = Transformaty.Konwerter.GenerujPodglad(eDok);
				html.Append(htmlRec);
			
			web.NavigateToString(html.ToString());
			web.Visibility = System.Windows.Visibility.Visible;
		}
	}
}
