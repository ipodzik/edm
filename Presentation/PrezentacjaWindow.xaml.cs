﻿using System.ComponentModel;
using System.Windows;
using System.Xml;
using System.Collections.Generic;

namespace BIUINF.Lib4.HL7CDA.Presentation
{
	/// <summary>
	/// Interaction logic for PrezentacjaWindow.xaml
	/// </summary>
	internal partial class PrezentacjaWindow : Window
	{

		public PrezentacjaWindow(XmlDocument eDok, List<string> ValidationErrors)
		{
			InitializeComponent();
			ctrlPodglad.UstawDokumentDoPodgladu(eDok);

			if(ValidationErrors.Count == 0)
			{
				ctrlWalidacja.ValidationListView.ItemsSource = new List<string>{"Brak błędów"};
				//grpWalidacja.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				ctrlWalidacja.UstawPodgladWalidacji(ValidationErrors);
				//grpWalidacja.Visibility = System.Windows.Visibility.Visible;
			}
			
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void btnZamknij_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			DialogResult = true;
		}

		
	}
}
