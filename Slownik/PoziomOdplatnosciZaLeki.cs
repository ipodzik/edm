﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/voc-2.16.840.1.113883.3.4424.13.11.16-2014-06-06T000000.html
	*/
	[OidEnum(SystemKodowania.PoziomOdplatnosciZaLeki, "2.16.840.1.113883.3.4424.13.11.16", "Poziom odpłatności za leki", "Poziom odpłatności za leki refundowane")]
	public enum PoziomOdplatnosciZaLeki
	{
		[Enum("B", "bezpłatne")]
		Bezplatne = 1,
		
		[Enum("R", "ryczałt")]
		Ryczalt = 2,
		
		[Enum("30%", "30% limitu")]
		Odplatnosc30Procent = 3,
		
		[Enum("50%", "50% limitu")]
		Odplatnosc50Procent = 4,

		[Enum("100%", "pełnopłatne")]
		Pelnoplatne = 5
	}
}
