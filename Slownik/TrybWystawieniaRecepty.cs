﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
	 * https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/voc-2.16.840.1.113883.3.4424.13.11.7-2014-06-06T000000.html
	 * 
	 * (numeracja zgodna z bazą - recepta.def_tryb_wystawienia_erecepty
	*/
	[OidEnum(SystemKodowania.PolskieKlasyfikatoryHL7v3, "2.16.840.1.113883.3.4424.13.11.7", "Tryb wystawienia recepty", "Klasyfikacja recept ze względu na tryb wystawienia recepty: zwykła, farmaceutyczna, pielęgniarska czy pielęgniarska na zlecenie lekarza")]
	public enum TrybWystawieniaRecepty
	{
		[Enum("Z", "Zwykła")]
		Zwykla = 1,

		[Enum("F", "Farmaceutyczna")]
		Farmaceutyczna = 2,

		[Enum("P", "Pielęgniarska")]
		Pielegniarska = 3,

		[Enum("PL", "Pielęgniarska na zlecenie lekarza")]
		PielegniarskaNaZlecenia = 4
	}
}
