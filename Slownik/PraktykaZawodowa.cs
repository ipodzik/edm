﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	[HL7XmlName("code")]
	public class PraktykaZawodowa : PozycjaSlownika
	{
		public RodzajPraktykiZawodowej? Rodzaj
		{
			get { return this.rodzaj; }
			set
			{
				if (rodzaj != value)
				{
					this.rodzaj = value;
					if (value.HasValue)
					{
						this.Kod = this.rodzaj.GetKod();
						this.NazwaWyswietlana = this.rodzaj.GetNazwa();
					}
					else
					{
						this.Kod = null;
						this.NazwaWyswietlana = null;
					}
				}
			}
		}
		private RodzajPraktykiZawodowej? rodzaj;




		public PraktykaZawodowa() : base(Slownik.SystemKodowania.RodzajPraktykiZawodowej) { }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (this.Rodzaj.HasValue)
			{
				xmlElement = base.TworzXmlElement(parent, this);
			}

			return xmlElement;
		}
	}
}
