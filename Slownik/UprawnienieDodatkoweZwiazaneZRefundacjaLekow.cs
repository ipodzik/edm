﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
				 https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/voc-2.16.840.1.113883.3.4424.13.11.15-2017-04-26T000000.html
	*/
	[OidEnum(SystemKodowania.UprawnienieDodatkowe, "2.16.840.1.113883.3.4424.13.11.15", "Uprawnienie dodatkowe", "Uprawnienie dodatkowe związane z refundacją leków")]
	public enum UprawnienieDodatkoweZwiazaneZRefundacjaLekow
	{
		[Enum("AZ")]
		AZ,
		
		[Enum("BW")]
		BW,

		[Enum("CN")]
		CN,

		[Enum("DN")]
		DN,

		[Enum("IB")]
		IB,

		[Enum("IN")]
		IN,

		[Enum("IW")]
		IW,

		[Enum("PO")]
		PO,

		[Enum("ZK")]
		ZK,

		[Enum("S")]
		S
	}
}
