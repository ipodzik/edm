﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	[OidEnum("2.16.840.1.113883.5.1")]
	public enum Plec
	{
		[Enum("UN", "Niezróżnicowana (undifferentiated)", "Trudna do jednoznacznego określenia")]
		Nieznana,
		[Enum("F", "Kobieta")]
		Kobieta,
		[Enum("M", "Meżczyzna")]
		Mezczyzna
	}
}
