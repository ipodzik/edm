﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;


namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
			https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/voc-2.16.840.1.113883.3.4424.13.11.36-2015-10-26T000000.html
	*/
	[OidEnum(SystemKodowania.TrybWypisuZeSzpitala, "2.16.840.1.113883.3.4424.13.11.36", "Tryb wypisu ze szpitala", "Tryb wypisu ze szpitala")]

	public enum TrybWypisuZeSzpitala
	{
		[Enum("1","zakończenie procesu terapeutycznego lub diagnostycznego")]
		ZakończenieProcesu,

		[Enum("2","skierowanie do dalszego leczenia w lecznictwie ambulatoryjnym")]
		DalszeLeczenie_Ambulatorium,
		
		[Enum("3","skierowanie do dalszego leczenia w innym szpitalu")]
		DalszeLeczenie_InnySzpital,

		[Enum("4","skierowanie do dalszego leczenia w innym niż szpital, zakładzie opieki stacjonarnej")]
		DalszeLeczenie_ZOS,

		[Enum("6","wypisanie na własne żądanie")]
		WypisNaŻądanie,

		[Enum("7","osoba leczona samowolnie opuściła zakład opieki stacjonarnej przed zakończeniem procesu terapeutycznego lub diagnostycznego")]
		WypisSamowolny,

		[Enum("8","wypisanie na podstawie art. 22 ust. 1 pkt 3 ustawy o zakładach opieki zdrowotnej")]
		WypisWgUstawyArt22,

		[Enum("9","zgon pacjenta")]
		ZgonPacjenta,

		[Enum("10","osoba leczona, przyjęta w trybie oznaczonym kodem „9” lub „10”, która samowolnie opuściła podmiot leczniczy")]
		WypisSamowolnyOsoby9lub10,

		[Enum("11","wypisanie na podstawie art. 46 albo 47 ustawy z dnia 22 listopada 2013 r.")]
		WypisWgUstawyArt46_47
	}
}
