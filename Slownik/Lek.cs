﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/// <summary>
	/// Kontener na dane o leku
	/// </summary>
	[HL7XmlName("code")]
	public class Lek : PozycjaSlownika
	{
		public string Nazwa { get { return base.NazwaWyswietlana; } set { base.NazwaWyswietlana = value; } }
		public new string Kod { get { return base.Kod; } set { base.Kod = value; } }
		/// <summary>
		/// Id referencji w pliku xml
		/// </summary>
		internal string ReferencjaId { get; set; }

		public Lek() : base(OID.ZbiorWartosci.LekiUrplId) { }

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			return base.TworzXmlElement(parent, this);
		}
	}
}
