﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/// <summary>
	/// System kodowania - lista słowników utworzonych celowo na potrzeby IG 
	/// </summary>
	/// <remarks>PIK HL7 CDA - zakładka Słowniki</remarks>
	public enum SystemKodowania
	{
		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.21
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.11.3.21", "Tryb wypisu ze szpitala")]
		TrybWypisuZeSzpitala,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.1.1
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.11.1.1", "Poziomy odpłatności leków refundowanych")]
		PoziomOdplatnosciZaLeki,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.1.25
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.11.1.25", "Kategoria dostępności leku")]
		KategoriaDostepnosciLeku,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.1
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.11.3.1", "Uprawnienia dodatkowe związane z refundacją leków")]
		UprawnienieDodatkowe,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.18
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.11.3.18", "Zawody medyczne")]
		ZawodyMedyczne,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.11.3.19
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.11.3.19", "RODZ_PRAKTYKI", "Rodzaj praktyki zawodowej")]
		RodzajPraktykiZawodowej,

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.5.1
		/// </summary>
		[OidEnum("2.16.840.1.113883.3.4424.13.5.1", "PolskieKlasyfikatoryHL7v3")]
		PolskieKlasyfikatoryHL7v3,

		/// <summary>
		/// 0.4.0.127.0.16.1.1.2.1
		/// </summary>
		[OidEnum("0.4.0.127.0.16.1.1.2.1", "EDQM")]
		EDQM,

		/// <summary>
		/// 1.3.160
		/// </summary>
		[OidEnum("1.3.160", "GS1")]
		GS1,

		/// <summary>
		/// 2.16.840.1.113883.6.3 - standardowy słownik ICD 10 
		/// </summary>
		[OidEnum("2.16.840.1.113883.6.3", "ICD10")]
		ICD10,

		/// <summary>
		/// 2.16.840.1.113883.6.260 - icd10DualCoding
		/// </summary>
		[OidEnum("2.16.840.1.113883.6.260", "ICD10DualCoding")]
		ICD10DualCoding

	}
}
