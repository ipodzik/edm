﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/// <summary>
	/// Ustalone z Rupim, że Slownik.Lek istniejące dotąd zostanie zamienione na Slownik.LekRecepty, a w dalszej kolejności wyeliminowane
	/// </summary>
	[HL7XmlName("code")]
	public class LekRecepty : PozycjaSlownika
	{
		private readonly Szablon.Recepta.Lek lek;
		private readonly string namespacePrefix;

		//internal Lek(Szablon.Recepta.Lek lek)
		//  : this(lek, null) { }

		internal LekRecepty(Szablon.Recepta.Lek lek, OID.ZbiorWartosci zbiorWartosci)
			: this(lek, null, zbiorWartosci) { }

		internal LekRecepty(Szablon.Recepta.Lek lek, string namespacePrefix)
			: this(lek, namespacePrefix, null) { }

		private LekRecepty(Szablon.Recepta.Lek lek, string namespacePrefix, OID.ZbiorWartosci? zbiorWartosci)
			: base()
		{
			this.lek = lek;
			this.namespacePrefix = namespacePrefix;

			if (zbiorWartosci.HasValue && zbiorWartosci.Value == OID.ZbiorWartosci.LekiUrplId)
			{
				this.Kod = this.lek.UrplId;
				this.UstalSystemKodowania(OID.ZbiorWartosci.LekiUrplId, true);
			}
			else
			{
				this.Kod = (!string.IsNullOrEmpty(this.lek.EAN)) ? this.lek.EAN : new String('0', 14);
				this.UstalSystemKodowania(OID.ZbiorWartosci.LekiEAN, true);
			}

			this.NazwaWyswietlana = this.lek.NazwaWyswietlana;
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.Kod))
			{
				xmlElement = base.TworzXmlElement(parent, this, this.namespacePrefix);
			}

			return xmlElement;
		}
	}
}
