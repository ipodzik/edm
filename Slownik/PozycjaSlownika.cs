﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.Common;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	public abstract class PozycjaSlownika : IElementSzablonu
	{
		public ZbiorWartosci? ZbiorWartosci { get { return this.zbiorWartosci; } }
		private ZbiorWartosci? zbiorWartosci;

		public SystemKodowania? SystemKodowania { get { return this.systemKodowania; } }
		private SystemKodowania? systemKodowania;

		[HL7XmlName("codeSystem")]
		protected string KodSystemu { get; private set; }
		[HL7XmlName("codeSystemName")]
		protected string KodSystemuNazwa { get; private set; }
		//[HL7XmlName("codeSystemVersion")]
		//protected string KodSystemuWersja { get; private set; } 
		[HL7XmlName("code")]
		protected string Kod { get; set; }
		[HL7XmlName("displayName")]
		protected string NazwaWyswietlana { get; set; }



		protected PozycjaSlownika() { }

		protected PozycjaSlownika(ZbiorWartosci zbiorWartosci)
		{
			this.UstalSystemKodowania(zbiorWartosci);
		}

		protected PozycjaSlownika(SystemKodowania systemKodowania)
		{
			//this.UstalSystemKodowania(systemKodowania);
			this.systemKodowania = systemKodowania;
			var oid = this.systemKodowania.GetOidEnumAttribute();
			this.KodSystemu = oid.Kod;
			this.KodSystemuNazwa = oid.Nazwa;
		}



		protected void UstalSystemKodowania(ZbiorWartosci zbiorWartosci)
		{
			this.UstalSystemKodowania(zbiorWartosci, true);
		}

		protected void UstalSystemKodowania(ZbiorWartosci zbiorWartosci, bool czySystemKodowania)
		{
			var zakres = zbiorWartosci.GetNadzbiorZbioruWartosci(true);

			if (zakres.HasValue)
			{
				if (zakres != NadzbiorZbioruWartosci.Slowniki && zakres != NadzbiorZbioruWartosci.PIK && zakres != NadzbiorZbioruWartosci.SlownikiP1 && zakres != NadzbiorZbioruWartosci.Leki)
				{
					throw new ErrorException("Niedozwolony zbiór (pula) wartości!");
				}
			}
			else
			{
				throw new ErrorException("Brak określenia zbioru (puli) wartości!");
			}

			this.zbiorWartosci = zbiorWartosci;

			var oid = this.zbiorWartosci.GetOidEnumAttribute();
			this.KodSystemu = oid.Kod;
			if (oid.SystemKodowania.HasValue && czySystemKodowania)
			{
				this.KodSystemuNazwa = oid.SystemKodowania.Value.GetNazwa();
				//this.UstalSystemKodowania(oid.SystemKodowania);
			}
		}

		//protected void UstalSystemKodowania(SystemKodowania? systemKodowania)
		//{
		//  if (systemKodowania.HasValue)
		//  {
		//    this.systemKodowania = systemKodowania;
		//    var oid = this.systemKodowania.GetOidEnumAttribute();
		//    this.KodSystemu = oid.Kod;
		//    if (oid.SystemKodowania.HasValue)
		//    {
		//      this.KodSystemuNazwa = oid.SystemKodowania.Value.GetNazwa();
		//    }
		//  }
		//}



		public virtual XmlElement TworzXmlElement(XmlElement parent)
		{
			throw new NotImplementedException();
		}

		protected XmlElement TworzXmlElement(XmlElement parent, IElementSzablonu elementSzablonu)
		{
			return this.TworzXmlElement(parent, elementSzablonu, null);
		}

		protected XmlElement TworzXmlElement(XmlElement parent, IElementSzablonu elementSzablonu, string namespacePrefix)
		{
			var xmlElement = parent.TworzElementXml(elementSzablonu, namespacePrefix);

			xmlElement.UstawAtrybut(() => this.Kod);
			xmlElement.UstawAtrybut(() => this.KodSystemu);
			xmlElement.UstawAtrybut(() => this.KodSystemuNazwa);
			xmlElement.UstawAtrybut(() => this.NazwaWyswietlana);

			return xmlElement;
		}
	}
}
