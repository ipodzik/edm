﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/voc-2.16.840.1.113883.3.4424.13.11.85-2018-09-30T000000.html
	 */
	[OidEnum(SystemKodowania.PolskieKlasyfikatoryHL7v3, ZbiorWartosci.RodzajReceptyElektronicznej)]
	public enum RodzajReceptyElektronicznej
	{
		/// <summary>
		/// zwykła
		/// </summary>
		[Enum("Rp", "zwykła")]
		ZA,

		/// <summary>
		/// pro auctore
		/// </summary>
		[Enum("PA", "pro auctore")]
		PA,

		/// <summary>
		/// pro familia
		/// </summary>
		[Enum("PF", "pro familia")]
		PF
	}
}
