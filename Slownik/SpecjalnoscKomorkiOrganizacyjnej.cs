﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	[HL7XmlName("standardIndustryClassCode")]
	public class SpecjalnoscKomorkiOrganizacyjnej : PozycjaSlownika
	{
		public string KodResortowy8
		{
			get { return this.Kod; }
			set { if (this.Kod != value) this.Kod = value; }
		}

		public string Nazwa
		{
			get { return this.NazwaWyswietlana; }
			set { this.NazwaWyswietlana = value; }
		}



		public SpecjalnoscKomorkiOrganizacyjnej() : base(OID.ZbiorWartosci.KomorkiOrganizacyjneSpecjalnosc) { }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.KodResortowy8))
			{
				xmlElement = base.TworzXmlElement(parent, this);
			}

			return xmlElement;
		}
	}
}
