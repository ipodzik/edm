﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/voc-2.16.840.1.113883.3.4424.13.11.8-2014-06-06T000000.html
	 */
	//[OidEnum(SystemKodowania.KategoriaDostepnosciLeku, "2.16.840.1.113883.3.4424.13.11.6", "Kategoria dostępności leku", "Klasyfikacja recept ze względu na kategorię dostępności leku")]
	[OidEnum(SystemKodowania.KategoriaDostepnosciLeku, ZbiorWartosci.KategoriaDostepnosciLeku)]
	public enum KategoriaDostepnosciLeku
	{
		[Enum("Rp", "Rp")]
		Rp,
		
		[Enum("Rpw", "Rpw")]
		Rpw,
		
		[Enum("Rpz", "Rpz")]
		Rpz
	}
}
