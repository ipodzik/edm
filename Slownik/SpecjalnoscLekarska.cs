﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	[HL7XmlName("code")]
	public class SpecjalnoscLekarska : PozycjaSlownika
	{
		private Queue<RodzajSpecjalnosciLekarskiej> rodzaje;



		public SpecjalnoscLekarska()
			: base()
		{
			this.rodzaje = new Queue<RodzajSpecjalnosciLekarskiej>();
		}



		public void DodajSpecjalnosc(RodzajSpecjalnosciLekarskiej rodzaj)
		{
			this.rodzaje.Enqueue(rodzaj);
			// wiem, ciulowo... 
			this.Kod = string.Join("_", this.rodzaje.Select(x => x.GetKod()));
			this.NazwaWyswietlana = string.Join(", ", this.rodzaje.Select(x => x.GetNazwa()));
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (this.rodzaje.Count > 0)
			{
				if (this.rodzaje.Count > 1)
				{
					base.UstalSystemKodowania(OID.ZbiorWartosci.MnogieSpecjalnosciLekarskich);
				}
				else
				{
					base.UstalSystemKodowania(OID.ZbiorWartosci.SpecjalnosciLekarskie);
				}
				xmlElement = base.TworzXmlElement(parent, this);
			}

			return xmlElement;
		}
	}




	[OidEnum("2.16.840.1.113883.3.4424.13.11.21")]
	public enum RodzajSpecjalnosciLekarskiej //TODO: uzupełnić 
	{
		[Enum("0731", "alergologia")]
		Alergologia,
		[Enum("0701", "anestezjologia i intensywna terapia")]
		AnestezjologiaIntensywnaTerapia,
		[Enum("0732", "angiologia")]
		Angiologia,
		[Enum("0733", "audiologia i foniatria")]
		AudiologiaFoniatria,
		[Enum("0734", "balneologia i medycyna fizykalna")]
		BalneologiaMedycynaFizykalna,
		[Enum("0702", "chirurgia dziecięca")]
		ChirurgiaDziecieca,
		[Enum("0735", "chirurgia klatki piersiowej")]
		ChirurgiaKlatkiPiersiowej,
		[Enum("0736", "chirurgia naczyniowa")]
		ChirurgiaNaczyniowa,
		[Enum("0703", "chirurgia ogólna")]
		ChirurgiaOgolna,
		[Enum("0737", "chirurgia onkologiczna")]
		ChirurgiaOnkologiczna,
		[Enum("0738", "chirurgia plastyczna")]
		ChirurgiaPlastyczna,
		[Enum("0704", "chirurgia szczękowo-twarzowa")]
		ChirurgiaSzczekowoTwarzowa
		//[Enum("", "")]


	}
}
