﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	[HL7XmlName("functionCode")]
	public class ZawodMedyczny : PozycjaSlownika
	{
		public RodzajZawoduMedycznego? Rodzaj
		{
			get { return this.rodzaj; }
			set
			{
				if (rodzaj != value)
				{
					this.rodzaj = value;
					if (value.HasValue)
					{
						this.Kod = this.rodzaj.GetKod();
						this.NazwaWyswietlana = this.rodzaj.GetNazwa();
					}
					else
					{
						this.Kod = null;
						this.NazwaWyswietlana = null;
					}
				}
			}
		}
		private RodzajZawoduMedycznego? rodzaj;



		public ZawodMedyczny() : base(OID.ZbiorWartosci.SlownikZawodowMedycznych) { }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (this.Rodzaj.HasValue)
			{
				xmlElement = base.TworzXmlElement(parent, this);
			}

			return xmlElement;
		}
	}



	//[OidEnum("2.16.840.1.113883.3.4424.13.11.37")]
	public enum RodzajZawoduMedycznego
	{
		[Enum("LEK", "Lekarz")]
		Lekarz,

		[Enum("FEL", "Felczer")]
		Felczer,

		[Enum("LEKD", "Lekarz dentysta")]
		LekarzDentysta,

		[Enum("PIEL", "Pielęgniarka")]
		Pielegniarka,

		[Enum("POL", "Położna")]
		Polozna,

		[Enum("FARM", "Farmaceuta")]
		Farmaceuta,

		[Enum("DLAB", "Diagnosta laboratoryjny")]
		DiagnostaLaboratoryjny,

		[Enum("TFARM", "Technik farmaceutyczny")]
		TechnikFarmaceutyczny
	}

}
