﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/voc-2.16.840.1.113883.3.4424.13.11.5-2017-10-26T000000.html 
	*/
	[OidEnum(SystemKodowania.PolskieKlasyfikatoryHL7v3, "2.16.840.1.113883.3.4424.13.11.5", "Rodzaj leku", "Klasyfikacja recept ze względu na rodzaj leku: lek gotowy, czy recepturowy")]
	public enum RodzajLeku
	{
		[Enum("G", "Lek gotowy")]
		LekGotowy,
		
		[Enum("R", "Lek recepturowy")]
		LekRecepturowy,
		
		[Enum("S", "Środek spożywczy specjalnego przeznaczenia żywieniowego")]
		SrodekSpozywczySpecjalnegoPrzeznaczeniaZywieniowego,
		
		[Enum("W", "Wyrób medyczny")]
		WyrobMedyczny
	}
}
