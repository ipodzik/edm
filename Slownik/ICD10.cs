﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Slownik
{
	/// <summary>
	/// Kontener na dane o rozpoznaniu
	/// </summary>
	[HL7XmlName("code")]
	public class ICD10 : PozycjaSlownika
	{
		/// <summary>
		/// Kod w klasyfikacji ICD-10
		/// </summary>
		public new string Kod { get { return base.Kod; } set { base.Kod = value; } }

		/// <summary>
		/// Opis w klasyfikacji ICD-10
		/// </summary>
		public string Opis { get { return base.NazwaWyswietlana; } set { base.NazwaWyswietlana = value; } }


		/// <summary>
		/// Id referencji w pliku xml
		/// </summary>
		internal string ReferencjaId { get; set; }

		/// <summary>
		/// Konstruktor
		/// </summary>
		public ICD10() : base(Slownik.SystemKodowania.ICD10) { }

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			return base.TworzXmlElement(parent, this);
		}
	}
}

