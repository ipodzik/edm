﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.Data;
using System.Data;

namespace BIUINF.Lib4.HL7CDA.MedInf
{
	internal class Adres
	{
		internal string Panstwo { get; set; }
		internal string Miejscowosc { get; set; }
		internal string Ulica { get; set; }
		internal string NrDomu { get; set; }
		internal string NrLokalu { get; set; }
		internal string Poczta { get; set; }
		internal string PocztaKod { get; set; }
		internal string TERYT { get; set; }
		internal string NrTelefonu { get; set; }



		/// <summary>
		/// Zwraca obiekt będący adresem pacjenta
		/// </summary>
		/// <param name="pacjent">pacjent</param>
		/// <returns>adres pacjenta</returns>
		internal static Adres Pobierz(Pacjent pacjent)
		{
			Adres ajtem = null;

			if (pacjent != null)
			{
				using (var conn = Library.CreateOpenedConnection(Library.DbIdMedInfPacjenci))
				{
					using (var cmd = conn.CreateCommand(CommandType.Text))
					{
						const int ID_PACJENT_ADRES = 0;
						const int PANSTWO          = 1;
						const int MIEJSCOWOSC      = 2;
						const int ULICA            = 3;
						const int NR_DOMU          = 4;
						const int NR_LOKALU        = 5;
						const int POCZTA           = 6;
						const int KOD_POCZTOWY     = 7;
						const int KOD_TERYTORIALNY = 8;

						cmd.CommandText =
@"SELECT ID_pacjent_adres, dkp.nazwa as panstwo, 
	case when miejscowosc_ID is not null then sm.nazwa else miejscowosc end as miejscowosc, 
	ulica, nr_domu, nr_lokalu, poczta, kod_pocztowy, kod_terytorialny
FROM dane.pacjent_adres pa 
INNER JOIN slowniki.klasyfikacja.def_klasyfikacja_pozycja dkp on dkp.ID_klasyfikacja_pozycja = pa.klasyfikacja_panstwo_ID  
LEFT JOIN slowniki.dbo.slo_miejscowosc sm on sm.ID_miejscowosc = pa.miejscowosc_pl_ID 
LEFT JOIN slowniki.dbo.slo_miejscowosc_gus smg on smg.miejscowosc_ID = pa.miejscowosc_pl_ID 
WHERE klasyfikacja_adres_ID = 303 AND pacjent_ID = @pacjent_ID";

						Sql.AddParameter(cmd, "@pacjent_ID", DbType.Int32, pacjent.Id);

						using (var drd = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							if (drd.Read())
							{
								ajtem = new Adres();
								ajtem.Panstwo = Sql.GetString(drd.GetValue(PANSTWO), SqlNullable.Yes);
								ajtem.Ulica = Sql.GetString(drd.GetValue(ULICA), SqlNullable.Yes);
								ajtem.NrDomu = Sql.GetString(drd.GetValue(NR_DOMU), SqlNullable.Yes);
								ajtem.NrLokalu = Sql.GetString(drd.GetValue(NR_LOKALU), SqlNullable.Yes);
								ajtem.Miejscowosc = Sql.GetString(drd.GetValue(MIEJSCOWOSC), SqlNullable.Yes);
								ajtem.TERYT = Sql.GetString(drd.GetValue(KOD_TERYTORIALNY), SqlNullable.Yes);
								ajtem.PocztaKod = Sql.GetString(drd.GetValue(KOD_POCZTOWY), SqlNullable.Yes);
								if (!drd.IsDBNull(POCZTA) && ajtem.Miejscowosc != drd.GetString(POCZTA)) ajtem.Poczta = drd.GetString(POCZTA);
							}
						}
					}
				}
			}

			return ajtem;
		}

		/// <summary>
		/// Zwraca obiekt będący adresem świadczeniodawcy 
		/// </summary>
		/// <param name="pacjent">pacjent</param>
		/// <returns>adres świadczeniodawcy</returns>
		internal static Adres Pobierz(KomorkaOrganizacyjna komorkaOrganizacyjna) 
		{
			Adres ajtem = null;

			if (komorkaOrganizacyjna != null)
			{
				using (var conn = Library.CreateOpenedConnection(Library.DbIdSIoK))
				{
					using (var cmd = conn.CreateCommand(CommandType.StoredProcedure))
					{
						cmd.CommandText = "dbo.bp_swiadczeniodawca_dane";

						using (var drd = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							if (drd.Read())
							{
								ajtem = new Adres();
								ajtem.Ulica = Sql.GetString(drd.GetValue(drd.GetOrdinal("ulica")), SqlNullable.Yes);
								ajtem.NrDomu = Sql.GetString(drd.GetValue(drd.GetOrdinal("nr_domu")), SqlNullable.Yes);
								ajtem.NrLokalu = Sql.GetString(drd.GetValue(drd.GetOrdinal("nr_lokalu")), SqlNullable.Yes);
								ajtem.PocztaKod = Sql.GetString(drd.GetValue(drd.GetOrdinal("kod_pocztowy")), SqlNullable.Yes);
								ajtem.Miejscowosc = Sql.GetString(drd.GetValue(drd.GetOrdinal("miasto")), SqlNullable.Yes);
								ajtem.NrTelefonu = Sql.GetString(drd.GetValue(drd.GetOrdinal("telefon")), SqlNullable.Yes);  
							}
						}
					}
				}
			}

			return ajtem;
		}
	}
}
