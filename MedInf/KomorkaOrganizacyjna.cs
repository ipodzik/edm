﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BIUINF.Lib4.Data;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.MedInf
{
	internal class KomorkaOrganizacyjna
	{
		internal int Id { get; private set; }
		internal string Nazwa { get; private set; }
		internal string KodResortowy1 { get; private set; }
		internal string KodResortowy5 { get; private set; }
		internal string KodResortowy7 { get; private set; }
		internal string KodResortowy8 { get; private set; }
		internal string REGON9 { get; private set; }
		internal string REGON14 { get; private set; }
		internal Adres Adres { get; private set; }



		private static KomorkaOrganizacyjna komorkaOrganizacyjna;



		private KomorkaOrganizacyjna() { }



		internal static KomorkaOrganizacyjna Pobierz(int medInfId)
		{
			if (KomorkaOrganizacyjna.komorkaOrganizacyjna == null || KomorkaOrganizacyjna.komorkaOrganizacyjna.Id != medInfId)
			{
				KomorkaOrganizacyjna.komorkaOrganizacyjna = null;

				using (var conn = Library.CreateOpenedConnection(Library.DbIdSlowniki))
				{
					using (var cmd = conn.CreateCommand(CommandType.Text))
					{
						const int KOMORKA_ORGANIZACYJNA_ID = 0;
						const int NAZWA                    = 1;
						const int KOD_RESORTOWY1           = 2;
						const int KOD_RESORTOWY5           = 3;
						const int KOD_RESORTOWY7           = 4;
						const int KOD_RESORTOWY8           = 5;
						const int REGON9				           = 6;
						const int REGON14				           = 7;


						cmd.CommandText =
@"SELECT komorka_organizacyjna_ID, nazwa, kod_resortowy1, kod_resortowy5, kod_resortowy7, kod_resortowy8, REGON9, REGON14 
FROM dbo.bv_komorka_organizacyjna v 
WHERE v.komorka_organizacyjna_ID = @komorka_organizacyjna_ID";

						Sql.AddParameter(cmd, "@komorka_organizacyjna_ID", DbType.Int32, medInfId);

						using (var drd = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							if (drd.Read())
							{
								KomorkaOrganizacyjna.komorkaOrganizacyjna = new KomorkaOrganizacyjna();

								KomorkaOrganizacyjna.komorkaOrganizacyjna.Id = drd.GetInt32(KOMORKA_ORGANIZACYJNA_ID);
								KomorkaOrganizacyjna.komorkaOrganizacyjna.Nazwa = drd.GetString(NAZWA);
								KomorkaOrganizacyjna.komorkaOrganizacyjna.KodResortowy1 = Sql.GetString(drd.GetValue(KOD_RESORTOWY1), SqlNullable.Yes); 
								KomorkaOrganizacyjna.komorkaOrganizacyjna.KodResortowy5 = Sql.GetString(drd.GetValue(KOD_RESORTOWY5), SqlNullable.Yes); 
								KomorkaOrganizacyjna.komorkaOrganizacyjna.KodResortowy7 = Sql.GetString(drd.GetValue(KOD_RESORTOWY7), SqlNullable.Yes); 
								KomorkaOrganizacyjna.komorkaOrganizacyjna.KodResortowy8 = Sql.GetString(drd.GetValue(KOD_RESORTOWY8), SqlNullable.Yes); 
								KomorkaOrganizacyjna.komorkaOrganizacyjna.REGON9 = Sql.GetString(drd.GetValue(REGON9), SqlNullable.Yes);
								KomorkaOrganizacyjna.komorkaOrganizacyjna.REGON14 = Sql.GetString(drd.GetValue(REGON14), SqlNullable.Yes);
							}
						}
					}

					if (KomorkaOrganizacyjna.komorkaOrganizacyjna != null)
					{
						KomorkaOrganizacyjna.komorkaOrganizacyjna.Adres = Adres.Pobierz(KomorkaOrganizacyjna.komorkaOrganizacyjna);
					}
				}
			}

			return KomorkaOrganizacyjna.komorkaOrganizacyjna;
		}
	}
}
