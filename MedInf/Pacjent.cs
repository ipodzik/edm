﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BIUINF.Lib4.Data;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.MedInf
{
	internal class Pacjent
	{
		internal int Id { get; private set; }
		internal IdentyfikatorPacjenta Identyfikator { get; private set; }
		internal Adres Adres { get; private set; }

		internal string Imie { get; private set; }
		internal string Imie2 { get; private set; }
		internal string Nazwisko { get; private set; }
		internal DateTime Urodzony { get; private set; }
		internal Plec? Plec { get; private set; }
		internal string KodOddzialuNFZ { get; private set; }
		


		private static Pacjent pacjent;



		internal static Pacjent Pobierz(int medInfId)
		{
			if (Pacjent.pacjent == null || Pacjent.pacjent.Id != medInfId)
			{
				Pacjent.pacjent = null;

				using (var conn = Library.CreateOpenedConnection(Library.DbIdMedInfPacjenci))
				{
					using (var cmd = conn.CreateCommand(CommandType.Text))
					{
						const int ID_PACJENT           = 0;
						const int NAZWISKO             = 1;
						const int IMIE                 = 2;
						const int IMIE2                = 3;
						const int PESEL                = 4;
						const int URODZONY             = 5;
						const int KLASYFIKACJA_PLEC_ID = 6;
						const int OW_NFZ_KOD           = 7;

						cmd.CommandText =
@"SELECT p.ID_pacjent, p.nazwisko, p.imie, p.imie2, p.pesel, p.urodzony, p.klasyfikacja_plec_ID, pc.wartosc as ow_nfz_kod
FROM dbo.pacjent p 
LEFT JOIN dane.pacjent_cecha pc ON pc.pacjent_ID = p.ID_pacjent AND pc.cecha_ID = 6 
WHERE p.ID_pacjent = @ID_pacjent";

						Sql.AddParameter(cmd, "@ID_pacjent", DbType.Int32, medInfId);

						using (var drd = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							if (drd.Read())
							{
								Pacjent.pacjent = new Pacjent();

								Pacjent.pacjent.Id = drd.GetInt32(ID_PACJENT);
								Pacjent.pacjent.Nazwisko = drd.GetString(NAZWISKO);
								Pacjent.pacjent.Imie = drd.GetString(IMIE);

								Pacjent.pacjent.Identyfikator = new IdentyfikatorPacjenta();

								if (!drd.IsDBNull(IMIE2)) Pacjent.pacjent.Imie2 = drd.GetString(IMIE2);
								if (!drd.IsDBNull(PESEL))
								{
									Pacjent.pacjent.Identyfikator.UstawWartosc(ZbiorWartosci.IdentyfikatoryOsobUePolska, drd.GetString(PESEL));
								}
								else
								{
									throw new NotImplementedException("Recepta dla osoby bez numeru PESEL nie może być zrealizowana.");
								}
								Pacjent.pacjent.Urodzony = drd.GetDateTime(URODZONY);
								switch (drd.GetInt32(KLASYFIKACJA_PLEC_ID))
								{
									case 35:
										{
											Pacjent.pacjent.Plec = HL7CDA.Slownik.Plec.Mezczyzna;
											break;
										}
									case 36:
										{
											Pacjent.pacjent.Plec = HL7CDA.Slownik.Plec.Kobieta;
											break;
										}
									default:
										{
											Pacjent.pacjent.Plec = HL7CDA.Slownik.Plec.Nieznana;
											break;
										}
								}
								if (!drd.IsDBNull(OW_NFZ_KOD)) Pacjent.pacjent.KodOddzialuNFZ = drd.GetString(OW_NFZ_KOD);
							}
						}
					}

					if (Pacjent.pacjent != null)
					{
						Pacjent.pacjent.Adres = Adres.Pobierz(Pacjent.pacjent);
					}
				}
			}

			return Pacjent.pacjent;
		}
	}
}
