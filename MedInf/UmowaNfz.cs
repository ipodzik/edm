﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using BIUINF.Lib4.Data;



namespace BIUINF.Lib4.HL7CDA.MedInf
{
	internal class UmowaNfz
	{
		internal string Nr { get; private set; }
		internal int KomorkaOrganizacyjnaId { get; private set; }
		internal int KontrahentId { get; private set; }
		internal string PlatnikKod { get; private set; }


		private static UmowaNfz umowaNfz;



		private UmowaNfz() { }



		internal static UmowaNfz Pobierz(int komorkaOrganizacyjnaId)
		{
			if (UmowaNfz.umowaNfz == null || UmowaNfz.umowaNfz.KomorkaOrganizacyjnaId != komorkaOrganizacyjnaId)
			{
				UmowaNfz.umowaNfz = null;

				using (DbConnection conn = Library.CreateOpenedConnection(Library.DbIdSIoK))
				{
					using (DbCommand cmd = conn.CreateCommand(CommandType.Text))
					{
						cmd.CommandText =
@"SELECT TOP 1 k.nr, skfk.kontrahent_ID, sk_platnik.symbol platnik_kod 
FROM siok.slo_kontrahent_funkcja_kontrahenta_powiazanie skfkp 
INNER JOIN siok.slo_kontrahent_funkcja_kontrahenta skfk ON skfk.ID_kontrahent_funkcja_kontrahenta = skfkp.kontrahent_funkcja_kontrahenta_ID AND skfk.funkcja_kontrahenta_ID = 3
INNER JOIN siok.kontrakt_usluga_miejsce_wykonywania AS kumw on kumw.kontrahent_ID = skfk.kontrahent_ID 
INNER JOIN siok.kontrakt_usluga AS ku ON kumw.kontrakt_usluga_ID = ku.ID_kontrakt_usluga 
INNER JOIN siok.kontrakt AS k ON ku.kontrakt_ID = k.ID_kontrakt 
INNER JOIN siok.slo_kontrahent sk_platnik on sk_platnik.ID_kontrahent = k.kontrahent_ID 
WHERE skfkp.powiazanie_ID = @powiazanie_ID AND (CONVERT(DATE, GETDATE()) BETWEEN K.obowiazuje_od AND K.obowiazuje_do) AND K.rodzaj_kontraktu_ID = 4;";

						Sql.AddParameter(cmd, "@powiazanie_ID", DbType.Int32, komorkaOrganizacyjnaId);

						const int NR            = 0;
						const int KONTRAHENT_ID = 1;
						const int PLATNIK_KOD   = 2;

						using (DbDataReader drd = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							if (drd.Read())
							{
								UmowaNfz.umowaNfz = new UmowaNfz();
								UmowaNfz.umowaNfz.KontrahentId = drd.GetInt32(KONTRAHENT_ID);
								UmowaNfz.umowaNfz.Nr = drd.GetString(NR);
								UmowaNfz.umowaNfz.KomorkaOrganizacyjnaId = komorkaOrganizacyjnaId;
								UmowaNfz.umowaNfz.PlatnikKod = Sql.GetString(drd.GetValue(PLATNIK_KOD), SqlNullable.Yes);
							}
						}
					}
				}

			}

			return UmowaNfz.umowaNfz;
		}
	}
}
