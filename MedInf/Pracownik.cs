﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;
using BIUINF.Lib4.Data;



namespace BIUINF.Lib4.HL7CDA.MedInf
{
	internal class Pracownik
	{
		internal int Id { get; private set; }
		internal string Nazwisko { get; private set; }
		internal string Imie { get; private set; }
		internal IdentyfikatorPersonelu Identyfikator { get; private set; }
		internal string TypPersoneluKod { get; private set; }
		internal RodzajZawoduMedycznego RodzajZawoduMedycznego { get; private set; }


		private static Pracownik pracownik;


		private Pracownik() { }



		internal static Pracownik Pobierz(int medInfId, bool czySerwisantDozwolony = false)
		{
			if (Pracownik.pracownik == null || Pracownik.pracownik.Id != medInfId)
			{
				Pracownik.pracownik = null;
				string dbId = (medInfId < 0 && czySerwisantDozwolony) ? Library.DbIdStart : Library.DbIdKadry;
				using (var conn = Library.CreateOpenedConnection(dbId))
				{
					using (var cmd = conn.CreateCommand(CommandType.Text))
					{
						const int PRACOWNIK_ID           = 0;
						const int NAZWISKO               = 1;
						const int IMIE                   = 2;
						const int IDENTYFIKATOR          = 3;
						const int TYP_IDENTYFIKATORA_KOD = 4;
						const int TYP_PERSONELU_KOD      = 5;
						if (medInfId < 0 && czySerwisantDozwolony)
						{
							cmd.CommandText =
	@"SELECT ID_uzytkownik AS pracownik_ID, nazwa as nazwisko, '' AS imie, '' AS identyfikator, null AS typ_identyfikatora_kod, '' AS typ_personelu_kod 
FROM uwierzytelnianie.uzytkownik 
WHERE ID_uzytkownik = @pracownik_ID;";
						}
						else
						{
							cmd.CommandText =
	@"SELECT v.pracownik_ID, v.nazwisko, v.imie, v.identyfikator, v.typ_identyfikatora_kod, v.typ_personelu_kod 
FROM bv_pracownik v 
WHERE v.pracownik_ID = @pracownik_ID;";
						}
						Sql.AddParameter(cmd, "@pracownik_ID", DbType.Int32, medInfId);

						using (var drd = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							if (drd.Read())
							{
								Pracownik.pracownik = new Pracownik();

								Pracownik.pracownik.Id = medInfId;
								Pracownik.pracownik.Nazwisko = drd.GetString(NAZWISKO);
								Pracownik.pracownik.Imie = drd.GetString(IMIE);

								if (!drd.IsDBNull(TYP_PERSONELU_KOD))
								{
									Pracownik.pracownik.TypPersoneluKod = drd.GetString(TYP_PERSONELU_KOD);

									switch (Pracownik.pracownik.TypPersoneluKod)
									{
										case "5": // felczer
											{
												Pracownik.pracownik.Identyfikator = new IdentyfikatorPersonelu();
												Pracownik.pracownik.Identyfikator.UstawWartosc(ZbiorWartosci.NpwzLekarzyDentystowFelczerow, drd.GetString(IDENTYFIKATOR));
												Pracownik.pracownik.RodzajZawoduMedycznego = RodzajZawoduMedycznego.Felczer;
												break;
											}
										case "11": // lekarz
											{
												Pracownik.pracownik.Identyfikator = new IdentyfikatorPersonelu();
												Pracownik.pracownik.Identyfikator.UstawWartosc(ZbiorWartosci.NpwzLekarzyDentystowFelczerow, drd.GetString(IDENTYFIKATOR));
												Pracownik.pracownik.RodzajZawoduMedycznego = RodzajZawoduMedycznego.Lekarz;
												break;
											}
										case "12": // dentysta
											{
												Pracownik.pracownik.Identyfikator = new IdentyfikatorPersonelu();
												Pracownik.pracownik.Identyfikator.UstawWartosc(ZbiorWartosci.NpwzLekarzyDentystowFelczerow, drd.GetString(IDENTYFIKATOR));
												Pracownik.pracownik.RodzajZawoduMedycznego = RodzajZawoduMedycznego.LekarzDentysta;
												break;
											}
										case "18": // pielęgniarka
											{
												Pracownik.pracownik.Identyfikator = new IdentyfikatorPersonelu();
												Pracownik.pracownik.Identyfikator.UstawWartosc(ZbiorWartosci.NpwzPielegniarekPoloznych, drd.GetString(IDENTYFIKATOR));
												Pracownik.pracownik.RodzajZawoduMedycznego = RodzajZawoduMedycznego.Pielegniarka;
												break;
											}
										case "19": // położna
											{
												Pracownik.pracownik.Identyfikator = new IdentyfikatorPersonelu();
												Pracownik.pracownik.Identyfikator.UstawWartosc(ZbiorWartosci.NpwzPielegniarekPoloznych, drd.GetString(IDENTYFIKATOR));
												Pracownik.pracownik.RodzajZawoduMedycznego = RodzajZawoduMedycznego.Polozna;
												break;
											}
										default:
											break;
									}
								}
							}
						}
					}
				}
			}
			if (Pracownik.pracownik == null)
				throw new Exception("Nie odnaleziono pracownika o identyfikatorze = " + medInfId.ToString());
			return Pracownik.pracownik;
		}
	}
}
