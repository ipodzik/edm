﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.Common;



namespace BIUINF.Lib4.HL7CDA.MedInf
{
	public class BrakDanychMedInfExclamationException : ExclamationException
	{
		private BrakDanychMedInfExclamationException(string msg) : base(msg) { }

		private BrakDanychMedInfExclamationException(BrakDanychMedInfExclamationException ex) : base(ex.Message, ex) { }




		public static BrakDanychMedInfExclamationException Create(string msg)
		{
			var ex = new BrakDanychMedInfExclamationException(msg);
			return new BrakDanychMedInfExclamationException(ex);
		}
	}
}
