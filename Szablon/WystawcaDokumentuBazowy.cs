﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.2.6-2018-06-30T000000.html 
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.6
	/// </summary>
	[Szablon("[2] Wystawca dokumentu (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.6", "plCdaBaseLegalAuthenticator", WersjaSzablonu.v1_3, "legalAuthenticator")]
	public class WystawcaDokumentuBazowy : SzablonSkladowy
	{
		internal DateTime? Wystawiono { get; set; }
		internal IdentyfikatorPersonelu Identyfikator { get; set; }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<WystawcaDokumentuBazowy>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.DodajIdentyfikatorSzablonu(this);

			if (this.Wystawiono.HasValue)
			{
				xmlElement.DodajElement("time").UstawAtrybut("value", this.Wystawiono.Value.ToString("yyyyMMdd"));
			}
			xmlElement.DodajElement("signatureCode").UstawAtrybut("code", "S");
			xmlElement.DodajElement("assignedEntity").DodajElement(this.Identyfikator);

			return xmlElement;
		}
	}
}
