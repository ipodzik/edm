﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// Unikalny identyfikator instancji dokumentu medycznego
	/// </summary>
	/// <remarks>
	/// Każda instancja dokumentu medycznego posiada unikalny identyfikator, zapisany w elemencie id na poziomie dokumentu, 
	/// składający się z węzła OID określającego pulę wartości identyfikatora, oraz samej wartości identyfikatora
	/// 
	/// Każda zgodna z polskim IG instancja dokumentu medycznego, poza identyfikatorem dokumentu, 
	/// posiada parę informacji pozwalających równie jednoznacznie zidentyfikować tę instancję. 
	/// Parę tę tworzy zapisywany w elemencie setId na poziomie dokumentu tzw. identyfikator zbioru wersji dokumentu, 
	/// a także zapisywany w elemencie versionNumber na poziomie dokumentu numer wersji tej instancji.
	/// </remarks>
	public class IdentyfikatorDokumentu
	{
		/// <summary>
		/// Identyfikator instancji dokumentu
		/// </summary>
		internal IdentyfikatorInstancji IdentyfikatorInstancji { get; private set; }

		/// <summary>
		/// Identyfikator zbioru wersji dokumentu
		/// </summary>
		internal IdentyfikatorInstancji IdentyfikatorZbioruWersji { get; private set; }

		/// <summary>
		/// Wartość identyfikatora instancji dokumentu (=IdentyfikatorInstancji.Extension)
		/// </summary>
		public string Instancja { get { return this.IdentyfikatorInstancji.Extension; } set { this.IdentyfikatorInstancji.Extension = value; } }

		/// <summary>
		/// Wartość identyfikatora zbioru wersji dokumentu (=IdentyfikatorZbioruWersji.Extension)
		/// </summary>
		public string ZbiorWersji { get { return this.IdentyfikatorZbioruWersji.Extension; } set { this.IdentyfikatorZbioruWersji.Extension = value; } }

		/// <summary>
		/// Numer wersji instancji
		/// </summary>
		public int Wersja { get; set; }


		/// <summary>
		/// Konstruktor tworzący 2 wymagane dla dokumentu identyfikatory instancji,
		/// przy czym własność <see cref="CzyWyswietlany"/> obu identyfikatorów ustawiana jest na null
		/// </summary>
		/// <param name="zbiorWartosciInstancji">Zbiór wartości, z którego pochodzi wartość identyfikatora instancji</param>
		/// <param name="zbiorWartoscizbioruWersji">Zbiór wartości, z którego pochodzi numer wersji instancji dokumentu</param>
		public IdentyfikatorDokumentu(ZbiorWartosci zbiorWartosciInstancji, ZbiorWartosci zbiorWartoscizbioruWersji)
			: this(zbiorWartosciInstancji, null, zbiorWartoscizbioruWersji) { }

		/// <summary>
		/// Konstruktor podstawowy. Tworzy 2 wymagane dla dokumentu identyfikatory instancji
		/// </summary>
		/// <param name="zbiorWartosciInstancji"></param>
		/// <param name="czyWyswietlac">Wartość atrybutu displayable dla identyfikatora instancji</param>
		/// <param name="zbiorWartoscizbioruWersji"></param>
		public IdentyfikatorDokumentu(ZbiorWartosci zbiorWartosciInstancji, bool? czyWyswietlac, ZbiorWartosci zbiorWartoscizbioruWersji)
		{
			this.IdentyfikatorInstancji = new IdentyfikatorInstancji(zbiorWartosciInstancji, czyWyswietlac);
			this.IdentyfikatorZbioruWersji = new IdentyfikatorInstancji(zbiorWartoscizbioruWersji);
			this.IdentyfikatorZbioruWersji.XmlElementName = "setId";
		}
	}




	public class IdentyfikatorDokumentuAnulowania
	{
		/// <summary>
		/// Wartość identyfikatora dokumentu 
		/// </summary>
		public string Instancja { get { return this.IdentyfikatorInstancji.Extension; } set { this.IdentyfikatorInstancji.Extension = value; } }
		internal IdentyfikatorInstancji IdentyfikatorInstancji { get; private set; }



		public IdentyfikatorDokumentuAnulowania(ZbiorWartosci zbiorWartosciInstancji, bool? czyWyswietlac)
		{
			this.IdentyfikatorInstancji = new IdentyfikatorInstancji(zbiorWartosciInstancji, czyWyswietlac);
		}
	}
}
