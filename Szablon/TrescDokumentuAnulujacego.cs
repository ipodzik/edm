﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.2.47-2018-06-30T000000.html 
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.47
	/// </summary>
	[Szablon("[2] Treść dokumentu anulującego", "2.16.840.1.113883.3.4424.13.10.2.47", "plCdaNullificationComponent", WersjaSzablonu.v1_3, "component")]
	public class TrescDokumentuAnulujacego : SzablonSkladowy
	{
		internal SekcjaDokumentuAnulujacego Dane { get { return this.dane; } }
		private SekcjaDokumentuAnulujacego dane;



		public TrescDokumentuAnulujacego()
		{
			this.dane = new SekcjaDokumentuAnulujacego();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajIdentyfikatorSzablonu(this);


			xmlElement.DodajElement("structuredBody").DodajElement("component").DodajElement(this.dane);

			return xmlElement;
		}
	}
}
