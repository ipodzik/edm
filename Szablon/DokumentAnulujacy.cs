﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.1.14-2019-09-30T000000.html
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.1.14
	/// </summary>
	[Szablon("[1] Dokument anulujący", "2.16.840.1.113883.3.4424.13.10.1.14", "plCdaNullification", WersjaSzablonu.v1_3_1, "ClinicalDocument")]
	public class DokumentAnulujacy : SzablonDokumentu
	{
		private readonly XmlDocument xmlDocument = new XmlDocument();
		private readonly TypDokumentuDoAnulowania typjDokumentuDoAnulowania;

		public IdentyfikatorDokumentuAnulowania Identyfikator { get; private set; }

		public string DokumentDoAnulowaniaIdentyfikatorInstancji
		{
			get { return this.dokumentDoAnulowaniaIdentyfikator.Instancja; }
			set { this.dokumentDoAnulowaniaIdentyfikator.Instancja = this.dokument.Dane.DokumentDoAnulowaniaIdentyfikator = value; }
		}
		public string DokumentDoAnulowaniaIdentyfikatorZbiorWersji
		{
			get { return this.dokumentDoAnulowaniaIdentyfikator.ZbiorWersji; }
			set { this.dokumentDoAnulowaniaIdentyfikator.ZbiorWersji = value; }
		}
		public int DokumentDoAnulowaniaIdentyfikatorWersja
		{
			get { return this.dokumentDoAnulowaniaIdentyfikator.Wersja; }
			set { this.dokumentDoAnulowaniaIdentyfikator.Wersja = value; }
		}
		private readonly IdentyfikatorDokumentu dokumentDoAnulowaniaIdentyfikator; // obiekt przekazywany jako parametr do this.dokumentDoAnulowania

		public DateTime? DokumentDoAnulowaniaWystawiono
		{
			get { return this.dokument.Dane.DokumentDoAnulowaniaWystawiono; }
			set { this.dokument.Dane.DokumentDoAnulowaniaWystawiono = value; }
		}

		public DateTime? Wystawiono
		{
			get { return this.wystationo; }
			set { this.wystationo = this.autor.UtworzenieDokumentu = this.WystawcaDokumentu.Wystawiono = value; }
		}
		private DateTime? wystationo;

		public DanePacjentaBazowy Pacjent { get { return this.pacjent; } }
		private DanePacjentaBazowy pacjent;

		public AutorDokumentuBazowy Autor
		{
			get { return this.autor; }
		}
		private AutorDokumentuBazowy autor;
		internal WystawcaDokumentuBazowy WystawcaDokumentu { get; private set; }

		public TrescDokumentuAnulujacego Dokument { get { return this.dokument; } }
		private TrescDokumentuAnulujacego dokument;

		public DokumentPowiazanyDokumentuAnulowanego DokumentDoAnulowania { get { return this.dokumentDoAnulowania; } }
		private DokumentPowiazanyDokumentuAnulowanego dokumentDoAnulowania;

		public DaneUbezpieczycielaPublicznegoBazowy Ubezpieczyciel
		{
			get { return this.ubezpieczyciel; }
		}
		private DaneUbezpieczycielaPublicznegoBazowy ubezpieczyciel;



		public DokumentAnulujacy(TypDokumentuDoAnulowania rodzajDokumentuAnulowania)
		{
			this.WystawcaDokumentu = new WystawcaDokumentuBazowy();
			this.pacjent = new DanePacjentaBazowy();
			this.autor = new AutorDokumentuBazowy();
			this.dokument = new TrescDokumentuAnulujacego();
			this.ubezpieczyciel = new DaneUbezpieczycielaPublicznegoBazowy();

			switch (this.typjDokumentuDoAnulowania)
			{
				case TypDokumentuDoAnulowania.Recepta:
					{
						this.Identyfikator = new IdentyfikatorDokumentuAnulowania(ZbiorWartosci.ReceptaInstancjaDokumentowAnulowania, true);
						this.dokumentDoAnulowaniaIdentyfikator = new IdentyfikatorDokumentu(ZbiorWartosci.ReceptaInstancja, true, ZbiorWartosci.ReceptaZbiorWersjiReceptIDokumentowAnulowania);
						this.dokument.Dane.DokumentDoAnulowaniaTytul = "Recepta";
						break;
					}
				default:
					throw new NotImplementedException("Obsługa anulowania wskazanego rodzaju dokument nie jest możliwa!");
			}
			this.dokumentDoAnulowania = new DokumentPowiazanyDokumentuAnulowanego(this.dokumentDoAnulowaniaIdentyfikator);
		}



		public override XmlDocument TworzXml()
		{
			var hl7Szablon = AttributeHelper<DokumentAnulujacy>.GetCustomAttribute<SzablonAttribute>();
			var xmlElement = this.xmlDocument.CreateElement(hl7Szablon.XmlElementNazwa);

			this.xmlDocument.AppendChild(this.xmlDocument.CreateXmlDeclaration("1.0", Encoding.UTF8.HeaderName, null));
			this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.xsl\" type=\"text/xsl\""));

			//switch (hl7Szablon.WersjaSzablonu)
			//{
			//  case WersjaSzablonu.v1_3_1:
			//    {
			//      this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.xsl\" type=\"text/xsl\""));
			//      break;
			//    }
			//  case WersjaSzablonu.v1_3_1_2:
			//    {
			//      this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.2.xsl\" type=\"text/xsl\""));
			//      break;
			//    }
			//  default:
			//    break;
			//}

			this.xmlDocument.AppendChild(xmlElement);

			this.xmlDocument.DocumentElement.SetAttribute("xmlns", "urn:hl7-org:v3");
			this.xmlDocument.DocumentElement.SetAttribute("xmlns:extPL", "http://www.csioz.gov.pl/xsd/extPL/r2");

			this.xmlDocument.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			this.xmlDocument.DocumentElement.SetAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", "extPL:ClinicalDocument");

			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "typeId").SetExtension("POCD_HD000040").SetRoot("2.16.840.1.113883.1.3").Build());

			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("1.3.6.1.4.1.19376.1.9.1.1.1").Build());
			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("1.3.6.1.4.1.19376.1.5.3.1.1.1").Build());
			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot(hl7Szablon.Id).SetExtension(hl7Szablon.Wersja).Build());

			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement).SetExtension(this.Identyfikator.Instancja).SetRoot(this.Identyfikator.IdentyfikatorInstancji.Root).SetDisplayable(true).Build());

			xmlElement.DodajTypCE("51851-4", "2.16.840.1.113883.6.1", "LOINC", "Administrative note",
				xmlElement.TworzTypCD("translation", "08.80", "2.16.840.1.113883.3.4424.11.1.32", "KLAS_DOK_P1", "Dokument anulujący"));

			xmlElement.DodajElement("title", "Dokument anulujący");
			//xmlElement.AppendChild(new CharacterStringBuilder(xmlElement, "title").SetInnerText("Dokument anulujący").Build());

			if (this.Wystawiono.HasValue)
			{
				//xmlElement.DodajElement("effectiveTime").UstawAtrybut("value", this.Wystawiono.Value.ToString("yyyyMMdd"));
				xmlElement.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());
			}

			xmlElement.DodajElement("confidentialityCode").UstawAtrybut("code", "N").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.25");

			xmlElement.DodajElement("languageCode").UstawAtrybut("code", "pl-PL");

			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "setId").SetExtension(this.DokumentDoAnulowaniaIdentyfikatorInstancji).SetRoot(ZbiorWartosci.ReceptaZbiorWersjiReceptIDokumentowAnulowania.GetKod()).Build());
			xmlElement.DodajElement("versionNumber").UstawAtrybut("value", "2");

			xmlElement.DodajElement(this.pacjent);

			xmlElement.DodajElement(this.autor);

			xmlElement.DodajElement(new OrganizacjaOdpowiedzialnaZaDokumentBazowy());

			this.WystawcaDokumentu.Identyfikator = this.autor.Identyfikator;
			xmlElement.DodajElement(this.WystawcaDokumentu);

			this.ubezpieczyciel.Platnik.Kod = this.pacjent.KodOddzialuNFZ;
			xmlElement.DodajElement(this.ubezpieczyciel);

			xmlElement.DodajElement(this.dokumentDoAnulowania);

			xmlElement.DodajElement(this.dokument);


			this.daneKontekstuWywolania = new DaneKontekstuWywolania(this.autor.Identyfikator, this.autor.JednostkaOrganizacyjna.Podmiot, this.autor.JednostkaOrganizacyjna.KomorkaOrganizacyjna);

			return this.xmlDocument;
		}
	}




	public enum TypDokumentuDoAnulowania
	{
		Recepta
	}
}
