﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[2] Osoba (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.1", "plCdaBasePerson", WersjaSzablonu.v1_3, "assignedPerson")]
	public class OsobaBazowy : SzablonSkladowy
	{
		public NazwiskoImieOsobyBazowy NazwiskoImie
		{
			get { return this.nazwiskoImie; }
		}
		private NazwiskoImieOsobyBazowy nazwiskoImie;

		private bool czyWystepujeSzablonId;



		public OsobaBazowy()
		{
			this.nazwiskoImie = new NazwiskoImieOsobyBazowy();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajSzablon(this.NazwiskoImie);

			return xmlElement;
		}
	}
}
