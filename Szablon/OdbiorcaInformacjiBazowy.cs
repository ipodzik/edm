﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.61
	/// </summary>
	[Szablon("[2] Odbiorca informacji bazowy", "2.16.840.1.113883.3.4424.13.10.2.61", "plCdaBaseInformationRecipient", WersjaSzablonu.v1_3_1, "informationRecipient")]
	class OdbiorcaInformacjiBazowy
	{
	}
}
