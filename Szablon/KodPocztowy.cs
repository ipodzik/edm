﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	public class KodPocztowy : IElementSzablonu
	{
		public string Kod { get; set; }
		[HL7XmlName("postCity")]
		public string Poczta { get; set; }



		public XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.Kod))
			{
				xmlElement = parent.OwnerDocument.CreateElement("postalCode");
				xmlElement.SetAttribute("type", parent.GetNamespaceOfPrefix("xsi"), "extPL:adxp.postalCode");
				xmlElement.UstawAtrybut(() => this.Poczta);
				xmlElement.InnerText = this.Kod;
			}

			return xmlElement;
		}
	}
}
