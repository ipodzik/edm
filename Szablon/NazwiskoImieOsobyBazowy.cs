﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[7] Nazwisko i imię osoby (bazowy)", "2.16.840.1.113883.3.4424.13.10.7.2", "plCdaBasePersonName", WersjaSzablonu.v1_3, "name")]
	public class NazwiskoImieOsobyBazowy : SzablonSkladowy
	{
		[HL7XmlName("prefix")]
		public string Prefix { get; set; }
		[HL7XmlName("given")]
		public string Imie { get; set; }
		[HL7XmlName("given")]
		public string Imie2 { get; set; }
		[HL7XmlName("family")]
		public string Nazwisko { get; set; }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajElement(parent, () => this.Prefix);
			xmlElement.DodajElement(parent, () => this.Imie);
			xmlElement.DodajElement(parent, () => this.Imie2);
			xmlElement.DodajElement(parent, () => this.Nazwisko);

			return xmlElement;
		}
	}
}
