﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;




namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.2.20-2018-06-30T000000.html 
	*/
	[Szablon("[2] Organizacja odpowiedzialna za dokument dla P1", "2.16.840.1.113883.3.4424.13.10.2.20", "plCdaP1BaseCustodian", WersjaSzablonu.v1_3, "custodian")]
	public class OrganizacjaOdpowiedzialnaZaDokumentDlaP1 : SzablonSkladowy
	{
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajIdentyfikatorSzablonu(this);

			var xmlReprezentowanaOrganizacja = xmlElement.DodajElement("assignedCustodian").DodajElement("representedCustodianOrganization");
			xmlReprezentowanaOrganizacja.DodajTypII(null, "2.16.840.1.113883.3.4424", false).UstawAtrybut("assigningAuthorityName", "CSIOZ");

			return xmlElement;
		}
	}
}
