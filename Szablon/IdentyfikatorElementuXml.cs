﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// Statyczna klasa z wartościami identyfikatorów elementów xml 
	/// </summary>
	public static class IdentyfikatorElementuXml
	{
		public static string Lek { get { return "SBADM_1"; } }

		public static string LekDawkowanie { get { return "DS_1"; } }

		public static string LekOdplatnosc { get { return "ACT_2"; } }

		public static string LekPlatnik { get { return "ACT_3"; } }

		public static string LekSubstancjaCzynna { get { return "OBS_1"; } }

		public static string InformacjaDlaOsobyWydjacejLek { get { return "TEXT1"; } }
	}
}
