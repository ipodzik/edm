﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[7] Adres (bazowy)", "2.16.840.1.113883.3.4424.13.10.7.1", "plCdaBaseAddr", WersjaSzablonu.v1_3, "addr")]
	public class AdresBazowy : SzablonSkladowy
	{
		[HL7XmlName("country")]
		public string Panstwo { get; set; }
		[HL7XmlName("city")]
		public string Miasto { get; set; }

		public KodPocztowy KodPocztowy { get; private set; }

		[HL7XmlName("streetName")]
		public string Ulica { get; set; }
		[HL7XmlName("houseNumber")]
		public string NrDomu { get; set; }
		[HL7XmlName("unitID")]
		public string NrLokalu { get; set; }
		[HL7XmlName("censusTract", "TERYT SIMC: {0}")]
		public string TERYT { get; set; }



		public AdresBazowy()
		{
			this.KodPocztowy = new KodPocztowy();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			//xmlElement.DodajIdentyfikatorSzablonu(parent, this);

			xmlElement.DodajElement(() => this.Panstwo);

			if (!string.IsNullOrEmpty(this.KodPocztowy.Poczta))	
				this.KodPocztowy.Poczta = this.Miasto;
			
			xmlElement.DodajElement(this.KodPocztowy);
			xmlElement.DodajElement(() => this.Miasto);
			if (!string.IsNullOrEmpty(this.Ulica)) xmlElement.DodajElement(() => this.Ulica);
			if (!string.IsNullOrEmpty(this.NrDomu)) xmlElement.DodajElement(() => this.NrDomu);
			if (!string.IsNullOrEmpty(this.NrLokalu)) xmlElement.DodajElement(() => this.NrLokalu);
			if (!string.IsNullOrEmpty(this.TERYT)) xmlElement.DodajElement(() => this.TERYT);

			return xmlElement;
		}
	}
}
