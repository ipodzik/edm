﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// Tworzy znacznik: <code>&lt;status code="completed" /&gt;</code> 
	/// </summary>
	public class StatusCodeCompleted : IElementSzablonu
	{
		public XmlElement TworzXmlElement(XmlElement parent)
		{
			return parent.TworzElementXml("statusCode").UstawAtrybut("code", "completed");
		}
	}




	/// <summary>
	/// Tworzy znacznik: <c>&lt;id nullFlavor="..." /&gt;</c>
	/// </summary>
	public class IdNullFlavor : IElementSzablonu
	{
		private readonly string value;


		/// <summary>
		/// Tworzy znacznik: <code><id nullFlavor=""/></code>
		/// </summary>
		/// <param name="value">wartość, która ma byc w kropkach</param>
		public IdNullFlavor(string value)
		{
			this.value = value;
		}



		public XmlElement TworzXmlElement(XmlElement parent)
		{
			return parent.TworzElementXml("id").UstawAtrybut("nullFlavor", this.value);
		}
	}



	/// <summary>
	/// Tworzy znaczniki: <code>&lt;performer typeCode="PRF" &gt;&lt;assignedEntity&gt;&lt;id .../&gt;&lt;/assignedEntity&gt;&lt;/performer&gt;</code> 
	/// </summary>
	public class PerformerAssignedEntity : IElementSzablonu
	{
		private readonly PodmiotFinansujacyZeSrodkowPublicznych podmiotFinansujacy;



		public PerformerAssignedEntity(PodmiotFinansujacyZeSrodkowPublicznych podmiotFinansujacy)
		{
			this.podmiotFinansujacy = podmiotFinansujacy;
		}



		public XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml("performer").UstawAtrybut("typeCode", "PRF");
			xmlElement.DodajElement("assignedEntity").DodajElement(this.podmiotFinansujacy);
			return xmlElement;
		}
	}

}
