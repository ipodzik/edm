﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				 https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.61-2018-06-30T000000.html
	*/
	[Szablon("[4] Uprawnienia dodatkowe publicznego ubezpieczenia zdrowotnego", "2.16.840.1.113883.3.4424.13.10.4.61", "plCdaSpecialEntitlementPolicyEntry", WersjaSzablonu.v1_3, "act")]
	public class UprawnienieDodatkowePublicznegoUbezpieczeniaZdrowotnego : SzablonSkladowy
	{
		private readonly UprawnienieDodatkoweZwiazaneZRefundacjaLekow uprawnieineDodatkowe;

		internal PodmiotFinansujacyZeSrodkowPublicznych Platnik
		{
			get { return this.platnik; }
			set { this.platnik = value; }
		}
		private PodmiotFinansujacyZeSrodkowPublicznych platnik;

		internal DaneAutoryzacyjneZwiazaneZUbezpieczeniem Ubezpieczenie
		{
			get { return this.ubezpieczenie; }
		}
		private DaneAutoryzacyjneZwiazaneZUbezpieczeniem ubezpieczenie;



		public UprawnienieDodatkowePublicznegoUbezpieczeniaZdrowotnego(SekcjaZleceniaLeku zlecenieLeku)
		{
			this.uprawnieineDodatkowe = zlecenieLeku.Lek.UprawnienieDodatkowe.Value;
			this.ubezpieczenie = new DaneAutoryzacyjneZwiazaneZUbezpieczeniem(zlecenieLeku);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<UprawnienieDodatkowePublicznegoUbezpieczeniaZdrowotnego>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "EVN");

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.26");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement(new IdNullFlavor("NA"));

			var xmlQualifier = xmlElement.DodajElement("code").UstawAtrybut("code", "PUBLICPOL").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.4")
				.DodajElement("qualifier");

			xmlQualifier.DodajElement("name").UstawAtrybut("code", "RLUD").UstawAtrybut("codeSystem", SystemKodowania.PolskieKlasyfikatoryHL7v3.GetKod()).UstawAtrybut("displayName", "Refundacja leków wynikająca z uprawnień dodatkowych");
			xmlQualifier.DodajElement("value").UstawAtrybut("code", this.uprawnieineDodatkowe.GetKod()).UstawAtrybut("displayName", this.uprawnieineDodatkowe.GetNazwa()).UstawAtrybut("codeSystem", SystemKodowania.UprawnienieDodatkowe.GetKod());

			xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.LekPlatnik));
			xmlElement.DodajElement(new StatusCodeCompleted());

			xmlElement.DodajElement(new PerformerAssignedEntity(this.platnik));

			xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "REFR").DodajElement(this.ubezpieczenie);

			return xmlElement;
		}
	}
}
