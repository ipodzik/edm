﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.Common;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	public enum WersjaSzablonu
	{
		/// <summary>
		/// 1.3 / 2018-06-30 
		/// </summary>
		[DaneWersjiSzablonu("1.3", "2018-06-30")]
		v1_3,
		/// <summary>
		/// 1.3.1 / 2018-09-30 
		/// </summary>
		[DaneWersjiSzablonu("1.3.1", "2018-09-30")]
		v1_3_1,
		/// <summary>
		/// 1.3.1.2 / 2018-09-30 
		/// </summary>
		[DaneWersjiSzablonu("1.3.1.2", "2018-09-30")]
		v1_3_1_2
	}




	/// <summary>
	/// Klasa atrybutu dla wartości wyliczenia WersjaSzablonu
	/// - pozwala powiązać element wyliczenia z danymi dodatkowymi (etykieta, początek obowiązywania)
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class DaneWersjiSzablonuAttribute : Attribute
	{
		public string Etykieta { get; private set; }
		public DateTime ObowiazujeOd { get; private set; }



		internal DaneWersjiSzablonuAttribute(string etykieta, string obowiazujeOd)
		{
			this.Etykieta = etykieta;

			DateTime tmp;
			if (!DateTime.TryParse(obowiazujeOd, out tmp))
			{
				throw new ErrorException("Nie można sparsować podanej daty początku obowiązywania szablonu!");
			}
			this.ObowiazujeOd = tmp;
		}
	}



	/// <summary>
	/// Rozszerzenie klasy DaneWersjiSzablonuAttribute - metody pobierające wartości atrybutu
	/// </summary>
	public static class WersjaSzablonuExtension
	{
		private static Dictionary<WersjaSzablonu, DaneWersjiSzablonuAttribute> wersje = new Dictionary<WersjaSzablonu, DaneWersjiSzablonuAttribute>();

		/// <summary>
		/// Metoda zwraca atrybut ustawiony dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DaneWersjiSzablonuAttribute GetDaneWersjiSzablonuAttribute(this WersjaSzablonu value)
		{
			if (!WersjaSzablonuExtension.wersje.ContainsKey(value))
			{
				var type = value.GetType();
				var name = Enum.GetName(type, value);
				WersjaSzablonuExtension.wersje.Add(value, type.GetField(name).GetCustomAttributes(false).OfType<DaneWersjiSzablonuAttribute>().SingleOrDefault());
			}

			return WersjaSzablonuExtension.wersje[value];
		}

		/// <summary>
		/// Metoda zwraca etykietę z atrybutu ustawionego dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetEtykieta(this WersjaSzablonu value)
		{
			var daneWersjiSzablonuAttribute = value.GetDaneWersjiSzablonuAttribute();

			if (daneWersjiSzablonuAttribute != null)
			{
				return daneWersjiSzablonuAttribute.Etykieta;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Metoda zwraca datę początku obowiązywania z atrybutu ustawionego dla elementu wyliczenia
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DateTime? GateObowiazujeOd(this WersjaSzablonu value)
		{
			var daneWersjiSzablonuAttribute = value.GetDaneWersjiSzablonuAttribute();

			if (daneWersjiSzablonuAttribute != null)
			{
				return daneWersjiSzablonuAttribute.ObowiazujeOd;
			}
			else
			{
				return null;
			}
		}
	}
}
