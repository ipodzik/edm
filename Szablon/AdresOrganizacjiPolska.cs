﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[7] Adres organizacji w Polsce", "2.16.840.1.113883.3.4424.13.10.7.4", "plCdaPolishOrganizationalAddr", WersjaSzablonu.v1_3, "addr")]
	public class AdresOrganizacjiPolska : SzablonSkladowy
	{
		[HL7XmlName("postalCode")]
		public string KodPocztowy { get; set; }
		[HL7XmlName("city")]
		public string Miasto { get; set; }
		[HL7XmlName("streetName")]
		public string Ulica { get; set; }
		[HL7XmlName("houseNumber")]
		public string NrDomu { get; set; }
		[HL7XmlName("unitID")]
		public string NrLokalu { get; set; }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajElement(parent, () => this.KodPocztowy);
			xmlElement.DodajElement(parent, () => this.Miasto);
			xmlElement.DodajElement(parent, () => this.Ulica);
			xmlElement.DodajElement(parent, () => this.NrDomu);
			xmlElement.DodajElement(parent, () => this.NrLokalu);

			if (!xmlElement.HasChildNodes)
			{
				xmlElement = null;
			}

			return xmlElement;
		}
	}
}
