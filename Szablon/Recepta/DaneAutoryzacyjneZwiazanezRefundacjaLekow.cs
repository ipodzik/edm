﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	[Szablon("[4] Dane autoryzacyjne związane z refundacją leków", "2.16.840.1.113883.3.4424.13.10.4.69", "plCdaDrugPrescriptionAuthorizationActivityEntry", WersjaSzablonu.v1_3, "act")]
	public class DaneAutoryzacyjneZwiazanezRefundacjaLekow : SzablonSkladowy
	{
		internal IdentyfikatorInstancji PozacjaRecepty { get; private set; }



		public DaneAutoryzacyjneZwiazanezRefundacjaLekow()
		{
			this.PozacjaRecepty = new IdentyfikatorInstancji(ZbiorWartosci.ReceptaPozycja);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<DaneAutoryzacyjneZwiazanezRefundacjaLekow>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.UstawAtrybut("classCode", "ACT");
			xmlElement.UstawAtrybut("moodCode", "EVN");

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.19");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement("id").UstawAtrybut("nullFlavor", "NA");

			xmlElement.DodajElement("code").UstawAtrybut("nullFlavor", "NA");

			var sa = xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "SUBJ")
				.DodajElement("substanceAdministration").UstawAtrybut("classCode", "SBADM").UstawAtrybut("moodCode", "PRMS");

			sa.DodajElement(this.PozacjaRecepty);

			sa.DodajElement("consumable").DodajElement("manufacturedProduct").DodajElement("manufacturedMaterial").UstawAtrybut("nullFlavor", "NA");

			return xmlElement;
		}		
	}
}
