﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.55-2018-09-30T000000.html
	*/
	/// <summary>
	/// <c>2.16.840.1.113883.3.4424.13.10.4.55</c>
	/// Wydanie leku
	/// </summary>
	[Szablon("[4] Wydanie leku", "2.16.840.1.113883.3.4424.13.10.4.55", "plCdaDrugPrescriptionSupplyEntry", WersjaSzablonu.v1_3_1_2, "supply")]
	public class WydanieLeku : SzablonSkladowy
	{
		private readonly Lek lek;

		// ToDO: Zmiana nazwy pola?
		/// <summary>
		/// Data realizacji recepty od - po której należy wydac lek w aptece 
		/// </summary>
		internal DateTime? RealizacjaOd { get; set; }

		internal PoziomOdplatnosciLeku Odplatnosc
		{
			get { return this.odplatnosc; }
		}
		private PoziomOdplatnosciLeku odplatnosc;

		internal PotwierdzenieCalkowitejDawkiSubstancjiCzynnej DawkaSubstancji
		{
			get { return this.dawkaSubstancji; }
		}
		private PotwierdzenieCalkowitejDawkiSubstancjiCzynnej dawkaSubstancji;



		public WydanieLeku(Lek lek)
		{
			this.lek = lek;
			// TODO: poziom odpłatności to chyba nie zawsze się pojawi. Leki OTC i inne mają wtedy "Nie dotyczy".s
			this.odplatnosc = new PoziomOdplatnosciLeku(this.lek);
			this.dawkaSubstancji = new PotwierdzenieCalkowitejDawkiSubstancjiCzynnej(this.lek);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<WydanieLeku>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.UstawAtrybut("classCode", "SPLY");
			xmlElement.UstawAtrybut("moodCode", "RQO");

			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.8");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			if (this.RealizacjaOd.HasValue)
			{
				// TODO: To jest data realizacji recepty OD, a nie czas od kiedy podawać lek.
				xmlElement.DodajElement("effectiveTime").UstawAtrybut("value", this.RealizacjaOd.Value.ToString("yyyyMMdd"));
			}

			// Cito gdy 'UR'
			if (this.lek.CzyCito)
			{
				xmlElement.DodajElement("priorityCode").UstawAtrybut("code", "UR").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.7");
			}

			// wartość stała, więc pomijalna w recepcie - ustawić false
			xmlElement.DodajElement("independentInd").UstawAtrybut("value", "false");

			// Uwaga: jeżeli poniżej podano product, to ta ilość dotyczy poniższych opakowań. Jeżeli nie podano product, to ilość dotyczy powyższego consumable 
			xmlElement.DodajTypPQ("quantity", this.lek.Ilosc);

			// jeżeli podano element product, to dotyczy on opakowania leku i musi zawierać kod GS1 
			xmlElement.DodajElement("product")
				.DodajElement("manufacturedProduct")
					.DodajElement("manufacturedLabeledDrug")
						.DodajElement(new Slownik.LekRecepty(this.lek, OID.ZbiorWartosci.LekiEAN));

			// poziom odpłatności za ten lek
			if (this.lek.Odplatnosc.HasValue)
			{
				xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP").DodajElement(this.odplatnosc);
			}

			if (this.lek.SubstancjaCzynna != null)
			{
				xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP").DodajElement(this.dawkaSubstancji);
			}

			if (this.lek.RealizacjaDo.HasValue)
			{
				var xmlSupply = xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP")
					.DodajElement("supply").UstawAtrybut("classCode", "SPLY").UstawAtrybut("moodCode", "RQO");

				xmlSupply.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.15");

				var xmlEffectiveTime = xmlSupply.DodajElement("effectiveTime").UstawAtrybut("type", "xsi", "IVL_TS");
				{
					xmlEffectiveTime.DodajElement("high").UstawAtrybut("value", this.lek.RealizacjaDo.Value.ToString("yyyyMMdd"));
				}
			}

			return xmlElement;
		}
	}
}
