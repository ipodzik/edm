﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;




namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	public class Lek : IElementSzablonu
	{
		public string Nazwa { get; set; }
		/// <summary>
		/// Ilość w opakowaniu 
		/// </summary>
		public string Opakowanie { get; set; }
		public int OpakowanieIlosc { get; set; }

		public PoziomOdplatnosciZaLeki? Odplatnosc
		{
			get { return this.odplatnosc; }
			set { this.odplatnosc = value; }
		}
		private PoziomOdplatnosciZaLeki? odplatnosc;

		public UprawnienieDodatkoweZwiazaneZRefundacjaLekow? UprawnienieDodatkowe
		{
			get { return this.uprawnienieDodatkowe; }
			set { this.uprawnienieDodatkowe = value; }
		}
		private UprawnienieDodatkoweZwiazaneZRefundacjaLekow? uprawnienieDodatkowe;


		//public string Odplatnosc { get; set; }
		//public byte OdplatnoscId { get; set; }
		public string Dawkowanie { get; set; }

		public bool CzyCito { get; set; }
		public bool CzyNieZamieniac { get; set; }
		public string MocSkladnikowLeku { get; set; }
		public string PostacLeku { get; set; }

		public string EAN { get; set; }
		public string UrplId { get; set; }
		//public string WielkoscOpakowania { get; set; }
		public string Ilosc { get; set; }

		/// <summary>
		/// Od kiedy można realizować receptę 
		/// </summary>
		public DateTime? RealizacjaOd { get; set; }
		//public DateTime? PodawacOd { get; set; }

		/// <summary>
		/// Do kiedy można realizować receptę 
		/// Recepta kiedyś była ważna tylko 30 dni 
		/// </summary>
		public DateTime? RealizacjaDo { get; set; }


		public SubstancjaCzynna SubstancjaCzynna { get; private set; }
		public void DodajSubstancjeCzynna(string ilosc, string jednostkaMiary)
		{
			if (!string.IsNullOrEmpty(ilosc) && !string.IsNullOrEmpty(jednostkaMiary))
			{
				this.SubstancjaCzynna = new SubstancjaCzynna();
				this.SubstancjaCzynna.Ilosc = ilosc.Replace(',', '.'); // poczeba zamienić przecinek na kropkę 
				this.SubstancjaCzynna.JednostkaMiary = jednostkaMiary;
			}
		}

		internal string NazwaWyswietlana { get { return string.Format("{0} {1} {2}", this.Nazwa, this.MocSkladnikowLeku, this.PostacLeku); } }
		internal string NazwaOpakowania { get { return string.Format("{0} {1} ({2})", this.Nazwa, this.MocSkladnikowLeku, this.Opakowanie); } }

		public string InformacjaDlaOsobyWydajacej { get; set; }



		public Lek()
		{
			this.CzyCito = false;
			this.CzyNieZamieniac = false;
			//this.Odplatnosc = PoziomOdplatnosciZaLeki.Pelnoplatne;
			this.OpakowanieIlosc = 1;
		}




		public XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml("text");

			var xmlLek = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.Lek);
			xmlLek.DodajElement("content", this.NazwaWyswietlana).DodajIdentyfikatorElementu("p1_nazwaLeku").UstawAtrybut("styleCode", "xPLbig");
			xmlLek.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
			xmlLek.DodajElement("content", this.MocSkladnikowLeku).DodajIdentyfikatorElementu("p1_mocSkladnikowLeku");
			xmlLek.DodajElement("content", null).DodajIdentyfikatorElementu("p1_edytuj_mocLeku");
			xmlLek.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
			//xmlLek.DodajElement("content", this.PostacLeku).DodajIdentyfikatorElementu("p1_postacLeku");
			xmlLek.DodajElement("content", null).DodajIdentyfikatorElementu("p1_postacLeku");
			xmlLek.DodajElement("content", null).DodajIdentyfikatorElementu("p1_edytuj_postacLeku");

			if (this.CzyNieZamieniac)
			{
				xmlLek.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
				xmlLek.DodajElement("content", "NZ").DodajIdentyfikatorElementu("p1_nieZamieniac").UstawAtrybut("styleCode", "xPLbig");
			}

			var xmlIlosc = xmlElement.DodajElement("paragraph");
			xmlIlosc.DodajElement("content", this.Ilosc).DodajIdentyfikatorElementu("p1_iloscLeku");
			xmlIlosc.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
			xmlIlosc.DodajElement("content", "op. po").DodajIdentyfikatorElementu("p1_krotnosc_opis");
			xmlIlosc.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
			xmlIlosc.DodajElement("content", this.Opakowanie).DodajIdentyfikatorElementu("p1_wielkoscOpakowania");

			var xmDS = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekDawkowanie);
			xmDS.DodajElement("content", "D.S.").DodajIdentyfikatorElementu("p1_stosowanie_opis_1");
			xmDS.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
			xmDS.DodajElement("content", null).DodajIdentyfikatorElementu("p1_stosowanie_wartosc_1").UstawAtrybut("styleCode", "Bold");
			xmDS.DodajElement("content", this.Dawkowanie).DodajIdentyfikatorElementu("p1_edytuj_stosowanie_wartosc_1").UstawAtrybut("styleCode", "Bold");


			//TODO: ustalenie czy pojawia się odpłatność czy nie (czy będzie referencja w WydanieLeku) 
			//TODO: Moim zdaniem chyba powinna być jeszze opcja "Nie dotyczy" w wyliczeniu, która mówiłaby że nie stosyujemy tego bloku (trzeba to uwzględnić również w WydanieLeku, żeby referencja się pojawiała lub nie)
			if (this.odplatnosc.HasValue)
			{
				var xmOdplatnosc = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekOdplatnosc);
				xmOdplatnosc.DodajElement("content", "Odpłatność").DodajIdentyfikatorElementu("p1_odplatnosc_opis");
				xmOdplatnosc.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
				xmOdplatnosc.DodajElement("content", this.Odplatnosc.GetKod()).DodajIdentyfikatorElementu("p1_odplatnosc_wartosc").UstawAtrybut("styleCode", "Bold");
			}

			if (this.RealizacjaOd.HasValue)
			{
				var xmlRealizacjaOd = xmlElement.DodajElement("paragraph");
				xmlRealizacjaOd.DodajElement("content", "Data realizacji od").DodajIdentyfikatorElementu("p1_dataRealizacjiOd_opis");
				xmlRealizacjaOd.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
				xmlRealizacjaOd.DodajElement("content", this.RealizacjaOd.Value.ToString("d MMMM yyyy r.")).DodajIdentyfikatorElementu("p1_dataRealizacjiOd_wartosc").UstawAtrybut("styleCode", "xPLred Bold");
			}

			if (this.RealizacjaDo.HasValue)
			{
				var xmlRealizacjaDo = xmlElement.DodajElement("paragraph");
				xmlRealizacjaDo.DodajElement("content", "Data realizacji do").DodajIdentyfikatorElementu("p1_dataRealizacjiDo_opis");
				xmlRealizacjaDo.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
				xmlRealizacjaDo.DodajElement("content", this.RealizacjaDo.Value.ToString("d MMMM yyyy r.")).DodajIdentyfikatorElementu("p1_dataRealizacjiDo_wartosc").UstawAtrybut("styleCode", "xPLred Bold");
			}

			if (this.SubstancjaCzynna != null)
			{
				var xmlSubstancjaCzynna = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekSubstancjaCzynna);
				xmlSubstancjaCzynna.DodajElement("content", "Potwierdzono ilość substancji czynnej:").DodajIdentyfikatorElementu("p1_potwIlosciSubstCzynnej_opis_1");
				xmlSubstancjaCzynna.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
				xmlSubstancjaCzynna.DodajElement("content", string.Format("{0} {1}", this.SubstancjaCzynna.Ilosc, this.SubstancjaCzynna.JednostkaMiary)).DodajIdentyfikatorElementu("p1_potwIlosciSubstCzynnej_wartosc_1");

				//<paragraph ID="OBS_1">
				//  <content ID="p1_potwIlosciSubstCzynnej_opis">Potwierdzono ilość substancji czynnej:</content>
				//  <content ID="p1_potwIlosciSubstCzynnej_wartosc">5 mg</content>
				//</paragraph>

			}


			return xmlElement;
		}

		//public XmlElement TworzXmlElement(XmlElement parent)
		//{
		//  var xmlElement = parent.TworzElementXml("text");

		//  var xmlLek = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.Lek);
		//  xmlLek.DodajElement("content", this.Nazwa).DodajIdentyfikatorElementu("p1_nazwaLeku").DodajAtrybut("styleCode", "xPLbig");
		//  xmlLek.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
		//  xmlLek.DodajElement("content", this.MocSkladnikowLeku).DodajIdentyfikatorElementu("p1_mocSkladnikowLeku");
		//  xmlLek.DodajElement("content", null).DodajIdentyfikatorElementu("p1_edytuj_mocLeku");
		//  xmlLek.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
		//  xmlLek.DodajElement("content", this.PostacLeku).DodajIdentyfikatorElementu("p1_postacLeku");
		//  xmlLek.DodajElement("content", null).DodajIdentyfikatorElementu("p1_edytuj_postacLeku");

		//  if (this.CzyNieZamieniac)
		//  {
		//    xmlLek.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
		//    xmlLek.DodajElement("content", "NZ").DodajIdentyfikatorElementu("p1_nieZamieniac").DodajAtrybut("styleCode", "xPLbig");
		//  }

		//  var xmlIlosc = xmlElement.DodajElement("paragraph");
		//  xmlIlosc.DodajElement("content", this.Ilosc).DodajIdentyfikatorElementu("p1_iloscLeku");
		//  xmlIlosc.DodajElement("content", " op. po ").DodajIdentyfikatorElementu("p1_krotnosc_opis");
		//  xmlIlosc.DodajElement("content", this.Opakowanie).DodajIdentyfikatorElementu("p1_wielkoscOpakowania");

		//  var xmDS = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekDawkowanie);
		//  xmDS.DodajElement("content", "D.S. ").DodajIdentyfikatorElementu("p1_stosowanie_opis_1");
		//  xmDS.DodajElement("content", this.Dawkowanie).DodajIdentyfikatorElementu("p1_stosowanie_wartosc_1").DodajAtrybut("styleCode", "Bold");
		//  xmDS.DodajElement("content", null).DodajIdentyfikatorElementu("p1_edytuj_stosowanie_wartosc_1").DodajAtrybut("styleCode", "Bold");


		//  // TODO: ustalenie czy pojawia się odpłatność czy nie (czy będzie referencja w WydanieLeku) 
		//  // TODO: Moim zdaniem chyba powinna być jeszze opcja "Nie dotyczy" w wyliczeniu, która mówiłaby że nie stosyujemy tego bloku (trzeba to uwzględnić również w WydanieLeku, żeby referencja się pojawiała lub nie)
		//  if (this.odplatnosc.HasValue)
		//  {
		//    var xmOdplatnosc = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekOdplatnosc);
		//    xmOdplatnosc.DodajElement("content", "Odpłatność ").DodajIdentyfikatorElementu("p1_odplatnosc_opis");
		//    xmOdplatnosc.DodajElement("content", this.Odplatnosc.GetKod()).DodajIdentyfikatorElementu("p1_odplatnosc_wartosc").DodajAtrybut("styleCode", "Bold");
		//  }

		//  if (this.RealizacjaOd.HasValue)
		//  {
		//    var xmlRealizacjaOd = xmlElement.DodajElement("paragraph");
		//    xmlRealizacjaOd.DodajElement("content", "Data realizacji od ").DodajIdentyfikatorElementu("p1_dataRealizacjiOd_opis");
		//    xmlRealizacjaOd.DodajElement("content", this.RealizacjaOd.Value.ToString("ddMMYY")).DodajIdentyfikatorElementu("p1_dataRealizacjiOd_wartosc").DodajAtrybut("styleCode", "xPLred Bold");
		//  }

		//  if (this.SubstancjaCzynna != null)
		//  {
		//    var xmlSubstancjaCzynna = xmlElement.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekSubstancjaCzynna);
		//    xmlSubstancjaCzynna.DodajElement("content", "Potwierdzono ilość substancji czynnej: ").DodajIdentyfikatorElementu("p1_potwIlosciSubstCzynnej_opis");
		//    xmlSubstancjaCzynna.DodajElement("content", string.Format("{0} {1}", this.SubstancjaCzynna.Ilosc, this.SubstancjaCzynna.JednostkaMiary)).DodajIdentyfikatorElementu("p1_potwIlosciSubstCzynnej_wartosc");

		//    //<paragraph ID="OBS_1">
		//    //  <content ID="p1_potwIlosciSubstCzynnej_opis">Potwierdzono ilość substancji czynnej:</content>
		//    //  <content ID="p1_potwIlosciSubstCzynnej_wartosc">5 mg</content>
		//    //</paragraph>

		//  }


		//  return xmlElement;
		//}
	}



	public class SubstancjaCzynna
	{
		public string Ilosc { get; set; }
		public string JednostkaMiary { get; set; }
	}
}
