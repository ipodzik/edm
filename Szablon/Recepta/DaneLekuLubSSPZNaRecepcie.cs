﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.54-2018-09-30T000000.html
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.54
	/// </summary>
	[Szablon("[4] Dane leku lub ŚSSPŻ na recepcie", "2.16.840.1.113883.3.4424.13.10.4.54", "plCdaMedicineOrSpecialFood", WersjaSzablonu.v1_3, "manufacturedMaterial")]
	public class DaneLekuLubSSPZNaRecepcie : SzablonSkladowy
	{
		private readonly Lek lek;



		public DaneLekuLubSSPZNaRecepcie(Lek lek)
		{
			this.lek = lek;
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<DaneLekuLubSSPZNaRecepcie>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.1");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			if (!string.IsNullOrEmpty(this.lek.UrplId))
			{
				xmlElement.DodajElement(new Slownik.LekRecepty(this.lek, ZbiorWartosci.LekiUrplId));
			}
			else
			{
				xmlElement.DodajElement(new Slownik.LekRecepty(this.lek, ZbiorWartosci.LekiEAN));
			}
			xmlElement.DodajElement("name", this.lek.NazwaWyswietlana);

			// w sumie to nie jest obowiązkowa, ale jednak są ostrzeżenia  
			var pharm = "pharm";
			{
				var xmlContent = xmlElement.DodajElement("asContent", null, pharm).UstawAtrybut("classCode", "CONT");				
				var xmlContainer = xmlContent.DodajElement("containerPackagedMedicine", null, pharm).UstawAtrybut("classCode", "CONT").UstawAtrybut("determinerCode", "INSTANCE");

				xmlContainer.DodajElement(new Slownik.LekRecepty(this.lek, pharm));
				xmlContainer.DodajElement("name", this.lek.NazwaOpakowania, pharm); 
				//TODO: formCode 2.16.840.1.113883.5.1127???

				xmlContainer.DodajElement("capacityQuantity", null, pharm).UstawAtrybut("value", this.lek.OpakowanieIlosc.ToString());
			}

			return xmlElement;
		}
	}
}
