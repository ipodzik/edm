﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.56-2018-06-30T000000.html
	*/
	[Szablon("[4] Brak zgody na wydanie zamiennika leku", "2.16.840.1.113883.3.4424.13.10.4.56", "plCdaDrugPrescriptionSupplySubstitutionEntry", WersjaSzablonu.v1_3, "act")]
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.56
	/// </summary 
	public class BrakZgodyNaWydanieZmamiennikaLeku : SzablonSkladowy
	{
		private readonly Lek lek;


		/// <summary />
		public BrakZgodyNaWydanieZmamiennikaLeku(Lek lek)
		{
			this.lek = lek;
		}



		/// <summary />
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<BrakZgodyNaWydanieZmamiennikaLeku>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "DEF");

			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.9.1");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement("code").UstawAtrybut("code", "N").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.1070").UstawAtrybut("codeSystemName", "HL7 Substance Admin Substitution");

			xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.Lek));

			xmlElement.DodajElement(new StatusCodeCompleted());

			return xmlElement;
		}
	}
}
