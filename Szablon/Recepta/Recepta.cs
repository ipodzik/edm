﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.1.3-2019-09-30T000000.html
	 */
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.1.3
	/// </summary>
	[Szablon("[1] Recepta", "2.16.840.1.113883.3.4424.13.10.1.3", "plCdaDrugPrescription", WersjaSzablonu.v1_3_1, "ClinicalDocument")]
	public class Recepta : SzablonDokumentu, IRecepta
	{
		private readonly XmlDocument xmlDocument;
		public DanePacjentaDlaDokumentuRecepty Pacjent { get; private set; }
		public IAutor Autor
		{
			get { return this.autor; }
		}
		private AutorDokumentuRecepty autor;

		public KategoriaDostepnosciLeku? KategoriaDostepnosciLeku
		{
			get { return this.kategoriaDostepnosciLeku; }
			set { this.kategoriaDostepnosciLeku = this.Dokument.ZlecenieLeku.KategoriaDostepnosciLeku = value; }
		}
		private KategoriaDostepnosciLeku? kategoriaDostepnosciLeku;
		public RodzajLeku? RodzajLeku
		{
			get { return this.rodzajLeku; }
			set { this.rodzajLeku = this.Dokument.ZlecenieLeku.RodzajLeku = value; }
		}
		private RodzajLeku? rodzajLeku;
		public TrybWystawieniaRecepty? TrybWystawieniaRecepty { get; set; }
		public TrybRealizacjiRecepty? TrybRealizacjiRecepty { get; set; }

		public RodzajReceptyElektronicznej? RodzajReceptyElektronicznej { get; set; }

		public IdentyfikatorDokumentu IdentyfikatorRecepty { get; private set; }
		internal WystawcaDokumentuBazowy WystawcaDokumentu { get; private set; }

		public DateTime? Wystawiono
		{
			get { return this.wystationo; }
			set { this.wystationo = value; }
		}
		private DateTime? wystationo;

		public TrescDokumentuRecepty Dokument
		{
			get { return this.dokument; }
			private set { this.dokument = value; }
		}
		private TrescDokumentuRecepty dokument;



		public Recepta()
		{
			this.xmlDocument = new XmlDocument();

			this.Pacjent = new DanePacjentaDlaDokumentuRecepty();
			this.autor = new AutorDokumentuRecepty();
			this.IdentyfikatorRecepty = new IdentyfikatorDokumentu(ZbiorWartosci.ReceptaInstancja, true, ZbiorWartosci.ReceptaZbiorWersjiReceptIDokumentowAnulowania);
			this.WystawcaDokumentu = new WystawcaDokumentuBazowy();
			this.Dokument = new TrescDokumentuRecepty();
		}



		public override XmlDocument TworzXml()
		{
			this.xmlDocument.AppendChild(this.xmlDocument.CreateXmlDeclaration("1.0", Encoding.UTF8.HeaderName, null));
			this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.xsl\" type=\"text/xsl\""));

			var hl7Szablon = AttributeHelper<Recepta>.GetCustomAttribute<SzablonAttribute>();
			var xmlElement = this.xmlDocument.CreateElement(hl7Szablon.XmlElementNazwa);
			this.xmlDocument.AppendChild(xmlElement);

			this.xmlDocument.DocumentElement.SetAttribute("xmlns", "urn:hl7-org:v3");
			this.xmlDocument.DocumentElement.SetAttribute("xmlns:extPL", "http://www.csioz.gov.pl/xsd/extPL/r2");
			this.xmlDocument.DocumentElement.SetAttribute("xmlns:pharm", "urn:ihe:pharm");

			this.xmlDocument.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			this.xmlDocument.DocumentElement.SetAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", "extPL:ClinicalDocument");

			xmlElement.DodajTypII("POCD_HD000040", "2.16.840.1.113883.1.3", "typeId");

			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.1.1");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.1.1");
			xmlElement.DodajIdentyfikatorSzablonu(this).UstawAtrybut("extension", hl7Szablon.Wersja);

			xmlElement.DodajTypII(this.IdentyfikatorRecepty.IdentyfikatorInstancji);
			if (!string.IsNullOrEmpty(this.IdentyfikatorRecepty.Instancja))
			{
				this.Dokument.ZlecenieLeku.Identyfikator.Extension = this.IdentyfikatorRecepty.Instancja;
				this.Dokument.ZlecenieLeku.PozycjaRecepty.Identyfikator.Extension = this.IdentyfikatorRecepty.Instancja + "-1";
			}

			var xmlCodeRecepta = xmlElement.DodajElement("code");
			{
				xmlCodeRecepta.UstawAtrybut("code", "57833-6");
				xmlCodeRecepta.UstawAtrybut("codeSystem", "2.16.840.1.113883.6.1");
				xmlCodeRecepta.UstawAtrybut("codeSystemName", "LOINC");
				xmlCodeRecepta.UstawAtrybut("displayName", "Prescription for medication Document");

				var xmlTransalation = xmlCodeRecepta.DodajElement("translation");
				{

					xmlTransalation.UstawAtrybut("code", "04.01");
					xmlTransalation.UstawAtrybut("codeSystem", "2.16.840.1.113883.3.4424.11.1.32");
					xmlTransalation.UstawAtrybut("codeSystemName", "KLAS_DOK_P1");
					xmlTransalation.UstawAtrybut("displayName", "Recepta");

					var oidEnumAttributePolskieKlasyfikatoryHL7v3 = SystemKodowania.PolskieKlasyfikatoryHL7v3.GetOidEnumAttribute();

					var xmlQualifier1 = xmlTransalation.DodajElement("qualifier");
					{
						var oidEnumAttributeKategoriaDostepnosciLeku = SystemKodowania.KategoriaDostepnosciLeku.GetOidEnumAttribute();

						var xmlQualifierName = xmlQualifier1.DodajElement("name");
						xmlQualifierName.UstawAtrybut("code", "KDLEK");
						xmlQualifierName.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						xmlQualifierName.UstawAtrybut("codeSystemName", oidEnumAttributePolskieKlasyfikatoryHL7v3.Nazwa);
						xmlQualifierName.UstawAtrybut("displayName", oidEnumAttributeKategoriaDostepnosciLeku.Nazwa);

						if (this.KategoriaDostepnosciLeku.HasValue)
						{
							var xmlQualifierValue = xmlQualifier1.DodajElement("value");
							xmlQualifierValue.UstawAtrybut("code", this.KategoriaDostepnosciLeku.Value.GetKod());
							xmlQualifierValue.UstawAtrybut("codeSystem", oidEnumAttributeKategoriaDostepnosciLeku.Kod);
						}
					}

					var xmlQualifier2 = xmlTransalation.DodajElement("qualifier");
					{
						var xmlQualifierName = xmlQualifier2.DodajElement("name");
						xmlQualifierName.UstawAtrybut("code", "RLEK");
						xmlQualifierName.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						xmlQualifierName.UstawAtrybut("codeSystemName", oidEnumAttributePolskieKlasyfikatoryHL7v3.Nazwa);
						xmlQualifierName.UstawAtrybut("displayName", AttributeHelper<RodzajLeku>.GetCustomAttribute<OidEnumAttribute>().Nazwa);

						if (this.RodzajLeku.HasValue)
						{
							var enumAttribute = this.RodzajLeku.Value.GetEnumAttribute();

							var xmlQualifierValue = xmlQualifier2.DodajElement("value");
							xmlQualifierValue.UstawAtrybut("code", enumAttribute.Kod);
							xmlQualifierValue.UstawAtrybut("displayName", enumAttribute.Nazwa);
							xmlQualifierValue.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						}
					}

					var xmlQualifier3 = xmlTransalation.DodajElement("qualifier");
					{

						var xmlQualifierName = xmlQualifier3.DodajElement("name");
						xmlQualifierName.UstawAtrybut("code", "TWREC");
						xmlQualifierName.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						xmlQualifierName.UstawAtrybut("codeSystemName", oidEnumAttributePolskieKlasyfikatoryHL7v3.Nazwa);
						xmlQualifierName.UstawAtrybut("displayName", AttributeHelper<TrybWystawieniaRecepty>.GetCustomAttribute<OidEnumAttribute>().Nazwa);

						if (this.TrybWystawieniaRecepty.HasValue)
						{
							var enumAttribute = this.TrybWystawieniaRecepty.Value.GetEnumAttribute();

							var xmlQualifierValue = xmlQualifier3.DodajElement("value");
							xmlQualifierValue.UstawAtrybut("code", enumAttribute.Kod);
							xmlQualifierValue.UstawAtrybut("displayName", enumAttribute.Nazwa);
							xmlQualifierValue.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						}
					}

					var xmlQualifier4 = xmlTransalation.DodajElement("qualifier");
					{

						var xmlQualifierName = xmlQualifier4.DodajElement("name");
						xmlQualifierName.UstawAtrybut("code", "TRREC");
						xmlQualifierName.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						xmlQualifierName.UstawAtrybut("codeSystemName", oidEnumAttributePolskieKlasyfikatoryHL7v3.Nazwa);
						xmlQualifierName.UstawAtrybut("displayName", AttributeHelper<TrybRealizacjiRecepty>.GetCustomAttribute<OidEnumAttribute>().Nazwa);

						if (this.TrybRealizacjiRecepty.HasValue)
						{
							var enumAttribute = this.TrybRealizacjiRecepty.Value.GetEnumAttribute();

							var xmlQualifierValue = xmlQualifier4.DodajElement("value");
							xmlQualifierValue.UstawAtrybut("code", enumAttribute.Kod);
							xmlQualifierValue.UstawAtrybut("displayName", enumAttribute.Nazwa);
							xmlQualifierValue.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						}
					}

					if (this.RodzajReceptyElektronicznej.HasValue)
					{
						var xmlQualifier5 = xmlTransalation.DodajElement("qualifier");
						{

							var xmlQualifierName = xmlQualifier5.DodajElement("name");
							xmlQualifierName.UstawAtrybut("code", "RRECE");
							xmlQualifierName.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
							xmlQualifierName.UstawAtrybut("codeSystemName", oidEnumAttributePolskieKlasyfikatoryHL7v3.Nazwa);
							xmlQualifierName.UstawAtrybut("displayName", AttributeHelper<RodzajReceptyElektronicznej>.GetCustomAttribute<OidEnumAttribute>().Nazwa);

							var enumAttribute = this.RodzajReceptyElektronicznej.Value.GetEnumAttribute();

							var xmlQualifierValue = xmlQualifier5.DodajElement("value");
							xmlQualifierValue.UstawAtrybut("code", enumAttribute.Kod);
							xmlQualifierValue.UstawAtrybut("displayName", enumAttribute.Nazwa);
							xmlQualifierValue.UstawAtrybut("codeSystem", oidEnumAttributePolskieKlasyfikatoryHL7v3.Kod);
						}
					}
				}
			}

			string tytul = "Recepta";
			{
				if (this.TrybWystawieniaRecepty.HasValue)
				{
					switch (this.TrybWystawieniaRecepty.Value)
					{
						//case Slownik.TrybWystawieniaRecepty.Farmaceutyczna:
						//  break;
						case Slownik.TrybWystawieniaRecepty.Pielegniarska:
						case Slownik.TrybWystawieniaRecepty.PielegniarskaNaZlecenia:
							{
								tytul += " " + this.TrybWystawieniaRecepty.Value.GetNazwa().ToLower();
							}
							break;
						default:
							break;
					}
				}

				xmlElement.DodajElement(xmlElement, "title", tytul);
			}

			if (this.Wystawiono.HasValue)
			{
				this.autor.UtworzenieDokumentu = this.Dokument.ZlecenieLeku.PozycjaRecepty.Wystawiono = this.WystawcaDokumentu.Wystawiono = this.Wystawiono;
				xmlElement.DodajElement("effectiveTime").UstawAtrybut("value", this.Wystawiono.Value.ToString("yyyyMMdd"));
			}

			xmlElement.DodajElement("confidentialityCode").UstawAtrybut("code", "N").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.25");

			xmlElement.DodajElement("languageCode").UstawAtrybut("code", "pl-PL");

			xmlElement.DodajTypII(this.IdentyfikatorRecepty.IdentyfikatorZbioruWersji);

			xmlElement.DodajElement("versionNumber").UstawAtrybut("value", this.IdentyfikatorRecepty.Wersja.ToString());

			xmlElement.DodajElement(this.Pacjent);
			xmlElement.DodajElement(this.autor);

			xmlElement.DodajElement(new OrganizacjaOdpowiedzialnaZaDokumentDlaP1());

			this.WystawcaDokumentu.Identyfikator = this.autor.Identyfikator;
			xmlElement.DodajElement(this.WystawcaDokumentu);

			//this.Dokument.ZlecenieLeku.PozycjaRecepty.Identyfikator.Wartosc = this.IdentyfikatorRecepty.IdentyfikatorInstancji.Wartosc + "-1";

			if (!string.IsNullOrEmpty(this.Pacjent.KodOddzialuNFZ))
			{
				//this.Dokument.DaneUbezpieczeniowe = new SekcjaDanychUbezpieczeniowych(this.Dokument, null);
				this.Dokument.DaneUbezpieczeniowe.PlatnikNazwa = "Oddział NFZ";
				this.Dokument.DaneUbezpieczeniowe.PlatnikKod = this.Pacjent.KodOddzialuNFZ;
				this.Dokument.ZlecenieLeku.PozycjaRecepty.WydanieLeku.Odplatnosc.Platnik.Kod = this.Pacjent.KodOddzialuNFZ;
			}
			xmlElement.DodajElement(this.Dokument);


			this.daneKontekstuWywolania = new DaneKontekstuWywolania(this.autor.Identyfikator, this.autor.JednostkaOrganizacyjna.Podmiot, this.autor.JednostkaOrganizacyjna.KomorkaOrganizacyjna);

			return this.xmlDocument;
		}
	}
}
