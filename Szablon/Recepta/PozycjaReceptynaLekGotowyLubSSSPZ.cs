﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.3-2018-06-30T000000.html
	*/
	[Szablon("[4] Pozycja recepty na lek gotowy lub ŚSSPŻ", "2.16.840.1.113883.3.4424.13.10.4.3", "plCdaDrugPrescriptionEntry", WersjaSzablonu.v1_3_1_2, "substanceAdministration")]
	public class PozycjaReceptyNaLekGotowyLubSSSPZ : SzablonSkladowy, IPozycjaRecepty
	{
		private readonly Lek lek;

		internal IdentyfikatorInstancji Identyfikator { get; private set; }
		internal DateTime? Wystawiono { get; set; }

		internal BrakZgodyNaWydanieZmamiennikaLeku NieZamieniac
		{
			get { return this.nieZamieniac; }
			set { this.nieZamieniac = value; }
		}
		private BrakZgodyNaWydanieZmamiennikaLeku nieZamieniac;

		internal DaneLekuLubSSPZNaRecepcie Dane
		{
			get { return this.dane; }
			private set { this.dane = value; }
		}
		private DaneLekuLubSSPZNaRecepcie dane;

		internal WydanieLeku WydanieLeku
		{
			get { return this.wydanieLeku; }
			private set { this.wydanieLeku = value; }
		}
		private WydanieLeku wydanieLeku;


		internal InformacjeOPrzyjmowaniuLekuDlaPacjenta InformacjaPrzyjmowanieLeku
		{
			get { return this.informacjaPrzyjmowanieLeku; }
		}
		private InformacjeOPrzyjmowaniuLekuDlaPacjenta informacjaPrzyjmowanieLeku;

		internal InformacjaDlaOsobyWydajacejLek InformacjaWydanieLeku
		{
			get { return this.informacjaWydanieLeku; }
		}
		private InformacjaDlaOsobyWydajacejLek informacjaWydanieLeku;



		public PozycjaReceptyNaLekGotowyLubSSSPZ(Lek lek)
		{
			this.lek = lek;
			this.Identyfikator = new IdentyfikatorInstancji(ZbiorWartosci.ReceptaPozycja);
			this.Dane = new DaneLekuLubSSPZNaRecepcie(lek);
			this.WydanieLeku = new WydanieLeku(lek);
			this.informacjaPrzyjmowanieLeku = new InformacjeOPrzyjmowaniuLekuDlaPacjenta(this.lek);
			this.informacjaWydanieLeku = new InformacjaDlaOsobyWydajacejLek(this.lek);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<PozycjaReceptyNaLekGotowyLubSSSPZ>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			xmlElement.UstawAtrybut("classCode", "SBADM").UstawAtrybut("moodCode", "INT");

			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.2");
			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.24");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.7");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.6");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.7.1");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajTypII(this.Identyfikator);
			xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.Lek));
			xmlElement.DodajElement(new StatusCodeCompleted());

			var xmlEffectiveTime = xmlElement.DodajElement("effectiveTime").UstawAtrybut("type", "xsi", "IVL_TS");
			{
				xmlEffectiveTime.DodajElement("low").UstawAtrybut("value", this.Wystawiono.Value.ToString("yyyyMMdd"));
				xmlEffectiveTime.DodajElement("high").UstawAtrybut("nullFlavor", "NA");
			}

			// repeatNumber - dotyczy powtórzeń podania przepisanego 'podania leku' jako całości, zwykle ma wartość 0 -->
			xmlElement.DodajElement("repeatNumber").UstawAtrybut("value", "0");


			var xmlManufacturedProduct = xmlElement.DodajElement("consumable").DodajElement("manufacturedProduct");
			xmlManufacturedProduct.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.53");
			xmlManufacturedProduct.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.7.2");
			xmlManufacturedProduct.DodajElement(this.Dane);

			if (this.lek.RealizacjaOd.HasValue)
			{
				this.WydanieLeku.RealizacjaOd = this.lek.RealizacjaOd;
			}
			else
			{
				this.WydanieLeku.RealizacjaOd = this.Wystawiono;
			}
			xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP").DodajElement(this.WydanieLeku);


			if (this.lek.CzyNieZamieniac)
			{
				this.NieZamieniac = new BrakZgodyNaWydanieZmamiennikaLeku(this.lek);
				xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP").DodajElement(this.NieZamieniac);
			}

			xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "SUBJ").UstawAtrybut("inversionInd", "true").DodajElement(this.informacjaPrzyjmowanieLeku);

			if (!string.IsNullOrEmpty(this.lek.InformacjaDlaOsobyWydajacej))
			{
				xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "SUBJ").UstawAtrybut("inversionInd", "true").DodajElement(this.informacjaWydanieLeku);
			}



			//if (this.lek.RealizacjaDo.HasValue)
			//{
			//  var xmlSupply = xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP")
			//    .DodajElement("supply").UstawAtrybut("classCode", "SPLY").UstawAtrybut("moodCode", "RQO");

			//  xmlSupply.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.15");

			//  var xmlEffectiveTime2 = xmlSupply.DodajElement("effectiveTime").UstawAtrybut("type", "xsi", "IVL_TS");
			//  {
			//    xmlEffectiveTime2.DodajElement("high").UstawAtrybut("value", this.lek.RealizacjaDo.Value.ToString("yyyyMMdd"));
			//  }
			//}

			return xmlElement;
		}
	}
}
