﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.80-2018-09-30T000000.html 
	*/
	/// <summary>
	/// 	2.16.840.1.113883.3.4424.13.10.4.80
	/// </summary>
	[Szablon("[4] Potwierdzenie całkowitej dawki substancji czynnej", "2.16.840.1.113883.3.4424.13.10.4.80", "plCdaActiveSubstanceTotalDoseConfirmation", WersjaSzablonu.v1_3_1, "observation")]
	public class PotwierdzenieCalkowitejDawkiSubstancjiCzynnej : SzablonSkladowy
	{
		private readonly Lek lek;



		public PotwierdzenieCalkowitejDawkiSubstancjiCzynnej(Lek lek)
		{
			this.lek = lek;
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (this.lek.SubstancjaCzynna != null)
			{
				xmlElement = parent.TworzElementXml(this);

				xmlElement.UstawAtrybut("classCode", "OBS").UstawAtrybut("moodCode", "EVN");

				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.DodajTypCD("CDSC", "2.16.840.1.113883.3.4424.13.5.3.14");
				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.LekSubstancjaCzynna));
				xmlElement.DodajTypCS();

				xmlElement.DodajTypPQ("value", this.lek.SubstancjaCzynna.Ilosc, this.lek.SubstancjaCzynna.JednostkaMiary);
			}

			return xmlElement;
		}
	}
}
