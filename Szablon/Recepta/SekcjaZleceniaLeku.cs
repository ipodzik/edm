﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.3.4-2018-06-30T000000.html
	*/
	[Szablon("[3] Sekcja zalecenia leku", "2.16.840.1.113883.3.4424.13.10.3.4", "plCdaDrugPrescriptionSection", WersjaSzablonu.v1_3, "section")]
	public class SekcjaZleceniaLeku : SzablonSkladowy
	{
		public IdentyfikatorInstancji Identyfikator { get; private set; }
		internal KategoriaDostepnosciLeku? KategoriaDostepnosciLeku { get; set; }

		public Lek Lek
		{
			get { return this.lek; }
			private set { this.lek = value; }
		}
		private Lek lek;

		internal RodzajLeku? RodzajLeku
		{
			get { return this.rodzajLeku; }
			set { this.UstawRodzjLeku(value); }
		}
		private RodzajLeku? rodzajLeku;

		internal PozycjaReceptyNaLekGotowyLubSSSPZ PozycjaRecepty { get; private set; }



		public SekcjaZleceniaLeku()
		{
			this.Lek = new Lek();
			this.Identyfikator = new IdentyfikatorInstancji(ZbiorWartosci.ReceptySekcjaZleceniaLeku);
		}



		private void UstawRodzjLeku(RodzajLeku? rodzajLeku)
		{
			this.rodzajLeku = rodzajLeku;

			this.PozycjaRecepty = null;

			switch (this.rodzajLeku)
			{
				case Slownik.RodzajLeku.LekGotowy:
				case Slownik.RodzajLeku.SrodekSpozywczySpecjalnegoPrzeznaczeniaZywieniowego:
					{
						this.PozycjaRecepty = new PozycjaReceptyNaLekGotowyLubSSSPZ(this.Lek);
						break;
					}
				//case Slownik.RodzajLeku.LekRecepturowy:
				//  {

				//    break;
				//  }
				//case Slownik.RodzajLeku.WyrobMedyczny:
				//  {

				//    break;
				//  }
				default:
					throw new NotImplementedException();
				//break;
			}
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<SekcjaZleceniaLeku>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.2.1");
			xmlElement.DodajIdentyfikatorSzablonu(this);
			xmlElement.DodajElement(this.Identyfikator);

			xmlElement.DodajElement("code").
				UstawAtrybut("code", "57828-6").
				UstawAtrybut("codeSystem", "2.16.840.1.113883.6.1").
				UstawAtrybut("codeSystemName", "LOINC").
				UstawAtrybut("displayName", "Prescriptions");

			if (this.KategoriaDostepnosciLeku.HasValue)
			{
				var tytul = this.KategoriaDostepnosciLeku.GetNazwa();
				if (this.Lek.CzyCito)
				{
					tytul += " (CITO)";
				}
				xmlElement.DodajElement("title", tytul);
			}

			//this.Lek.Id = "SBADM_1"; // ID jest chyba i tak na sztywno podawany, więc podaję go w samym obiekcie Lek
			//this.PozycjaRecepty.OpisWyrazeniaKlinicznego.DodajReferencje(this.Lek.Id);
			xmlElement.DodajElement(this.Lek);

			xmlElement.DodajElement("entry").DodajElement(this.PozycjaRecepty);

			return xmlElement;
		}
	}
}
