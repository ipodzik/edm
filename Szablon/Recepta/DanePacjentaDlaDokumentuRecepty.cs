﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using System.Data;
using BIUINF.Lib4.Data;
using System.Data.Common;
using BIUINF.Lib4.HL7CDA.Slownik;
using BIUINF.Lib4.HL7CDA.MedInf;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.2.23-2018-09-30T000000.html 
	*/
	[Szablon("[2] Dane pacjenta dla dokumentu recepty", "2.16.840.1.113883.3.4424.13.10.2.23", "plCdaDrugPrescriptionRecordTarget", WersjaSzablonu.v1_3_1, "recordTarget")]
	public class DanePacjentaDlaDokumentuRecepty : SzablonSkladowy
	{
		public int? MedInfId { get; private set; }

		public IdentyfikatorPacjenta Identyfikator { get; private set; }
		public Plec? Plec { get; set; }
		public DateTime? Urodzony { get; set; }

		internal string KodOddzialuNFZ { get; private set; }

		public AdresBazowy Adres
		{
			get { return this.adres; }
		}
		private AdresBazowy adres;

		public NazwiskoImieOsobyBazowy NazwiskoImie
		{
			get { return this.nazwiskoImie; }
		}
		private NazwiskoImieOsobyBazowy nazwiskoImie;



		internal DanePacjentaDlaDokumentuRecepty()
		{
			this.adres = new AdresBazowy();
			this.nazwiskoImie = new NazwiskoImieOsobyBazowy();
			//this.IdentyfikatorOsoby = new IdentyfikatorOsoby();
			this.Identyfikator = new IdentyfikatorPacjenta();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.DodajIdentyfikatorSzablonu(this);

			var xmlPatientRole = xmlElement.DodajElement("patientRole");

			if (this.MedInfId.HasValue)
			{
				var oid = new IdentyfikatorInstancji(ZbiorWartosci.SystemUslugodawcyIdentyfikatorPacjenta, this.MedInfId.Value.ToString(), false);
				xmlPatientRole.DodajElement(oid);
			}
			xmlPatientRole.DodajElement(this.Identyfikator);
			xmlPatientRole.DodajElement(this.Adres);

			var xmlPatient = xmlPatientRole.DodajElement("patient");
			xmlPatient.DodajElement(this.NazwiskoImie);

			if (this.Plec.HasValue)
			{
				var xmlPlec = xmlPatient.DodajElement("administrativeGenderCode");
				xmlPlec.SetAttribute("code", this.Plec.Value.GetKod());
				xmlPlec.SetAttribute("codeSystem", AttributeHelper<Plec>.GetCustomAttribute<OidEnumAttribute>().Kod);
			}

			if (this.Urodzony.HasValue)
			{
				var xmlUrodzony = xmlPatient.DodajElement("birthTime");
				xmlUrodzony.SetAttribute("value", this.Urodzony.Value.ToString("yyyyMMdd"));
			}

			return xmlElement;
		}

		public void PobierzDane(int medInfId)
		{
			var pacjent = MedInf.Pacjent.Pobierz(medInfId);

			if (pacjent != null)
			{
				this.MedInfId = medInfId;
				this.NazwiskoImie.Nazwisko = pacjent.Nazwisko;
				this.NazwiskoImie.Imie = pacjent.Imie;
				this.NazwiskoImie.Imie2 = pacjent.Imie2;
				this.Identyfikator = pacjent.Identyfikator;
				this.Urodzony = pacjent.Urodzony;
				this.Plec = pacjent.Plec;
				this.KodOddzialuNFZ = pacjent.KodOddzialuNFZ;

				if (string.IsNullOrEmpty(this.KodOddzialuNFZ))
				{
					throw BrakDanychMedInfExclamationException.Create("Brak kodu o/w NFZ w danych pacjenta!");
				}

				if (pacjent.Adres != null)
				{
					this.Adres.Panstwo = pacjent.Adres.Panstwo;
					this.Adres.Ulica = pacjent.Adres.Ulica;
					this.Adres.NrDomu = pacjent.Adres.NrDomu;
					this.Adres.NrLokalu = pacjent.Adres.NrLokalu;
					this.Adres.Miasto = pacjent.Adres.Miejscowosc;
					this.Adres.TERYT = pacjent.Adres.TERYT;
					this.Adres.KodPocztowy.Kod = pacjent.Adres.PocztaKod;
					this.Adres.KodPocztowy.Poczta = pacjent.Adres.Poczta;
				}
			}
		}
	}
}
