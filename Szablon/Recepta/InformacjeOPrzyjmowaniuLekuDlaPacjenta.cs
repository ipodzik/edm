﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.56-2018-06-30T000000.html
	*/
	[Szablon("[4] Informacje o przyjmowaniu leku dla pacjenta", "2.16.840.1.113883.3.4424.13.10.4.74", "plCdaDrugPrescriptionPatientMedicationInstructions", WersjaSzablonu.v1_3, "act")]
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.74
	/// </summary 
	public class InformacjeOPrzyjmowaniuLekuDlaPacjenta : SzablonSkladowy
	{
		private readonly Lek lek;


		/// <summary />
		public InformacjeOPrzyjmowaniuLekuDlaPacjenta(Lek lek)
		{
			this.lek = lek;
		}



		/// <summary />
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.lek.Dawkowanie))
			{
				xmlElement = parent.TworzElementXml(this);

				xmlElement.UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "INT");

				xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.3");
				xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.49");
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.DodajElement("code").UstawAtrybut("code", "PINSTRUCT").UstawAtrybut("codeSystem", "1.3.6.1.4.1.19376.1.5.3.2").UstawAtrybut("codeSystemName", "IHEActCode");

				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.LekDawkowanie));

				xmlElement.DodajElement(new StatusCodeCompleted());
			}

			return xmlElement;
		}
	}
}
