﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.4.75-2018-06-30T000000.html 
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.75
	/// </summary>
	[Szablon("[4] Informacje dla osoby wydającej lek", "2.16.840.1.113883.3.4424.13.10.4.75", "plCdaDrugPrescriptionFulfillmentInstructions", WersjaSzablonu.v1_3, "act")]
	public class InformacjaDlaOsobyWydajacejLek : SzablonSkladowy
	{
		private readonly Lek lek;



		public InformacjaDlaOsobyWydajacejLek(Lek lek)
		{
			this.lek = lek;
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(this.lek.InformacjaDlaOsobyWydajacej))
			{
				xmlElement = parent.TworzElementXml(this);

				xmlElement = parent.TworzElementXml(this);

				xmlElement.UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "INT");

				xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.43");
				xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.3.1");
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.DodajTypCD("FINSTRUCT", "1.3.6.1.4.1.19376.1.5.3.2", "IHEActCode", null);
				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.InformacjaDlaOsobyWydjacejLek));
				xmlElement.DodajTypCS();
			}


			return xmlElement;
		}
	}
}
