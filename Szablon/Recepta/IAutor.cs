﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/// <summary>
	/// Interfejs autora dokumentu recepty
	/// </summary>
	public interface IAutor
	{
		void PobierzDane(int medInfId);
		JednostkaPodmiotuWykonujacegoDzialanoscLecznicza JednostkaOrganizacyjna { get; set; }

		DaneUmowyZwiazanejZRefundacja Umowa { get; }

		IdentyfikatorPersonelu Identyfikator { get; }
	}
}
