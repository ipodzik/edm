﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.4.48-2019-09-30T000000.html
	*/
	[Szablon("[4] Pozycja recepty na lek recepturowy", "2.16.840.1.113883.3.4424.13.10.4.48", "plCdaMagistralFormulaPrescriptionEntry", WersjaSzablonu.v1_3_1_2, "substanceAdministration")]
	public class PozycjaReceptyNaLekRecepturowy : SzablonSkladowy, IPozycjaRecepty
	{

		internal IdentyfikatorInstancji Identyfikator { get; private set; }
		internal DateTime? Wystawiono { get; set; }





		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.UstawAtrybut("classCode", "SBADM").UstawAtrybut("moodCode", "INT");

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.24");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.7");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.2");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.9.1.3.6");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.4.7.1");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajTypII(this.Identyfikator);
			xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.Lek));
			xmlElement.DodajElement(new StatusCodeCompleted());

			

			return xmlElement;
		}
		
	}
}
