﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.57-2018-06-30T000000.html
	*/
	/// <summary>
	/// <c>2.16.840.1.113883.3.4424.13.10.4.57</c>
	/// Poziom odpłatności leku
	/// </summary>
	[Szablon("[4] Poziom odpłatności leku", "2.16.840.1.113883.3.4424.13.10.4.57", "plCdaDrugPaymentLevel", WersjaSzablonu.v1_3, "act")]
	public class PoziomOdplatnosciLeku : SzablonSkladowy
	{
		private readonly Lek lek;
		internal PodmiotFinansujacyZeSrodkowPublicznych Platnik
		{
			get { return this.platnik; }
		}
		private readonly PodmiotFinansujacyZeSrodkowPublicznych platnik;



		public PoziomOdplatnosciLeku(Lek lek)
		{
			this.lek = lek;
			this.platnik = new PodmiotFinansujacyZeSrodkowPublicznych();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<PoziomOdplatnosciLeku>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa).
				UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "DEF");

			xmlElement.DodajIdentyfikatorSzablonu(this);
			xmlElement.DodajElement("code").UstawAtrybut("code", "48768-6").UstawAtrybut("codeSystem", "2.16.840.1.113883.6.1").UstawAtrybut("displayName", "Payment source");


			// Opis wyrażenia klinicznego
			// TODO: referencja musi wskazywać na istniejący elemnt z podanym ID, który zawiera opis odplatności w bloku narracyjnym np. na <paragraph ID='ACT_odplatnosc'>...
			if (this.lek.Odplatnosc.HasValue)
			{
				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.LekOdplatnosc));
			}
			xmlElement.DodajElement(new StatusCodeCompleted());

			var xmlAct = xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP")
				.DodajElement("act").UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "EVN");

			var xmlQualifier = xmlAct.DodajElement("code").UstawAtrybut("code", "PUBLICPOL").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.4")
				.DodajElement("qualifier");

			var systemKodowaniaPolskieKlasyfikatoryHL7v3 = SystemKodowania.PolskieKlasyfikatoryHL7v3.GetOidEnumAttribute();
			var systemkodowaniaPoziomOdplatnosci = SystemKodowania.PoziomOdplatnosciZaLeki.GetOidEnumAttribute();
			xmlQualifier.DodajElement("name").UstawAtrybut("code", "RLPO").UstawAtrybut("codeSystem", systemKodowaniaPolskieKlasyfikatoryHL7v3.Kod).UstawAtrybut("codeSystemName", systemKodowaniaPolskieKlasyfikatoryHL7v3.Nazwa).UstawAtrybut("displayName", systemkodowaniaPoziomOdplatnosci.Nazwa);
			xmlQualifier.DodajElement("value")
				.UstawAtrybut("code", this.lek.Odplatnosc.GetKod())
				.UstawAtrybut("codeSystem", systemkodowaniaPoziomOdplatnosci.Kod)
				.UstawAtrybut("displayName", this.lek.Odplatnosc.GetNazwa());

			xmlAct.DodajElement(new StatusCodeCompleted());

			xmlAct.DodajElement(new PerformerAssignedEntity(this.Platnik));

			return xmlElement;
		}
	}
}
