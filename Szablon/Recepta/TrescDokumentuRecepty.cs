﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.2.25-2018-06-30T000000.html
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.25
	/// </summary>
	[Szablon("[2] Treść dokumentu recepty", "2.16.840.1.113883.3.4424.13.10.2.25", "plCdaDrugPrescriptionComponent", WersjaSzablonu.v1_3, "component")]
	public class TrescDokumentuRecepty : SzablonSkladowy
	{
		public SekcjaZleceniaLeku ZlecenieLeku { get; private set; }
		public SekcjaDanychUbezpieczeniowych DaneUbezpieczeniowe { get; private set; }



		public TrescDokumentuRecepty()
		{
			this.ZlecenieLeku = new SekcjaZleceniaLeku();
			this.DaneUbezpieczeniowe = new SekcjaDanychUbezpieczeniowych(this.ZlecenieLeku);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<TrescDokumentuRecepty>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.DodajIdentyfikatorSzablonu(this);

			var xmlBody = xmlElement.DodajElement("structuredBody");
			xmlBody.DodajElement("component").DodajElement(this.ZlecenieLeku);
			xmlBody.DodajElement("component").DodajElement(this.DaneUbezpieczeniowe);

			return xmlElement;
		}
	}
}
