﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	public interface IRecepta
	{
		TrescDokumentuRecepty Dokument { get; }
		IAutor Autor { get; }
		DanePacjentaDlaDokumentuRecepty Pacjent { get; }
		IdentyfikatorDokumentu IdentyfikatorRecepty { get; }
		DateTime? Wystawiono { get; set; }

		KategoriaDostepnosciLeku? KategoriaDostepnosciLeku { get; set; }
		RodzajLeku? RodzajLeku { get; set; }
		TrybWystawieniaRecepty? TrybWystawieniaRecepty { get; set; }
		TrybRealizacjiRecepty? TrybRealizacjiRecepty { get; set; }

		DaneKontekstuWywolania DaneKontekstuWywolania { get; }

		XmlDocument TworzXml();
	}
}
