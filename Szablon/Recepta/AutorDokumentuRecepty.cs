﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;
using System.Data;
using BIUINF.Lib4.Data;
using System.Data.Common;
using BIUINF.Lib4.HL7CDA.MedInf;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.2.82-2019-09-30T000000.html
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.82
	/// </summary>
	[Szablon("[2] Autor dokumentu recepty", "2.16.840.1.113883.3.4424.13.10.2.82", "plCdaDrugPrescriptionAuthor", WersjaSzablonu.v1_3_1_2, "author")]
	public class AutorDokumentuRecepty : SzablonSkladowy, IAutor
	{
		public int? MedInfId { get; private set; }

		public ZawodMedyczny ZawodMedyczny { get; private set; }
		internal DateTime? UtworzenieDokumentu { get; set; }

		public IdentyfikatorPersonelu Identyfikator { get; private set; }
		//internal SpecjalnoscLekarska SpecjalnoscLekarska { get; private set; }

		public OsobaBazowy Osoba
		{
			get { return this.osoba;}
		}
		private OsobaBazowy osoba;


		/// <summary>
		/// Zamiennie z KomorkaOrganizacyjna lub PraktykaMedyczna 
		/// </summary>
		public JednostkaPodmiotuWykonujacegoDzialanoscLecznicza JednostkaOrganizacyjna { get; set; }
		/// <summary>
		/// Zamiennie z JednostkaOrganizacyjna lub PraktykaMedyczna 
		/// </summary>
		public KomorkaPodmiotuWykonujacegoDzialalnoscLeczniczaBazowy KomorkaOrganizacyjna { get; set; }
		/// <summary>
		/// Zamiennie z JednostkaOrganizacyjna lub PraktykaMedyczna 
		/// </summary>
		public PraktykaMedycznaBazowy PraktykaMedyczna { get; set; }

		public DaneUmowyZwiazanejZRefundacja Umowa { get; private set; }



		public AutorDokumentuRecepty()
		{
			this.Identyfikator = new IdentyfikatorPersonelu();
			//this.SpecjalnoscLekarska = new SpecjalnoscLekarska();

			this.osoba = new OsobaBazowy();
			this.ZawodMedyczny = new ZawodMedyczny();
			//this.JednostkaOrganizacyjna = new JednostkaPodmiotuWykonujacegoDzialanoscLecznicza();
			//this.PraktykaMedyczna = new PraktykaMedycznaBazowy();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.DodajIdentyfikatorSzablonu(this);

			if (this.ZawodMedyczny.Rodzaj.HasValue)
			{
				xmlElement.DodajElement(this.ZawodMedyczny);
			}

			if (this.UtworzenieDokumentu.HasValue)
			{
				xmlElement.DodajElement("time").UstawAtrybut("value", this.UtworzenieDokumentu.Value.ToString("yyyyMMdd"));
			}

			var xmlAssignedAuthor = xmlElement.DodajElement("assignedAuthor");
			xmlAssignedAuthor.DodajElement(this.Identyfikator);
			//xmlAssignedAuthor.DodajElement(this.SpecjalnoscLekarska);
			xmlAssignedAuthor.DodajElement(this.Osoba);

			xmlAssignedAuthor.DodajElement(this.PraktykaMedyczna);
			xmlAssignedAuthor.DodajElement(this.JednostkaOrganizacyjna);
			xmlAssignedAuthor.DodajElement(this.KomorkaOrganizacyjna);

			return xmlElement;
		}

		public void PobierzDane(int medInfId)
		{
			var pracownik = Pracownik.Pobierz(medInfId);

			if (pracownik != null)
			{
				this.MedInfId = pracownik.Id;
				this.osoba.NazwiskoImie.Imie = pracownik.Imie;
				this.osoba.NazwiskoImie.Nazwisko = pracownik.Nazwisko;
				this.ZawodMedyczny.Rodzaj = pracownik.RodzajZawoduMedycznego;
				this.Identyfikator = pracownik.Identyfikator;
			}
		}
	}
}
