﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[HL7XmlName("wholeOrganization")]
	public class PelnaOrganizacja : IElementSzablonu
	{
		public WpisDoRejestruIzby WpisDoIzby { get; private set; }
		public KomorkaOrganizacyjna KomorkaOrganizacyjna { get; private set; }
		public Podmiot Podmiot { get; private set; }
		public Regon REGON { get; private set; }


		public Kontakt Kontakt
		{
			get { return this.kontakt; }
			private set { this.kontakt = value; }
		}
		private Kontakt kontakt;

		[HL7XmlName("name")]
		public string Nazwa { get; set; }

		public AdresOrganizacjiPolska Adres
		{
			get { return this.adres; }
		}
		private AdresOrganizacjiPolska adres;

		public CzescOrganizacji CzescOrganizacji
		{
			get { return this.czescOrganizacji; }
			set { this.czescOrganizacji = value; }
		}
		private CzescOrganizacji czescOrganizacji;



		public PelnaOrganizacja()
		{
			//this.WpisDoIzby = new WpisDoRejestruIzby();
			this.REGON = new Regon(OID.ZbiorWartosci.Regon9);
			this.Podmiot = new Podmiot();
			//this.KomorkaOrganizacyjna = new KomorkaOrganizacyjna();
		}

		public PelnaOrganizacja(KomorkaPodmiotuWykonujacegoDzialalnoscLeczniczaBazowy komorkaOrganizacyjna)
		{
			this.KomorkaOrganizacyjna = komorkaOrganizacyjna.Identyfikator;
		}



		public XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajElement(this.WpisDoIzby);
			xmlElement.DodajElement(this.KomorkaOrganizacyjna);
			xmlElement.DodajElement(this.Podmiot);
			xmlElement.DodajElement(this.REGON);
			xmlElement.DodajElement(() => this.Nazwa);
			xmlElement.DodajElement(this.Kontakt);
			xmlElement.DodajElement(this.Adres);

			xmlElement.DodajElement(this.CzescOrganizacji);

			if (!xmlElement.HasChildNodes) xmlElement = null;

			return xmlElement;
		}
	}
}
