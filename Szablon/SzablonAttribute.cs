﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.Common;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// Klasa atrybutu dla szablonu 
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public class SzablonAttribute : Attribute
	{
		public string NazwaWyswietlana { get; private set; }
		public string Id { get; private set; }
		public string Nazwa { get; private set; }
		public DateTime ObowiazujeOd { get; private set; }
		public string Wersja { get; private set; }
		public string XmlElementNazwa { get; private set; }
		public string NamespacePrefix { get; private set; }
		public string KodKlasy { get; private set; }
		public HL7CDA.Szablon.WersjaSzablonu WersjaSzablonu { get; private set; }



		internal SzablonAttribute(string nazwaWyswietlana, string id, string nazwa, HL7CDA.Szablon.WersjaSzablonu wersjaSzablonu, string xmlElementNazwa, string namespacePrefix)
		{
			this.NazwaWyswietlana = nazwaWyswietlana;
			this.Id = id;
			this.Nazwa = nazwa;


			var daneWersjiSzablonuAttribute = wersjaSzablonu.GetDaneWersjiSzablonuAttribute();
			if (daneWersjiSzablonuAttribute != null)
			{
				this.ObowiazujeOd = daneWersjiSzablonuAttribute.ObowiazujeOd;
				this.Wersja = daneWersjiSzablonuAttribute.Etykieta;
			}

			this.XmlElementNazwa = xmlElementNazwa;
			this.NamespacePrefix = namespacePrefix;
		}

		internal SzablonAttribute(string nazwaWyswietlana, string id, string nazwa, HL7CDA.Szablon.WersjaSzablonu wersjaSzablonu, string xmlElementNazwa)
			: this(nazwaWyswietlana, id, nazwa, wersjaSzablonu, xmlElementNazwa, null) { }




		//internal SzablonAttribute(string nazwaWyswietlana, string id, string nazwa, string obowiazujeOd, string wersja, string xmlElementNazwa)
		//{
		//  this.NazwaWyswietlana = nazwaWyswietlana;
		//  this.Id = id;
		//  this.Nazwa = nazwa;

		//  DateTime tmp;

		//  if (!DateTime.TryParse(obowiazujeOd, out tmp))
		//  {
		//    throw new ErrorException("Nie można sparsować podanej daty początku obowiązywania szablonu!");
		//  }

		//  this.ObowiazujeOd = tmp;

		//  this.Wersja = wersja;
		//  this.XmlElementNazwa = xmlElementNazwa;
		//}
	}
}
