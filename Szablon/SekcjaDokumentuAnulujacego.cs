﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.3.27-2018-06-30T000000.html 
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.27
	/// </summary>
	[Szablon("[3] Sekcja dokumentu anulującego", "2.16.840.1.113883.3.4424.13.10.3.27", "plCdaNullificationSection", WersjaSzablonu.v1_3, "section")]
	public class SekcjaDokumentuAnulujacego : SzablonSkladowy
	{
		internal DateTime? DokumentDoAnulowaniaWystawiono { get; set; }
		internal string DokumentDoAnulowaniaIdentyfikator { get; set; }
		internal string DokumentDoAnulowaniaTytul { get; set; }



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement("title", "Dane dokumentu anulowanego");

			var xmlText = xmlElement.DodajElement("text");

			xmlText.DodajElement("paragraph", "Proszę o anulowanie dokumentu:");//.DodajSekcjeCData(" ");

			var xmlTytul = xmlText.DodajElement("paragraph");
			//xmlTytul.DodajElement("caption", "Tytuł:").DodajSekcjeCData(" "); 
			//xmlTytul.DodajElement("content", this.DokumentDoAnulowaniaTytul);
			xmlTytul.DodajElement("caption", string.Format("Tytuł: {0}", this.DokumentDoAnulowaniaTytul)).DodajSekcjeCData(" "); 

			if (this.DokumentDoAnulowaniaWystawiono.HasValue)
			{
				var xmlWystawiono = xmlText.DodajElement("paragraph");
				//xmlWystawiono.DodajElement("caption", "Data wystawienia:").DodajSekcjeCData(" ");
				//xmlWystawiono.DodajElement("content", this.DokumentDoAnulowaniaWystawiono.Value.ToString("dd.MM.yyyy"));
				xmlWystawiono.DodajElement("caption", string.Format("Data wystawienia: {0}", this.DokumentDoAnulowaniaWystawiono.Value.ToString("dd.MM.yyyy"))).DodajSekcjeCData(" ");
			}
			if (!string.IsNullOrEmpty(this.DokumentDoAnulowaniaIdentyfikator))
			{
				var xmlId = xmlText.DodajElement("paragraph");
				//xmlId.DodajElement("caption", "Identyfikator:").DodajSekcjeCData(" ");
				//xmlId.DodajElement("content", );
				xmlId.DodajElement("caption", string.Format("Identyfikator: {0}", this.DokumentDoAnulowaniaIdentyfikator)).DodajSekcjeCData(" ");
			}

			return xmlElement;
		}
	}
}
