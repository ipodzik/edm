﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Szablon;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[7] Opis wyrażenia klinicznego", "2.16.840.1.113883.3.4424.13.10.7.3", "plCdaClinicalStatementText", WersjaSzablonu.v1_3, "text")]
	public class OpisWyrazeniaKlinicznego : SzablonSkladowy
	{
		//[HL7XmlName("value")]
		//internal string Referencja { get; private set; }
		private readonly string referencja;



		///// <summary>
		///// Tworzy obiekt z podaną referencją
		///// </summary>
		///// <param name="IdentyfikatorXml">identyfikator XML z wyliczenia</param>
		//public OpisWyrazeniaKlinicznego(IdentyfikatorXml IdentyfikatorXml) 
		//{
		//  this.DodajReferencje(IdentyfikatorXml.GetKod());
		//}

		/// <summary>
		/// Tworzy obiekt z podaną referencją
		/// </summary>
		/// <param name="id">sam identyfikator bez znaku '#'</param>
		public OpisWyrazeniaKlinicznego(string id)
		{
			if (!string.IsNullOrEmpty(id))
			{
				this.referencja = "#" + id;
			}
			else
			{
				this.referencja = null;
			}
		}



		//internal void DodajReferencje(string id)
		//{
		//  if (!string.IsNullOrEmpty(id))
		//  {
		//    this.referencja = "#" + id;
		//  }
		//  else
		//  {
		//    this.referencja = null;
		//  }
		//}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<OpisWyrazeniaKlinicznego>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.DodajElement("reference").UstawAtrybut("value", this.referencja);

			return xmlElement;
		}
	}
}
