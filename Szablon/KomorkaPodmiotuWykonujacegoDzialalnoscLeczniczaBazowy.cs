﻿using System.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[2] Komórka podmiotu wykonującego działalność leczniczą (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.18", "plCdaBaseOrganizationalCell", WersjaSzablonu.v1_3, "representedOrganization")]
	public class KomorkaPodmiotuWykonujacegoDzialalnoscLeczniczaBazowy : SzablonSkladowy
	{
		public int? MedInfId { get; private set; }

		public OID.KomorkaOrganizacyjna Identyfikator { get; private set; }
		[HL7XmlName("name")]
		public string Nazwa { get; set; }
		public Kontakt Kontakt { get; private set; }
		public AdresOrganizacjiPolska Adres { get; private set; }

		public SpecjalnoscKomorkiOrganizacyjnej Specjalnosc { get; private set; }


		public CzescOrganizacji CzescOrganizacji { get; private set; }


		public KomorkaPodmiotuWykonujacegoDzialalnoscLeczniczaBazowy()
		{
			this.Identyfikator = new OID.KomorkaOrganizacyjna();
			this.Kontakt = new Kontakt();
			this.Adres = new AdresOrganizacjiPolska();
			this.Specjalnosc = new SpecjalnoscKomorkiOrganizacyjnej();
			this.CzescOrganizacji = new CzescOrganizacji(this);
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement(this.Identyfikator);

			xmlElement.DodajElement(() => this.Nazwa);
			xmlElement.DodajElement(this.Kontakt);
			xmlElement.DodajElement(this.Adres);

			xmlElement.DodajElement(this.Specjalnosc);

			xmlElement.DodajElement(this.CzescOrganizacji);

			return xmlElement;
		}

		public void PobierzDane(int medInfId)
		{
			MedInf.KomorkaOrganizacyjna komOrg = MedInf.KomorkaOrganizacyjna.Pobierz(medInfId);
			if (komOrg != null)
			{
				this.MedInfId = medInfId;
				this.Nazwa = komOrg.Nazwa;
				this.Identyfikator.KodResortowy1 = komOrg.KodResortowy1;
				this.Identyfikator.KodResortowy7 = komOrg.KodResortowy7;
				this.Specjalnosc.Nazwa = this.Nazwa;
				this.Specjalnosc.KodResortowy8 = komOrg.KodResortowy8;
			}
		}
	}
}
