﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.53
	/// </summary>
	[Szablon("[2] Dane zlecenia (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.53", "plCdaBaseInFulfillmentOf", WersjaSzablonu.v1_3, "inFulfillmentOf")]
	public class DaneZleceniaBazowy : SzablonSkladowy
	{
		//public override XmlElement TworzXmlElement(XmlElement parent)
		//{
		//   var xmlElement = parent.TworzElementXml(this);
		//   xmlElement.DodajIdentyfikatorSzablonu(this);		
		//}
			
	}
}
