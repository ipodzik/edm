﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.60-2018-06-30T000000.html
	*/
	[Szablon("[4] Publiczne ubezpieczenie zdrowotne", "2.16.840.1.113883.3.4424.13.10.4.60", "plCdaPublicPolicyEntry", WersjaSzablonu.v1_3, "act")]
	public class PubliczneUbezpieczenieZdrowotne : SzablonSkladowy
	{
		private readonly SekcjaZleceniaLeku zlecenieLeku;

		internal PodmiotFinansujacyZeSrodkowPublicznych Platnik
		{
			get { return this.platnik; }
			set { this.platnik = value; }
		}
		private PodmiotFinansujacyZeSrodkowPublicznych platnik;

		internal DaneAutoryzacyjneZwiazaneZUbezpieczeniem Ubezpieczenie
		{
			get { return this.ubezpieczenie; }
			private set { this.ubezpieczenie = value; }
		}
		private DaneAutoryzacyjneZwiazaneZUbezpieczeniem ubezpieczenie;

		internal DaneAutoryzacyjneZwiazanezRefundacjaLekow Refundacja
		{
			get { return this.refundacja; }
			set { this.refundacja = value; }
		}
		private DaneAutoryzacyjneZwiazanezRefundacjaLekow refundacja;



		public PubliczneUbezpieczenieZdrowotne(SekcjaZleceniaLeku zlecenieLeku)
		{
			this.Platnik = new PodmiotFinansujacyZeSrodkowPublicznych();
			this.zlecenieLeku = zlecenieLeku;
			this.Ubezpieczenie = new DaneAutoryzacyjneZwiazaneZUbezpieczeniem(zlecenieLeku);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<PubliczneUbezpieczenieZdrowotne>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.UstawAtrybut("classCode", "ACT").UstawAtrybut("moodCode", "EVN");

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.26");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement(new IdNullFlavor("NAV"));

			xmlElement.DodajElement("code").UstawAtrybut("code", "PUBLICPOL").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.4");

			xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.LekOdplatnosc));
			xmlElement.DodajElement(new StatusCodeCompleted());

			xmlElement.DodajElement(new PerformerAssignedEntity(this.Platnik));

			xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "REFR").DodajElement(this.Ubezpieczenie);
			
			//if (this.zlecenieLeku.Lek.Odplatnosc != Slownik.PoziomOdplatnosciZaLeki.Pelnoplatne)
			//{
			//  this.Refundacja = new DaneAutoryzacyjneZwiazanezRefundacjaLekow();
			//  this.Refundacja.PozacjaRecepty.Extension = this.zlecenieLeku.PozycjaRecepty.Identyfikator.Extension;
			//  xmlElement.DodajElement("entryRelationship").DodajAtrybut("typeCode", "REFR").DodajElement(this.Refundacja);
			//}
			
			return xmlElement;
		}
	}
}
