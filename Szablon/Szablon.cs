﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary>
	/// Abstrakcyjna klasa szablonu HL7
	/// </summary>
	/// <remarks>
	/// Szablon HL7 (ang. template) jest zarejestrowanym (= posiadającym unikalny identyfikator) zestawem reguł nałożonych na określony model danych.
	/// Szablon HL7 reguluje zarówno strukturę modelu, na który jest nałożony, jak i zawartość instancji tego modelu, a więc fragmentu dokumentu XML w przypadku HL7 CDA
	/// </remarks>

	public abstract class Szablon
	{
		//informacje przeniesione do atrybutu:
		//public static string SzablonNazwaWyswietlana { get; protected set; }
		//public static string SzablonId { get; protected set; }
		//public static string SzablonNazwa { get; protected set; }
		//public static DateTime SzablonObowiazujeOd { get; protected set; }
		//public static string SzablonWersja { get; protected set; }
		//public static string SzablonRootXmlElementNazwa { get; protected set; }
	}

	/// <summary>
	/// Abstrakcyjna klasa szablonu dokumentu
	/// </summary>
	public abstract class SzablonDokumentu : Szablon
	{
		/// <summary>
		/// Informacje o kontekście wywołania usługi 
		/// </summary>
		public DaneKontekstuWywolania DaneKontekstuWywolania { get { return this.daneKontekstuWywolania; } }
		protected DaneKontekstuWywolania daneKontekstuWywolania; 

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <returns></returns>
		public virtual XmlDocument TworzXml()
		{
			this.daneKontekstuWywolania = new DaneKontekstuWywolania(null, null, null); //! pamiętaj by w tej metodzie utworzyć obiekt KontekstWywolania (najlepiej na samym końcu)! 

			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// Abstrakcyjna klasa szablonu składowego dla dokumentu
	/// </summary>
	public abstract class SzablonSkladowy : Szablon, IElementSzablonu
	{
		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <returns></returns>
		public virtual XmlElement TworzXmlElement(XmlElement parent)
		{
			throw new NotImplementedException();
			//return null;
		}
	}
}
