﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[2] Organizacja (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.2", "plCdaBaseOrganization", WersjaSzablonu.v1_3, "providerOrganization")]
	public class OrganizacjaBazowy : SzablonSkladowy
	{
		//public IdentyfikatorObiektu NumerKsiegiRejestrowej { get; private set; }
		public NumerKsiegiRejestrowej NumerKsiegiRejestrowej { get; private set; }



		internal OrganizacjaBazowy()
		{
			this.NumerKsiegiRejestrowej = new NumerKsiegiRejestrowej();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.SetAttribute("classCode", "ORG");

			xmlElement.DodajIdentyfikatorSzablonu(this);
			//xmlElement.DodajTypII(this.NumerKsiegiRejestrowej);
			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement).SetExtension(this.NumerKsiegiRejestrowej.Extension).SetRoot(ZbiorWartosci.PodmiotKodResortowyI.GetKod()).SetDisplayable(true).Build());

			return xmlElement;
		}
	}
}
