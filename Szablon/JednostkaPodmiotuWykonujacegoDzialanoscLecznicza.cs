﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using System.Data;
using System.Data.Common;
using BIUINF.Lib4.Data;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[2] Jednostka podmiotu wykonującego działalność leczniczą (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.17", "plCdaBaseOrganizationalUnit", WersjaSzablonu.v1_3, "representedOrganization")]
	public class JednostkaPodmiotuWykonujacegoDzialanoscLecznicza : SzablonSkladowy
	{
		public int? MedInfId { get; private set; }

		public JednostkaOrganizacyjna Identyfikator { get; private set; }
		[HL7XmlName("name")]
		public string Nazwa { get; set; }
		public Kontakt Kontakt { get; private set; }
		public AdresOrganizacjiPolska Adres { get; private set; }

		public CzescOrganizacji CzescOrganizacji { get; private set; }

		internal KomorkaOrganizacyjna KomorkaOrganizacyjna { get; private set; } //jako kontener danych pobranych z bazy 
		internal Podmiot Podmiot { get; private set; } //jako kontener danych pobranych z bazy 


		public JednostkaPodmiotuWykonujacegoDzialanoscLecznicza()
		{
			this.Identyfikator = new JednostkaOrganizacyjna();
			this.Kontakt = new Kontakt();
			this.Adres = new AdresOrganizacjiPolska();
			this.CzescOrganizacji = new CzescOrganizacji();
			this.CzescOrganizacji.PelnaOrganizacja.CzescOrganizacji = new CzescOrganizacji();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement(this.Identyfikator);

			xmlElement.DodajElement(() => this.Nazwa);
			xmlElement.DodajElement(this.Kontakt);
			xmlElement.DodajElement(this.Adres);

			xmlElement.DodajElement(this.CzescOrganizacji);


			return xmlElement;
		}

		public void PobierzDane(int medInfId)
		{
			MedInf.KomorkaOrganizacyjna komOrg = MedInf.KomorkaOrganizacyjna.Pobierz(medInfId);
			if (komOrg != null)
			{
				this.MedInfId = medInfId;
				this.Nazwa = komOrg.Nazwa;
				this.Identyfikator.KodResortowy1 = komOrg.KodResortowy1;
				this.Identyfikator.KodResortowy5 = komOrg.KodResortowy5;
				if (!string.IsNullOrEmpty(komOrg.REGON14)) this.CzescOrganizacji.PelnaOrganizacja.REGON.UstawWartosc(ZbiorWartosci.Regon14, komOrg.REGON14);

				this.KomorkaOrganizacyjna = new KomorkaOrganizacyjna();
				this.KomorkaOrganizacyjna.KodResortowy1 = komOrg.KodResortowy1;
				this.KomorkaOrganizacyjna.KodResortowy7 = komOrg.KodResortowy7;

				this.Podmiot = new Podmiot();
				this.Podmiot.Extension = komOrg.KodResortowy1;

				this.Adres.Ulica = komOrg.Adres.Ulica;
				this.Adres.NrDomu = komOrg.Adres.NrDomu;
				this.Adres.NrLokalu = komOrg.Adres.NrLokalu;
				this.Adres.KodPocztowy = komOrg.Adres.PocztaKod;
				this.Adres.Miasto = komOrg.Adres.Miejscowosc;
				if (!string.IsNullOrEmpty(komOrg.Adres.NrTelefonu)) this.Kontakt.UstawWartosc(komOrg.Adres.NrTelefonu);
				this.CzescOrganizacji.PelnaOrganizacja.CzescOrganizacji.PelnaOrganizacja.Podmiot.Extension = this.Podmiot.Extension;
				this.CzescOrganizacji.PelnaOrganizacja.CzescOrganizacji.PelnaOrganizacja.REGON.UstawWartosc(ZbiorWartosci.Regon9, komOrg.REGON9);
			}
		}
	}
}
