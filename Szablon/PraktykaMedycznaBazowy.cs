﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[Szablon("[2] Praktyka medyczna (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.15", "plCdaBaseMedicalPractice", WersjaSzablonu.v1_3, "representedOrganization")]
	public class PraktykaMedycznaBazowy : SzablonSkladowy
	{
		public WpisDoRejestrIzbMiejsceWykonywania WpisDoRejestrIzbMiejsceWykonywania { get; private set; }
		[HL7XmlName("name")]
		public string Nazwa { get; set; }
		public Kontakt Kontakt { get; private set; } //! jest 0...* , póki co przyjmuję że może być 0...1
		public AdresOrganizacjiPolska Adres { get; private set; }
		public SpecjalnoscKomorkiOrganizacyjnej SpecjalnoscKomorki { get; private set; }

		//+ część organizacji
		public CzescOrganizacji CzescOrganizacji { get; private set; }
		//public PraktykaZawodowa PraktykaZawodowa { get; private set; }
		//public Kontakt Kontakt { get; private set; } 



		internal PraktykaMedycznaBazowy()
		{
			this.WpisDoRejestrIzbMiejsceWykonywania = new WpisDoRejestrIzbMiejsceWykonywania();
			this.Kontakt = new Kontakt();
			this.Adres = new AdresOrganizacjiPolska();
			this.SpecjalnoscKomorki = new SpecjalnoscKomorkiOrganizacyjnej();

			this.CzescOrganizacji = new CzescOrganizacji();
			//this.PraktykaZawodowa = new PraktykaZawodowa();
			//this.Kontakt = new Kontakt();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajTypII(this.WpisDoRejestrIzbMiejsceWykonywania);

			xmlElement.DodajElement(() => this.Nazwa);
			xmlElement.DodajElement(this.Kontakt);
			xmlElement.DodajSzablon(this.Adres);
			xmlElement.DodajElement(this.SpecjalnoscKomorki);

			xmlElement.DodajElement(this.CzescOrganizacji);

			return xmlElement;
		}
	}
}
