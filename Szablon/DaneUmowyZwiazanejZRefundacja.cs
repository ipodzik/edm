﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.Common;
using BIUINF.Lib4.HL7CDA.Xml;
using BIUINF.Lib4.HL7CDA.MedInf;



namespace BIUINF.Lib4.HL7CDA.Szablon.Recepta
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.51-2018-06-30T000000.html 
	*/
	/// <summary>
	/// <c>2.16.840.1.113883.3.4424.13.10.2.44</c>
	/// </summary>
	[Szablon("[2] Dane umowy związanej z refundacją", "2.16.840.1.113883.3.4424.13.10.2.44", "plExtReimbursementRelatedContract", WersjaSzablonu.v1_3, "boundedBy", "extPL")]
	public class DaneUmowyZwiazanejZRefundacja : SzablonSkladowy
	{
		public UmowaZNfz UmowaZNfz { get; private set; }

		public PodmiotFinansujacyZeSrodkowPublicznych Platnik { get; private set; }



		public DaneUmowyZwiazanejZRefundacja()
		{
			var namespacePrefix = this.GetAttribute<SzablonAttribute>().NamespacePrefix;
			this.UmowaZNfz = new UmowaZNfz(namespacePrefix);
			this.Platnik = new PodmiotFinansujacyZeSrodkowPublicznych(namespacePrefix);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.UstawAtrybut("typeCode", "PART");

			var namespacePrefix = this.GetAttribute<SzablonAttribute>().NamespacePrefix;
			xmlElement.DodajIdentyfikatorSzablonu(this, namespacePrefix);

			var xmlReimbursementRelatedContract = xmlElement.DodajElement("reimbursementRelatedContract", null, namespacePrefix)
				.UstawAtrybut("classCode", "CNTRCT").UstawAtrybut("moodCode", "EVN");

			if (!string.IsNullOrEmpty(this.UmowaZNfz.Numer))
			{
				xmlReimbursementRelatedContract.DodajElement(this.UmowaZNfz);
			}
			else
			{
				//xmlReimbursementRelatedContract.AppendChild(new InstanceIdentifierBuilder(parent, "id", "extPL").SetNullFlavor("NA").Build());
				xmlReimbursementRelatedContract.DodajElement("id", null, "extPL").UstawAtrybut("nullFlavor", "NA");
			}
			xmlReimbursementRelatedContract.DodajElement("bounding", null, namespacePrefix).UstawAtrybut("typeCode", "PART").DodajElement("reimburser", null, namespacePrefix).UstawAtrybut("classCode", "UNDWRT")
				.DodajElement(this.Platnik);

			return xmlElement;
		}

		public void PobierzDane(int komorkaOrganizacyjnaId)
		{
			//var un = MedInf.UmowaNfz.Pobierz(komorkaOrganizacyjnaId);
			//if (un != null)
			//{
			//  // możliwe braki numeru umowy, kodu płatnika (symbol kontrahenta)
			//  this.UmowaZNfz.Numer = un.Nr;
			//  if (string.IsNullOrEmpty(un.PlatnikKod))
			//  {
			//    throw KontrolaDanychMedInfExclamationException.Create("Brak kodu płatnika w danych kontrahenta (oddział NFZ)!");
			//  }
			//  this.Platnik.Kod = un.PlatnikKod;
			//}
			//else
			//{
			//  string kod = string.Empty;
			//  Library.AppCore.Parameters.FindValue("MedInf", "RozliczajacyOddzialNFZ", out kod);
			//  if (string.IsNullOrEmpty(kod))
			//  {
			//    throw KontrolaDanychMedInfExclamationException.Create("Brak kodu płatnika w parametrach systemu (moduł 'MedInf')!");
			//  }
			//  this.Platnik.Kod = kod;
			//}

			string kod = string.Empty;
			Library.AppCore.Parameters.FindValue("MedInf", "RozliczajacyOddzialNFZ", out kod);
			if (string.IsNullOrEmpty(kod))
			{
				throw BrakDanychMedInfExclamationException.Create("Brak kodu płatnika w parametrach systemu (moduł: 'MedInf', parametr: 'RozliczajacyOddzialNFZ')!");
			}
			this.Platnik.Kod = kod;


			var ownfz = ZbiorWartosciHelper.GetValues(NadzbiorZbioruWartosci.NumeryUmowZNfzSwiadczeniodawcow)
				.Where(x => x.GetKod().EndsWith("." + this.Platnik.Kod.Replace("0", string.Empty))).ToArray();
			if (ownfz.Length == 1)
			{
				this.UmowaZNfz.OddzialWojewodzki = ownfz.SingleOrDefault();
			}
			else
			{
				throw BrakDanychMedInfExclamationException.Create(string.Format("Brak możliwości określenia płatnika dla kodu: '{0}'!", this.Platnik.Kod));
			}
		}
	}
}
