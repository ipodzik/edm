﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;
using BIUINF.Lib4.HL7CDA.OID;


namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.2.19-2018-06-30T000000.html 
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.19
	/// </summary>
	[Szablon("[2] Dane ubezpieczyciela publicznego (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.19", "plCdaBasePublicUnderwriterParticipant", WersjaSzablonu.v1_3, "participant")]
	public class DaneUbezpieczycielaPublicznegoBazowy : SzablonSkladowy
	{
		internal PodmiotFinansujacyZeSrodkowPublicznych Platnik { get; private set; }



		public DaneUbezpieczycielaPublicznegoBazowy()
		{
			this.Platnik = new PodmiotFinansujacyZeSrodkowPublicznych();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this).UstawAtrybut("typeCode", "IND");

			xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot(this).Build());

			//var xmlAssociatedEntity = 
			xmlElement.DodajElement("associatedEntity").UstawAtrybut("classCode", "UNDWRT").DodajElement(this.Platnik);

			return xmlElement;
		}
	}
}
