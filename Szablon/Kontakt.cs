﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/// <summary />
	public class Kontakt : IElementSzablonu
	{
		public RodzajKontaktu RodzajKontaktu { get; private set; }
		public string Wartosc { get { return this.wartosc; } }
		private string wartosc;


		/// <summary>
		/// Ustawia dane kontaktowe 
		/// </summary>
		/// <param name="rodzajKontaktu">rodzaj kontaktu</param>
		/// <param name="protokol">protokół kontaktu</param>
		/// <param name="wartosc">wartosc</param>
		public void UstalWartosc(RodzajKontaktu rodzajKontaktu, ProtokolKontaktu protokol, string wartosc)
		{
			this.RodzajKontaktu = rodzajKontaktu;
			this.wartosc = string.Format("{0}:{1}", protokol.GetKod(), wartosc);
		}

		/// <summary>
		/// Ustawia dane kontaktowe dla recepcji 
		/// </summary>
		/// <param name="protokol">protokół kontaktu</param>
		/// <param name="wartosc">wartosc</param>
		public void UstalWartosc(ProtokolKontaktu protokol, string wartosc)
		{
			this.UstalWartosc(RodzajKontaktu.Recepcja, protokol, wartosc);
		}

		/// <summary>
		/// Ustawia numer telefonu dla recepcji 
		/// </summary>
		/// <param name="wartosc">wartosc</param>
		public void UstawWartosc(string wartosc)
		{
			this.UstalWartosc(RodzajKontaktu.Recepcja, ProtokolKontaktu.Tel, wartosc);
		}



		public XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = null;

			if (!string.IsNullOrEmpty(wartosc))
			{
				xmlElement = parent.TworzElementXml("telecom");

				xmlElement.UstawAtrybut("use", this.RodzajKontaktu.GetKod());
				xmlElement.UstawAtrybut("value", this.wartosc);
			}

			return xmlElement;
		}
	}




	/// <summary />
	public enum RodzajKontaktu
	{
		Inny,
		[Enum("H", "domowy")]
		Domowy,
		[Enum("HP", "domowy")]
		Domowy1,
		[Enum("WP", "służbowy")]
		Sluzbowy,
		[Enum("DIR", "służbowy bezpośredni")]
		SluzbowyBezposredni,
		[Enum("PUB", "recepcja")]
		Recepcja,
		[Enum("TMP", "tymczasowy")]
		Tymczasowy,
		[Enum("EC", "w nagłych przypadkach")]
		NalgePrzypadki,
		[Enum("MC", "komórkowy")]
		Komorkowy
	}




	/// <summary />
	public enum ProtokolKontaktu
	{
		Inny,
		[Enum("faks", "Numer faksu")]
		Faks,
		[Enum("Internet", "Adres strony www")]
		Http,
		[Enum("e-mail", "Adres e-mail")]
		Email,
		[Enum("tel", "Numer telefonu")]
		Tel
	}
}
