﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.51-2018-06-30T000000.html 
	*/
	/// <summary>
	/// <c>2.16.840.1.113883.3.4424.13.10.4.51</c>
	/// Dane o uprawnieniach i płatnikach 
	/// </summary>
	[Szablon("[4] Dane o uprawnieniach i płatnikach", "2.16.840.1.113883.3.4424.13.10.4.51", "plCdaCoverageActivityEntry", WersjaSzablonu.v1_3, "act")]
	public class DaneOUprawnieniachIPlatnikach : SzablonSkladowy
	{
		private readonly SekcjaZleceniaLeku zlecenieLeku;

		internal PodmiotFinansujacyZeSrodkowPublicznych Platnik
		{
			get { return this.platnik; }
		}
		private readonly PodmiotFinansujacyZeSrodkowPublicznych platnik;

		internal PubliczneUbezpieczenieZdrowotne Ubezpieczenie
		{
			get { return this.ubezpieczenie; }
			set { this.ubezpieczenie = value; }
		}
		private PubliczneUbezpieczenieZdrowotne ubezpieczenie;

		internal UprawnienieDodatkowePublicznegoUbezpieczeniaZdrowotnego UprawnienieDodatkowe
		{
			get { return this.uprawnienieDodatkowe; }
		}
		private UprawnienieDodatkowePublicznegoUbezpieczeniaZdrowotnego uprawnienieDodatkowe;



		public DaneOUprawnieniachIPlatnikach(SekcjaZleceniaLeku zlecenieLeku)
		{
			this.zlecenieLeku = zlecenieLeku;
			this.platnik = new PodmiotFinansujacyZeSrodkowPublicznych();
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<DaneOUprawnieniachIPlatnikach>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			xmlElement.UstawAtrybut("classCode", "ACT");
			xmlElement.UstawAtrybut("moodCode", "DEF");

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.20");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement(new IdNullFlavor("NA"));
			xmlElement.DodajTypCD("48768-6", "2.16.840.1.113883.6.1", null, "Payment source");

			xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(IdentyfikatorElementuXml.LekPlatnik));
			xmlElement.DodajTypCS();

			if (!this.zlecenieLeku.Lek.UprawnienieDodatkowe.HasValue)
			{
				this.Ubezpieczenie = new PubliczneUbezpieczenieZdrowotne(zlecenieLeku);
				this.ubezpieczenie.Platnik = this.platnik;
				xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP").DodajElement(this.Ubezpieczenie);
			}
			else
			{
				this.uprawnienieDodatkowe = new UprawnienieDodatkowePublicznegoUbezpieczeniaZdrowotnego(this.zlecenieLeku);
				this.uprawnienieDodatkowe.Platnik = this.platnik;
				xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "COMP").DodajElement(this.uprawnienieDodatkowe);
			}

			return xmlElement;
		}
	}
}
