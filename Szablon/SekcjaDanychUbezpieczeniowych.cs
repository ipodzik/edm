﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.3.69-2018-06-30T000000.html 
	*/
	[Szablon("[3] Sekcja zalecenia leku", "2.16.840.1.113883.3.4424.13.10.3.69", "plCdaPayersSection", WersjaSzablonu.v1_3, "section")]
	public class SekcjaDanychUbezpieczeniowych : SzablonSkladowy
	{
		private readonly SekcjaZleceniaLeku zlecenieLeku;

		public string PlatnikNazwa { get; set; }
		public string PlatnikKod { get; set; }

		internal DaneOUprawnieniachIPlatnikach Dane
		{
			get { return this.dane; }
		}
		private readonly DaneOUprawnieniachIPlatnikach dane;



		public SekcjaDanychUbezpieczeniowych(SekcjaZleceniaLeku zlecenieLeku)
		{
			this.zlecenieLeku = zlecenieLeku;
			this.dane = new DaneOUprawnieniachIPlatnikach(this.zlecenieLeku);
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<SekcjaDanychUbezpieczeniowych>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.9");
			xmlElement.DodajIdentyfikatorSzablonu("1.3.6.1.4.1.19376.1.5.3.1.1.5.3.7");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajTypCD("48768-6", "2.16.840.1.113883.6.1");
			xmlElement.DodajElement("title", "Dane o ubezpieczeniu i uprawnieniach");

			this.Dane.Platnik.Kod = this.PlatnikKod;

			var xmlText = xmlElement.DodajElement("text");

			var xmlPlatnik = xmlText.DodajElement("paragraph").DodajIdentyfikatorElementu(IdentyfikatorElementuXml.LekPlatnik);
			xmlPlatnik.DodajElement("content", this.PlatnikNazwa).DodajIdentyfikatorElementu("p1_platnik_opis_1");
			xmlPlatnik.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
			xmlPlatnik.DodajElement("content", this.PlatnikKod).DodajIdentyfikatorElementu("p1_platnik_wartosc_1");
			xmlPlatnik.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1

			if (this.zlecenieLeku.Lek.UprawnienieDodatkowe.HasValue)
			{
				var xmlUD = xmlText.DodajElement("paragraph");
				xmlUD.DodajElement("content", "Uprawnienia dodatkowe").DodajIdentyfikatorElementu("p1_uprawnieniaDodatkowe_opis_1");
				xmlUD.DodajElement("content").DodajSekcjeCData(" ");		// separator między wyrazami wg uwag CSIOZ pkt 4. wyjaśnienia do W8.1
				xmlUD.DodajElement("content", this.zlecenieLeku.Lek.UprawnienieDodatkowe.Value.GetKod()).DodajIdentyfikatorElementu("p1_uprawnieniaDodatkowe_wartosc_1");
				//xmlPlatnik.DodajElement("content", ).DodajIdentyfikatorElementu("p1_dokumentUprawnien_wartosc_1");
			}
			
			xmlElement.DodajElement("entry").DodajElement(this.Dane);

			return xmlElement;
		}
	}
}
