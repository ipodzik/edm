﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				 https://www.csioz.gov.pl/HL7POL-1.3.1/plcda-html-1.3.1/plcda-html-1.3.1/tmp-2.16.840.1.113883.3.4424.13.10.4.53-2018-06-30T000000.html
	*/
	[Szablon("[4] Dane autoryzacyjne związane z ubezpieczeniem", "2.16.840.1.113883.3.4424.13.10.4.53", "plCdaAuthorizationActivityEntry", WersjaSzablonu.v1_3, "act")]
	public class DaneAutoryzacyjneZwiazaneZUbezpieczeniem : SzablonSkladowy
	{
		private readonly SekcjaZleceniaLeku zlecenieLeku;
		internal IdentyfikatorInstancji Identyfikator { get; private set; }



		public DaneAutoryzacyjneZwiazaneZUbezpieczeniem()
		{
			this.Identyfikator = new IdentyfikatorInstancji(ZbiorWartosci.ReceptaPozycja);
		}

		public DaneAutoryzacyjneZwiazaneZUbezpieczeniem(SekcjaZleceniaLeku zlecenieLeku)
			: this()
		{
			this.zlecenieLeku = zlecenieLeku;
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<DaneAutoryzacyjneZwiazaneZUbezpieczeniem>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);

			xmlElement.UstawAtrybut("classCode", "ACT");
			xmlElement.UstawAtrybut("moodCode", "EVN");

			xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.10.20.1.19");
			xmlElement.DodajIdentyfikatorSzablonu(this);

			xmlElement.DodajElement(new IdNullFlavor("NA"));
			xmlElement.DodajElement("code").UstawAtrybut("nullFlavor", "NA");

			var xmlSA = xmlElement.DodajElement("entryRelationship").UstawAtrybut("typeCode", "SUBJ")
				.DodajElement("substanceAdministration").UstawAtrybut("classCode", "SBADM").UstawAtrybut("moodCode", "PRMS");

			if (this.zlecenieLeku.PozycjaRecepty != null)
			{
				this.Identyfikator.Extension = this.zlecenieLeku.PozycjaRecepty.Identyfikator.Extension;
			}
			xmlSA.DodajElement(this.Identyfikator);
			xmlSA.DodajElement("consumable").DodajElement("manufacturedProduct").DodajElement("manufacturedMaterial").UstawAtrybut("nullFlavor", "NA");

			return xmlElement;
		}
	}
}
