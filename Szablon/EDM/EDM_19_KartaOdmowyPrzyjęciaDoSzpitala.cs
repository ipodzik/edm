﻿//	class EDM_19_KartaOdmowyPrzyjęciaDoSzpitala
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;
using BIUINF.Lib4.HL7CDA.OID;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.1.19
	/// </summary>
	[Szablon("[1] Karta odmowy przyjęcia do szpitala", "2.16.840.1.113883.3.4424.13.10.1.19", "plCdaRefusalNote", WersjaSzablonu.v1_3_1, "ClinicalDocument")]
	public class EDM_19_KartaOdmowyPrzyjęciaDoSzpitala : EDM
	{

		/// <summary>
		/// Data wystawienia dokumentu
		/// </summary>		
		public new DateTime? Wystawiono
		{
			get { return base.Wystawiono; }
			set
			{
				base.Wystawiono = value;
				szablon10_wizyta.Wystawiono = value;
			}
		}

		/// <summary>
		/// Lista wyników wszystkich badań zrealizowanych w ramach jednego zlecenia
		/// </summary>
		public List<Badanie> WynikiZrealizowanychBadań
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaWynikowBadan.WynikiZrealizowanychBadań = value;
			}
		}

		/// <summary>
		/// Opis zastosowanego leczenia
		/// </summary>
		public string ZastosowaneLeczenie
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaZastosowanegoLeczenia.ZastosowaneLeczenie = value;
			}
		}

		public string PowodOdmowyPrzyjeciaDoSzpitala
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaPowoduOdmowyPrzyjeciaDoSzpitala.PowodOdmowyPrzyjeciaDoSzpitala = value;
			}
		}

		public string OpisBadaniaPrzedmiotowego
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaBadaniaPrzedmiotowego.OpisBadaniaPrzedmiotowego = value;
			}
		}

		/// <summary>
		/// Opis udzielonego świadczenia
		/// </summary>
		public string OpisUdzielonegoSwiadczenia
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaWywiaduLekarskiego.TrescSekcji.Add(value);
			}
		}

		/// <summary>
		/// Rozpoznanie główne - wg klasyfikacji ICD10
		/// </summary>
		public Slownik.ICD10 RozpoznanieGlowne
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaRozpoznan.SzablonRozpoznanieGlowne.RozpoznanieICD10 = value;
			}
		}

		/// <summary>
		/// Lista rozpoznań dodatkowych/współistniejących - wg klasyfikacji ICD10
		/// </summary>
		public List<Slownik.ICD10> RozpoznanDodatkowychLista
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaRozpoznan.ListaRozpoznanDodatkowych = value;
			}
		}

		/// <summary>
		/// Zawiera zalecone leki, badania, procedury lub inne zalecenia. Wyświetlane w sekcji zaleceń
		/// </summary>
		public Zalecenie Zalecenie
		{
			set
			{
				this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaZalecen.Zalecenie = value;
			}
		}

		//szablony 1-9 dziedziczone z EDM
		private DaneWizytyWIzbiePrzyjęć szablon10_wizyta;	// 1 … 1		
		private TrescKartyOdmowyPrzyjeciaDoSzpitala szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala; // 1 … 1		

		/// <summary>
		/// Konstruktor
		/// </summary>
		public EDM_19_KartaOdmowyPrzyjęciaDoSzpitala()
		{
			szablon10_wizyta = new DaneWizytyWIzbiePrzyjęć();
			this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala = new TrescKartyOdmowyPrzyjeciaDoSzpitala();
			this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala.SzablonSekcjaWywiaduLekarskiego.NaglowekSekcji = "Opis udzielonego świadczenia";
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <returns></returns>	
		internal override XmlDocument TworzXml()
		{
			try
			{
				//+ utwórz dokument
				this.xmlDocument.AppendChild(this.xmlDocument.CreateXmlDeclaration("1.0", Encoding.UTF8.HeaderName, null));
				this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.xsl\" type=\"text/xsl\""));
				//+ utwórz element główny dokumentu
				SzablonAttribute hl7Szablon = AttributeHelper<EDM_28_Informacja_dla_lekarza>.GetCustomAttribute<SzablonAttribute>();
				XmlElement xmlElement = this.xmlDocument.CreateElement(hl7Szablon.XmlElementNazwa);
				this.xmlDocument.AppendChild(xmlElement);
				//+ ustaw atrybuty elementu głównego
				this.xmlDocument.DocumentElement.SetAttribute("xmlns", "urn:hl7-org:v3");
				this.xmlDocument.DocumentElement.SetAttribute("xmlns:extPL", "http://www.csioz.gov.pl/xsd/extPL/r2");
				this.xmlDocument.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
				this.xmlDocument.DocumentElement.SetAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", "extPL:ClinicalDocument");

				//+ dalsze elementy dokumentu
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "typeId").SetExtension("POCD_HD000040").SetRoot("2.16.840.1.113883.1.3").Build());
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot(hl7Szablon.Id).SetExtension(hl7Szablon.Wersja).Build());
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement).SetExtension(this.IdentyfikatorDokumentu.Instancja).SetRoot(this.IdentyfikatorDokumentu.IdentyfikatorInstancji.Root).SetDisplayable(true).Build());

				var xmlCode = xmlElement.DodajElement("code");
				{
					xmlCode.UstawAtrybut("code", "11488-4");
					xmlCode.UstawAtrybut("codeSystem", "2.16.840.1.113883.6.1");
					xmlCode.UstawAtrybut("codeSystemName", "LOINC");
					xmlCode.UstawAtrybut("displayName", "Consult note");

					var xmlTranslation = xmlCode.DodajElement("translation");
					{
						xmlTranslation.UstawAtrybut("code", "00.65");
						xmlTranslation.UstawAtrybut("codeSystem", "2.16.840.1.113883.3.4424.11.1.32");
						xmlTranslation.UstawAtrybut("codeSystemName", "KLAS_DOK_P1");
						xmlTranslation.UstawAtrybut("displayName", "	Informacja o odmowie przyjęcia");
					}
				}
				xmlElement.DodajElement(xmlElement, "title", "Karta odmowy przyjęcia do szpitala");

				if (this.Wystawiono.HasValue)
					xmlElement.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());

				xmlElement.DodajElement("confidentialityCode").UstawAtrybut("code", "N").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.25");
				xmlElement.DodajElement("languageCode").UstawAtrybut("code", "pl-PL");
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "setId").SetExtension(this.IdentyfikatorDokumentu.ZbiorWersji).SetRoot(this.IdentyfikatorDokumentu.IdentyfikatorZbioruWersji.Root).SetDisplayable(true).Build());
				xmlElement.DodajElement("versionNumber").UstawAtrybut("value", this.IdentyfikatorDokumentu.Wersja.ToString());

				xmlElement.DodajElement(szablon1_pacjent);
				xmlElement.DodajElement(szablon2_autor);
				xmlElement.DodajElement(szablon3_organizacja);
				if (szablon4_odbiorca != null)
					xmlElement.DodajElement(szablon4_odbiorca);
				xmlElement.DodajElement(szablon5_wystawca);
				xmlElement.DodajElement(this.szablon6_ubezpieczyciel); //niewymagane
				if (szablon7_zlecenie != null)
					xmlElement.DodajElement(szablon7_zlecenie);
				if (szablon8_usluga != null)
					xmlElement.DodajElement(szablon8_usluga);
				if (szablon9_powiazanie != null)
					xmlElement.DodajElement(szablon9_powiazanie);
				if (szablon10_wizyta != null)
					xmlElement.DodajElement(szablon10_wizyta);
				if (this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala != null)
					xmlElement.DodajElement(this.szablon11_TrescKartyOdmowyPrzyjeciaDoSzpitala);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXML", ex);
			}

			return this.xmlDocument;

		}

	}
}
