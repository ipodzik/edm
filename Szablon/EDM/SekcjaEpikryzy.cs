﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.37
	/// </summary>
	[Szablon("[3] Sekcja epikryzy", "2.16.840.1.113883.3.4424.13.10.3.37", "plCdaDischargeSummarySection	", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaEpikryzy : SzablonSkladowy
	{
		internal string TrescEpikryzy { get; set; }

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("11493-4").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("Hospital discharge studies summary Narrative").Build());

				xmlElement.DodajElement("title", "Epikryza");

				XmlElement xmlText = xmlElement.DodajElement("text");
				xmlText.DodajElement("paragraph", this.TrescEpikryzy);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
