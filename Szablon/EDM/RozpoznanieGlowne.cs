﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Szablon.Recepta;



namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.1
	/// </summary>
	[Szablon("[4] Rozpoznanie główne", "2.16.840.1.113883.3.4424.13.10.4.1", "plCdaPrincipalDiagnosis", WersjaSzablonu.v1_3_1, "entry")]
	internal class RozpoznanieGlowne : SzablonSkladowy
	{
		/// <summary>
		/// Rozpoznanie wg klasyfikacji ICD-10
		/// </summary>
		public Slownik.ICD10 RozpoznanieICD10 { get; set; }

		/// <summary>
		/// Id referencji do bloku narracyjnego
		/// </summary>
		public string ReferencjaId { get; set; }

		/// <summary>
		/// Konstruktor
		/// </summary>
		public RozpoznanieGlowne()
		{
			this.RozpoznanieICD10 = new Slownik.ICD10();
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				var xmlOrganizer = xmlElement.DodajElement("organizer").UstawAtrybut("classCode", "BATTERY").UstawAtrybut("moodCode", "EVN");

				var xmlStatusCode = xmlOrganizer.DodajElement("statusCode").UstawAtrybut("code", "completed");
				var xmlComponent = xmlOrganizer.DodajElement("component");
				var xmlObservation = xmlComponent.DodajElement("observation").UstawAtrybut("classCode", "OBS").UstawAtrybut("moodCode", "EVN");
				xmlObservation.DodajElement(RozpoznanieICD10);
				xmlObservation.DodajElement(new OpisWyrazeniaKlinicznego(ReferencjaId));
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}
			return xmlElement;
		}
	}
}

