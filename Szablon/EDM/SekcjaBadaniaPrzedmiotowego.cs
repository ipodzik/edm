﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.31
	/// </summary>
	[Szablon("[3] Sekcja badania przedmiotowego", "2.16.840.1.113883.3.4424.13.10.3.31", "plCdaPhysicalFindingsSection", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaBadaniaPrzedmiotowego : SzablonSkladowy
	{
		internal string OpisBadaniaPrzedmiotowego { get; set; }

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("29545-1").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("Physical findings").Build());
				xmlElement.DodajElement("title", "Badanie przedmiotowe");

				XmlElement xmlText = xmlElement.DodajElement("text");

				XmlElement xmlParagraph = xmlText.DodajElement("paragraph", this.OpisBadaniaPrzedmiotowego);

			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}

