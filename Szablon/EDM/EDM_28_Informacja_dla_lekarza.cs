﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;
using BIUINF.Lib4.HL7CDA.OID;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.1.28
	/// </summary>
	[Szablon("[1] Informacja dla lekarza kierującego/POZ", "2.16.840.1.113883.3.4424.13.10.1.28", "plCdaReferringPhysicianLetter", WersjaSzablonu.v1_3_1, "ClinicalDocument")]
	public class EDM_28_Informacja_dla_lekarza : EDM
	{
		/// <summary>
		/// Data wystawienia dokumentu
		/// </summary>
		public new DateTime? Wystawiono
		{
			get { return base.Wystawiono; }
			set
			{
				base.Wystawiono = value;
				szablon10_wizyta.Wystawiono = value;
			}
		}

		public Slownik.ICD10 RozpoznanieGlowne
		{
			set
			{
				this.szablon11_tresc.szablon2_sekcjaRozpoznan.SzablonRozpoznanieGlowne.RozpoznanieICD10 = value;
			}
		}

		public List<Slownik.ICD10> RozpoznaniaDodatkowe
		{
			set
			{
				this.szablon11_tresc.szablon2_sekcjaRozpoznan.ListaRozpoznanDodatkowych = value;
			}
		}

		/// <summary>
		/// Opis udzielonego świadczenia
		/// </summary>
		public string OpisUdzielonegoSwiadczenia
		{
			set
			{
				this.szablon11_tresc.szablon3_sekcjaWywiadu.TrescSekcji.Add(value);
			}
		}

		/// <summary>
		/// Zawiera zalecone leki, badania, procedury lub inne zalecenia. Wyświetlane w sekcji zaleceń
		/// </summary>
		public List<Zalecenie> Zalecenia
		{
			set
			{
				this.szablon11_tresc.Zalecenia = value;
			}
		}

		private DaneWizytyBazowy szablon10_wizyta;		// 0 … 1
		private TrescInformacjiDlaLekarza szablon11_tresc;	// 1 … 1

		/// <summary>
		/// Konstruktor
		/// </summary>
		public EDM_28_Informacja_dla_lekarza()
		{
			this.szablon10_wizyta = new DaneWizytyBazowy();
			this.szablon11_tresc = new TrescInformacjiDlaLekarza();
			this.szablon11_tresc.szablon3_sekcjaWywiadu.NaglowekSekcji = "Opis udzielonego świadczenia";

		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <returns></returns>	
		internal override XmlDocument TworzXml()
		{
			try
			{
				//+ utwórz dokument
				this.xmlDocument.AppendChild(this.xmlDocument.CreateXmlDeclaration("1.0", Encoding.UTF8.HeaderName, null));
				this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.xsl\" type=\"text/xsl\""));
				//+ utwórz element główny dokumentu
				SzablonAttribute hl7Szablon = AttributeHelper<EDM_28_Informacja_dla_lekarza>.GetCustomAttribute<SzablonAttribute>();
				XmlElement xmlElement = this.xmlDocument.CreateElement(hl7Szablon.XmlElementNazwa);
				this.xmlDocument.AppendChild(xmlElement);
				//+ ustaw atrybuty elementu głównego
				this.xmlDocument.DocumentElement.SetAttribute("xmlns", "urn:hl7-org:v3");
				this.xmlDocument.DocumentElement.SetAttribute("xmlns:extPL", "http://www.csioz.gov.pl/xsd/extPL/r2");
				this.xmlDocument.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
				this.xmlDocument.DocumentElement.SetAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", "extPL:ClinicalDocument");

				//+ dalsze elementy dokumentu
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "typeId").SetExtension("POCD_HD000040").SetRoot("2.16.840.1.113883.1.3").Build());
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot(hl7Szablon.Id).SetExtension(hl7Szablon.Wersja).Build());
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement).SetExtension(this.IdentyfikatorDokumentu.Instancja).SetRoot(this.IdentyfikatorDokumentu.IdentyfikatorInstancji.Root).SetDisplayable(true).Build());

				var xmlCode = xmlElement.DodajElement("code");
				{
					xmlCode.UstawAtrybut("code", "51852-2");
					xmlCode.UstawAtrybut("codeSystem", "2.16.840.1.113883.6.1");
					xmlCode.UstawAtrybut("codeSystemName", "LOINC");
					xmlCode.UstawAtrybut("displayName", "Letter");

					var xmlTranslation = xmlCode.DodajElement("translation");
					{
						xmlTranslation.UstawAtrybut("code", "08.90");
						xmlTranslation.UstawAtrybut("codeSystem", "2.16.840.1.113883.3.4424.11.1.32");
						xmlTranslation.UstawAtrybut("codeSystemName", "KLAS_DOK_P1");
						xmlTranslation.UstawAtrybut("displayName", "Informacja dla lekarza kierującego/POZ");
					}
				}
				xmlElement.DodajElement(xmlElement, "title", "Informacja dla lekarza POZ");

				if (this.Wystawiono.HasValue)
					xmlElement.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());

				xmlElement.DodajElement("confidentialityCode").UstawAtrybut("code", "N").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.25");
				xmlElement.DodajElement("languageCode").UstawAtrybut("code", "pl-PL");
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "setId").SetExtension(this.IdentyfikatorDokumentu.ZbiorWersji).SetRoot(this.IdentyfikatorDokumentu.IdentyfikatorZbioruWersji.Root).SetDisplayable(false).Build());
				xmlElement.DodajElement("versionNumber").UstawAtrybut("value", this.IdentyfikatorDokumentu.Wersja.ToString());

				xmlElement.DodajElement(szablon1_pacjent);
				xmlElement.DodajElement(szablon2_autor);
				xmlElement.DodajElement(szablon3_organizacja);
				if (szablon4_odbiorca != null) xmlElement.DodajElement(szablon4_odbiorca);
				xmlElement.DodajElement(szablon5_wystawca);
				xmlElement.DodajElement(this.szablon6_ubezpieczyciel);
				if (szablon7_zlecenie != null) xmlElement.DodajElement(szablon7_zlecenie);
				if (szablon8_usluga != null) xmlElement.DodajElement(szablon8_usluga);
				if (szablon9_powiazanie != null) xmlElement.DodajElement(szablon9_powiazanie);
				if (szablon10_wizyta != null) xmlElement.DodajElement(szablon10_wizyta);
				//if (szablon11_tresc != null) xmlElement.DodajElement(szablon11_tresc);
				xmlElement.DodajElement(szablon11_tresc);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXML", ex);
			}

			return this.xmlDocument;
		}

	}
}
