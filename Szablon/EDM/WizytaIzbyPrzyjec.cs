﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.69
	/// </summary>
	[Szablon("[2] Dane wizyty w izbie przyjęć", "2.16.840.1.113883.3.4424.13.10.2.69", "plCdaRefusalNoteComponentOf", WersjaSzablonu.v1_3, "componentOf")]
	internal class DaneWizytyWIzbiePrzyjęć : SzablonSkladowy
	{
		internal DateTime? Wystawiono { get; set; }

		private SzablonSkladowy szablon1_osoba;	//2.16.840.1.113883.3.4424.13.10.2.49	Ograniczanie	active [2] Osoba przypisana (1.3)	DYNAMICZNE
		private SzablonSkladowy szablon2_miejsce;	//2.16.840.1.113883.3.4424.13.10.2.75	Ograniczanie	active [4] Miejsce (1.3)	DYNAMICZNE
		private OrganizacjaBazowy szablon3_organizacja;	//2.16.840.1.113883.3.4424.13.10.2.2	Ograniczanie	active [2] Organizacja (bazowy) (1.3)
		/// <summary>
		/// Konstruktor
		/// </summary>
		public DaneWizytyWIzbiePrzyjęć()
		{
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.UstawAtrybut("typeCode", "COMP");
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("2.16.840.1.113883.3.4424.13.10.2.69").Build());
				var xmlEncounter = xmlElement.DodajElement("encompassingEncounter");
				{
					xmlEncounter.UstawAtrybut("classCode", "ENC");
					xmlEncounter.UstawAtrybut("moodCode", "EVN");
					//hl7:id - krotność 0 … *
					var xmlCode = xmlEncounter.DodajElement("code");
					{
						xmlCode.UstawAtrybut("code", "4900");
						xmlCode.UstawAtrybut("codeSystem", "2.16.840.1.113883.3.4424.11.2.4");
						xmlCode.UstawAtrybut("displayName", "Izba przyjęć szpitala");
					}
					xmlEncounter.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
