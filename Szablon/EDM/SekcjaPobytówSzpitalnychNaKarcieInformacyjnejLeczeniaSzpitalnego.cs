﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.56
	/// </summary>
	[Szablon("[3] Sekcja pobytów szpitalnych na karcie informacyjnej leczenia szpitalnego", "2.16.840.1.113883.3.4424.13.10.3.56", "plCdaDischargeSummaryEncountersSection", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaPobytówSzpitalnychNaKarcieInformacyjnejLeczeniaSzpitalnego : SzablonSkladowy
	{
		public DanePobytuSzpitalnegoNaWypisie SzablonDanePobytuNaWypisie { get; set; } //1 … *
		public List<PobytNaOddziale> ListaPobytow { get; set; }

		public SekcjaPobytówSzpitalnychNaKarcieInformacyjnejLeczeniaSzpitalnego()
		{
			this.SzablonDanePobytuNaWypisie = new DanePobytuSzpitalnegoNaWypisie();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<SekcjaPobytówSzpitalnychNaKarcieInformacyjnejLeczeniaSzpitalnego>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.3.4424.13.10.3.56");

				var xmlText = xmlElement.DodajElement("text");

				var listElement = xmlText.DodajElement("list");

				listElement.DodajElement("caption", "Pobyty na oddziałach:");

				//dla kazdego pobytu na oddziale w ramach pobytu szpitalnego dodaje element listy
				int numer = 1;
				foreach (var pobyt in ListaPobytow)
				{
					pobyt.Referencja = String.Concat("ENC_", numer.ToString());
					listElement.DodajElement("item", String.Concat(pobyt.PoczatekPobytu.ToShortDateString(), " ", pobyt.KoniecPobytu.Value.ToShortDateString(), " ", pobyt.KomorkaOrganizacyjna.Nazwa)).SetAttribute("ID", pobyt.Referencja);
					numer++;
				}

				foreach (var pobyt in ListaPobytow)
				{
					//dla kazdego pobytu na liscie musze dodac element entry
					this.SzablonDanePobytuNaWypisie.pobyt = pobyt;

					xmlElement.DodajElement("entry").DodajElement(this.SzablonDanePobytuNaWypisie);
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}

	/// <summary>
	/// Kontener na dane o pobycie oddziałowym
	/// </summary>
	public class PobytNaOddziale
	{
		internal OID.KomorkaOrganizacyjna KomorkaOrganizacyjna { get; set; }
		internal DateTime PoczatekPobytu { get; set; }
		internal DateTime? KoniecPobytu { get; set; }
		internal string Referencja { get; set; }

		public PobytNaOddziale(OID.KomorkaOrganizacyjna komorkaOrganizacyjna, DateTime odKiedy, DateTime? doKiedy)
		{
			this.KomorkaOrganizacyjna = komorkaOrganizacyjna;
			this.PoczatekPobytu = odKiedy;
			this.KoniecPobytu = doKiedy;
		}
		public PobytNaOddziale(int komorkiOrgMedinfId, DateTime odKiedy, DateTime? doKiedy)
		{
			MedInf.KomorkaOrganizacyjna komOrg = MedInf.KomorkaOrganizacyjna.Pobierz(komorkiOrgMedinfId);
			if (komOrg != null)
			{
				this.KomorkaOrganizacyjna = new OID.KomorkaOrganizacyjna();
				this.KomorkaOrganizacyjna.Nazwa = komOrg.Nazwa;
				this.KomorkaOrganizacyjna.KodResortowy1 = komOrg.KodResortowy1;
				this.KomorkaOrganizacyjna.KodResortowy7 = komOrg.KodResortowy7;
			}
			this.PoczatekPobytu = odKiedy;
			this.KoniecPobytu = doKiedy;
		}

	}
}
