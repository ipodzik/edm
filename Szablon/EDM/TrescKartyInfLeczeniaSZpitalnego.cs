﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.60
	/// </summary>
	[Szablon("[2] Treść karty informacyjnej leczenia szpitalnego", "2.16.840.1.113883.3.4424.13.10.2.60", "plCdaDischargeSummaryNoteComponent", WersjaSzablonu.v1_3, "component")]
	internal class TrescKartyInfLeczeniaSzpitalnego : SzablonSkladowy
	{
		internal SekcjaPostaciBinarnejDokumentuKartyIinformacyjnejLeczeniaSzpitalnego szablonSekcjaPostaciBinarnejDokumentu;  // 0 … 1
		internal SekcjaPobytówSzpitalnychNaKarcieInformacyjnejLeczeniaSzpitalnego SzablonSekcjaPobytow { get; private set; } //1 … 1
		internal SzablonSekcjaRozpoznan SekcjaRozpoznan { get; private set; } // 1 … 1
		internal SekcjaWynikówBadań SzablonSekcjaWynikowBadan { get; private set; } //0 … 1
		internal SekcjaZastosowanegoLeczenia SzablonSekcjaZastosowanegoLeczenia { get; private set; } //0 … 1
		internal SekcjaEpikryzy SzablonSekcjaEpikryzy { get; private set; } // 1 … 1
		internal SekcjaZalecen SzablonSekcjaZalecen { get; private set; } // 1 … 1
		// TODO: internal SekcjaZałączników szablonSekcjaZałącznikow; //0 … 1


		public TrescKartyInfLeczeniaSzpitalnego()
		{
			this.SzablonSekcjaPobytow = new SekcjaPobytówSzpitalnychNaKarcieInformacyjnejLeczeniaSzpitalnego();
			this.SekcjaRozpoznan = new SzablonSekcjaRozpoznan();
			this.SzablonSekcjaWynikowBadan = new SekcjaWynikówBadań();
			this.SzablonSekcjaZastosowanegoLeczenia = new SekcjaZastosowanegoLeczenia();
			this.SzablonSekcjaEpikryzy = new SekcjaEpikryzy();
			this.SzablonSekcjaZalecen = new SekcjaZalecen();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<TrescKartyInfLeczeniaSzpitalnego>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			try
			{
				xmlElement.SetAttribute("contextConductionInd", "true");
				xmlElement.DodajIdentyfikatorSzablonu(this);

				var xmlBody = xmlElement.DodajElement("structuredBody");

				xmlBody.SetAttribute("classCode", "DOCBODY");
				xmlBody.SetAttribute("moodCode", "EVN");

				//            Elementy do wyboru z:
				//hl7:structuredBody[hl7:component/hl7:section/hl7:templateId/@root='2.16.840.1.113883.3.4424.13.10.3.55']
				//hl7:structuredBody[not(hl7:component/hl7:section/hl7:templateId/@root='2.16.840.1.113883.3.4424.13.10.3.55')]

				//Jeżeli w dokumencie znajduje się sekcja postaci binarnej dokumentu, to nie może występować sekcja innego typu.
				if(this.szablonSekcjaPostaciBinarnejDokumentu != null)
				{
					//xmlBody.DodajElement("component").DodajElement(this.szablonSekcjaPostaciBinarnejDokumentu);
				}
				else
				{
					xmlBody.DodajElement("component").DodajElement(this.SzablonSekcjaPobytow);

					xmlBody.DodajElement("component").DodajElement(this.SekcjaRozpoznan);

					if(this.SzablonSekcjaWynikowBadan != null)
					{
						xmlBody.DodajElement("component").DodajElement(this.SzablonSekcjaWynikowBadan);
					}

					if(this.SzablonSekcjaZastosowanegoLeczenia != null)
					{
						xmlBody.DodajElement("component").DodajElement(this.SzablonSekcjaZastosowanegoLeczenia);
					}					

					xmlBody.DodajElement("component").DodajElement(this.SzablonSekcjaEpikryzy);

					xmlBody.DodajElement("component").DodajElement(this.SzablonSekcjaZalecen);

					//if(this.szablonSekcjaZałączników != null)
					//{
					//   xmlBody.DodajElement("component").DodajElement(this.szablonSekcjaZałączników);
					//}
				}

			}
			catch(Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
