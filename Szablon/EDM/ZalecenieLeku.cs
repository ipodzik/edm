﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.22
	/// </summary>
	[Szablon("[4] Zalecenie leku", "2.16.840.1.113883.3.4424.13.10.4.22", "plCdaSubstanceAdministrationRequest", WersjaSzablonu.v1_3, "entry")]
	public class ZalecenieLeku : SzablonSkladowy
	{
		internal Slownik.Lek Lek { get; set; }
		private LekGotowy SzablonLekGotowy;

		public ZalecenieLeku()
		{
			this.SzablonLekGotowy = new LekGotowy();

			this.Lek = new Slownik.Lek();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				var substanceAdministration = xmlElement.DodajElement("substanceAdministration").UstawAtrybut("classCode", "SBADM").UstawAtrybut("moodCode", "RQO");
				substanceAdministration.DodajElement(new OpisWyrazeniaKlinicznego(Lek.ReferencjaId));

				var consumable = substanceAdministration.DodajElement("consumable");

				this.SzablonLekGotowy.Lek = this.Lek;
				consumable.DodajElement(this.SzablonLekGotowy);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
