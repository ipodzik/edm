﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.32
	/// </summary>
	[Szablon("[3] Sekcja zaleceń", "2.16.840.1.113883.3.4424.13.10.3.32", "plCdaPrescriptionsSection", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaZalecen : SzablonSkladowy
	{
		/// <summary>
		/// Zawiera zalecone leki, badania, procedury lub inne zalecenia
		/// </summary>
		internal Zalecenie Zalecenie;

		private SzablonSkladowy szablon1_danePodmiotuDotFragmentuTresci;// 2.16.840.1.113883.3.4424.13.10.4.15	[4] Dane podmiotu związanego z fragmentem treści dokumentu (1.3)	0 … 1
		private SzablonSkladowy szablon2_daneAutoraFragmentuTresci; //2.16.840.1.113883.3.4424.13.10.4.18	[4] Autor fragmentu treści dokumentu (1.3)	0 … *
		private SzablonSkladowy szablon3_daneInformatoraDotFragmentuTresci; //2.16.840.1.113883.3.4424.13.10.4.14	[4] Dane informatora dla fragmentu treści dokumentu (1.3)	0 … *
		//szablon4 - do wyboru 1 z 3
		private ZalecenieLeku szablon4_ZalecenieLeku; //2.16.840.1.113883.3.4424.13.10.4.22	[4] Zalecenie leku (1.3)	0 … *
		private ZalecenieWykonaniaProcedury szablon4_ZalecenieWykonaniaProcedury; //2.16.840.1.113883.3.4424.13.10.4.23	[4] Zalecenie wykonania procedury (1.3)	0 … *
		private ZalecenieWykonaniaBadania szablon4_ZalecenieWykonaniaBadania; //2.16.840.1.113883.3.4424.13.10.4.24	[4] Zalecenie wykonania badania (1.3)	0 … *


		public SekcjaZalecen()
		{
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			if(this.Zalecenie == null)
				throw new Exception("W sekcji zaleceń brak zaleceń");

			var xmlElement = parent.TworzElementXml(this);
			try
			{

				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("57828-6").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("Prescriptions/Prescription list").Build());

				xmlElement.DodajElement("title", this.Zalecenie.Naglowek);

				//+ hl7:text
				{
					XmlElement xmlText = xmlElement.DodajElement("text");
					XmlElement xmlList = xmlText.DodajElement("list");

					int nr = 1;

					//sprawdzam czy zalecono jakies leki, badania lub procedury 

					if (this.Zalecenie.Leki != null)
					{
						foreach (var lek in this.Zalecenie.Leki)
						{
							var listItem = xmlList.DodajElement("item", lek.Nazwa);
							listItem.SetAttribute("ID", String.Concat("SBADM_", nr.ToString()));
							lek.ReferencjaId = String.Concat("SBADM_", nr.ToString());
							nr++;
						}
					}

					if (this.Zalecenie.Badania != null)
					{
						foreach (var badanie in this.Zalecenie.Badania)
						{
							var listItem = xmlList.DodajElement("item", badanie.MetodaBadań);
							listItem.SetAttribute("ID", String.Concat("SBADM_", nr.ToString()));
							badanie.ReferencjaId = String.Concat("SBADM_", nr.ToString());
							nr++;
						}
					}

					if (this.Zalecenie.Procedury != null)
					{
						foreach (var procedura in this.Zalecenie.Procedury)
						{
							var listItem = xmlList.DodajElement("item", procedura.Nazwa);
							listItem.SetAttribute("ID", String.Concat("SBADM_", nr.ToString()));
							procedura.ReferencjaId = String.Concat("SBADM_", nr.ToString());
							nr++;
						}
					}

					if (this.Zalecenie.Inne != null)
					{
						foreach (var zalecenie in this.Zalecenie.Inne)
						{
							var listItem = xmlList.DodajElement("item", zalecenie);
							nr++;
						}
					}
				}

				//+ hl7:entry

				{
					//jesli zostały zalecone jakies leki, badania lub procedury to dodaje odpowiednie szablony

					if (this.Zalecenie.Leki != null)
					{
						szablon4_ZalecenieLeku = new ZalecenieLeku();
						foreach (var lek in this.Zalecenie.Leki)
						{
							szablon4_ZalecenieLeku.Lek = lek;
							xmlElement.DodajElement(szablon4_ZalecenieLeku); //entry dodaje sie samo
						}
					}



					if (this.Zalecenie.Badania != null)
					{
						szablon4_ZalecenieWykonaniaBadania = new ZalecenieWykonaniaBadania();
						foreach (var badanie in this.Zalecenie.Badania)
						{
							szablon4_ZalecenieWykonaniaBadania.ZaleconeBadanie = badanie;
							xmlElement.DodajElement("entry").DodajElement(szablon4_ZalecenieWykonaniaBadania);
						}
					}


					szablon4_ZalecenieWykonaniaProcedury = new ZalecenieWykonaniaProcedury();
					if (this.Zalecenie.Procedury != null)
					{
						szablon4_ZalecenieWykonaniaProcedury = new ZalecenieWykonaniaProcedury();
						foreach (var procedura in this.Zalecenie.Procedury)
						{
							szablon4_ZalecenieWykonaniaProcedury.Procedura = procedura;
							xmlElement.DodajElement("entry").DodajElement(szablon4_ZalecenieWykonaniaProcedury);
						}
					}

				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}
			return xmlElement;
		}
	}

	public class Zalecenie
	{
		public string Naglowek { get; set; }
		public List<Slownik.Lek> Leki { get; set; }
		public List<string> Inne { get; set; }
		public List<Badanie> Badania { get; set; }
		public List<Procedura> Procedury { get; set; }

		//public Zalecenie(string TytułZalecenia, List<Slownik.Lek> ZaleconeLeki, List<string> InneZalecenia)
		//{
		//   this.Naglowek = TytułZalecenia;
		//   this.Leki = ZaleconeLeki;
		//   this.Inne = InneZalecenia;
		//}
	}
}
