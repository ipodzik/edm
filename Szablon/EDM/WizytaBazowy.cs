﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.52
	/// </summary>
	[Szablon(" [2] Dane wizyty (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.52", "plCdaBaseComponentOf", WersjaSzablonu.v1_3, "componentOf")]
	internal class DaneWizytyBazowy : SzablonSkladowy
	{
		/// <summary>
		/// Data wystawienia dokumentu
		/// </summary>
		internal DateTime? Wystawiono { get; set; }

		/// <summary>
		/// Konstruktor
		/// </summary>
		public DaneWizytyBazowy()
		{
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.UstawAtrybut("typeCode", "COMP");
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("2.16.840.1.113883.3.4424.13.10.2.52").Build());
				var xmlEncounter = xmlElement.DodajElement("encompassingEncounter");
				{
					xmlEncounter.UstawAtrybut("classCode", "ENC");
					xmlEncounter.UstawAtrybut("moodCode", "EVN");
					//hl7:id - krotność 0 … *
					//hl7:code - krotność 0 … *
					xmlEncounter.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());
					//hl7:dischargeDispositionCode - nie wymagane
					//hl7:responsibleParty
					//hl7:encounterParticipant
					//hl7:location
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
