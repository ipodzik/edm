﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.87
	/// </summary>
	[Szablon(" [2] Treść dokumentu informacji dla lekarza kierującego/POZ", "2.16.840.1.113883.3.4424.13.10.2.87", "plCdaReferringPhysicianLetterComponent", WersjaSzablonu.v1_3, "component")]
	internal class TrescInformacjiDlaLekarza : SzablonSkladowy
	{
		//do wyboru szablon1_SekcjaPostaciBinarnej lub pozostałe 
		private SekcjaPostaciBinarnejDokumentuInformacjiDlaLekarza szablon1_SekcjaPostaciBinarnej; // 1 … 1
		internal SzablonSekcjaRozpoznan szablon2_sekcjaRozpoznan;  //  1 … 1
		internal SekcjaWywiaduLekarskiego szablon3_sekcjaWywiadu;  //  1 … 1
		private SekcjaZalecen szablon4_sekcjaZalecen;  //wymagany, krotność 1 … *
		private SzablonSkladowy szablon5_sekcjaZalacznikow;  // TODO: szablon 2.16.840.1.113883.3.4424.13.10.3.39	Sekcja załączników (1.3) 0 … 1

		internal List<Zalecenie> Zalecenia;

		/// <summary>
		/// Konstruktor
		/// </summary>
		public TrescInformacjiDlaLekarza()
		{
			//this.szablon1_SekcjaPostaciBinarnej = new SekcjaPostaciBinarnejDokumentuInformacjiDlaLekarza(); nieobowiazkowa
			this.szablon2_sekcjaRozpoznan = new SzablonSekcjaRozpoznan();
			this.szablon3_sekcjaWywiadu = new SekcjaWywiaduLekarskiego();
			this.szablon4_sekcjaZalecen = new SekcjaZalecen();
			//this.szablon5_sekcjaZalacznikow = new(); niewymagane
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.UstawAtrybut("typeCode", "COMP");
				//xmlElement.UstawAtrybut("contextConductionInd", "");

				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("2.16.840.1.113883.3.4424.13.10.2.87").Build());
				//xmlElement.DodajElement("structuredBody").DodajElement("component").DodajElement(this.szablon1_SekcjaPostaciBinarnej);

				var xmlBody = xmlElement.DodajElement("structuredBody");

				xmlBody.SetAttribute("classCode", "DOCBODY");
				xmlBody.SetAttribute("moodCode", "EVN");

				// Jeżeli w dokumencie znajduje się sekcja postaci binarnej dokumentu, to nie może występować sekcja innego typu.
				if(szablon1_SekcjaPostaciBinarnej != null)
				{
					xmlBody.DodajElement("component").DodajElement(this.szablon1_SekcjaPostaciBinarnej);
				}
				else
				{
					xmlBody.DodajElement("component").DodajElement(this.szablon2_sekcjaRozpoznan);
					xmlBody.DodajElement("component").DodajElement(this.szablon3_sekcjaWywiadu);

					// szablon sekcja zalecen może wystąpić kilka razy
					if(this.Zalecenia != null)
					{
						foreach(var item in this.Zalecenia)
						{
							this.szablon4_sekcjaZalecen.Zalecenie = item;
							xmlBody.DodajElement("component").DodajElement(this.szablon4_sekcjaZalecen);
						}
					}
				}

			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}


			return xmlElement;
		}
	}
}
