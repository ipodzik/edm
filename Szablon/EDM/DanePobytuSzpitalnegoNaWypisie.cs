﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;
using BIUINF.Lib4.HL7CDA.OID;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.50
	/// </summary>
	[Szablon("[4] Dane pobytu szpitalnego na wypisie", "2.16.840.1.113883.3.4424.13.10.4.50", "plCdaDischargeSummaryEncounterEntry", WersjaSzablonu.v1_3, "encounter")]
	public class DanePobytuSzpitalnegoNaWypisie : IElementSzablonu
	{
		public PobytNaOddziale pobyt;


		public DanePobytuSzpitalnegoNaWypisie()
		{

		}

		public XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<DanePobytuSzpitalnegoNaWypisie>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			try
			{

				xmlElement.SetAttribute("classCode", "DOCBODY");
				xmlElement.SetAttribute("moodCode", "EVN");

				xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.3.4424.13.10.4.50");

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode(this.pobyt.KomorkaOrganizacyjna.KodResortowy8).SetCodeSystem("2.16.840.1.113883.3.4424.11.2.4").SetDisplayName(this.pobyt.KomorkaOrganizacyjna.Nazwa).Build());


				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(pobyt.Referencja));

				var effectiveTimeElement = xmlElement.DodajElement("effectiveTime");

				var low = effectiveTimeElement.OwnerDocument.CreateElement("low").UstawAtrybut("value", this.pobyt.PoczatekPobytu.ToString("yyyyMMdd"));

				effectiveTimeElement.AppendChild(low);

				if (this.pobyt.KoniecPobytu.HasValue)
				{
					var high = effectiveTimeElement.OwnerDocument.CreateElement("high").UstawAtrybut("value", this.pobyt.KoniecPobytu.Value.ToString("yyyyMMdd"));
					effectiveTimeElement.AppendChild(high);
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}
			return xmlElement;
		}

	}
}
