﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.35
	/// </summary>
	[Szablon("[3] Sekcja wyników badań", "2.16.840.1.113883.3.4424.13.10.3.35", "plCdaRelevantDiagnosticTestsSection", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaWynikówBadań : SzablonSkladowy
	{
		/// <summary>
		/// NazwaBadania - co to jest ? czy nie powinna być w ramach listy ?
		/// </summary>
		/// <remarks>
		///Wiem że zrobione jak w przykładzie ale to coś nie styka
		///W podglądzie wyników z laboratorium analitycznego Lis mamy tabelkę z kolumnami:
		/// metoda badań/materiał/data,godz wyniku/nazwa/wynik/jednostka/wart. referencyjna/flaga/opis wyniku
		/// Porównując z przykładem: 
		/// "Morfologia krwi obwodowej" to nie jest nazwa badania, to nazwa grupy badań - u nas to metoda badań
		/// można to pokazywać właśnie jako grupę, ale musi być możliwość przekazania wielu grup
		/// 
		/// </remarks>

		internal List<Badanie> WynikiZrealizowanychBadań;

		public SekcjaWynikówBadań()
		{
			//this.WynikiBadań = new List<WynikiBadania>();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("30954-2").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("Relevant diagnostic tests/laboratory data").Build());

				xmlElement.DodajElement("title", "Wyniki badań");

				XmlElement xmlText = xmlElement.DodajElement("text");

				if(this.WynikiZrealizowanychBadań != null)
				{
					XmlElement xmlTable = xmlText.DodajElement("table");
					XmlElement xmlThead = xmlTable.DodajElement("thead");
					XmlElement xmlTrHead = xmlThead.DodajElement("tr");
					xmlTrHead.DodajElement("th", "Nazwa badania");
					xmlTrHead.DodajElement("th", "Wynik badania");
					xmlTrHead.DodajElement("th", "Zakres referencyjny");

					int numer = 1;

					foreach(Badanie zrealizowaneBadanie in this.WynikiZrealizowanychBadań)
					{

						XmlElement xmlTBody = xmlTable.DodajElement("tbody");

						XmlElement xmlTrBody = xmlTBody.DodajElement("tr");
						xmlTrBody.SetAttribute("ID", String.Concat("OBS_", numer.ToString()));
						XmlElement xmlTd = xmlTrBody.DodajElement("td", zrealizowaneBadanie.MetodaBadań);
						xmlTd.SetAttribute("colspan", "3");
						xmlTd.SetAttribute("styleCode", "Bold");

						numer++;
						//wyswietlam kazdy element wyniku badania 						
						foreach(WynikBadania wynik in zrealizowaneBadanie.WynikiBadania)
						{
							var element = xmlTBody.DodajElement("tr");
							element.SetAttribute("ID", String.Concat("OBS_", numer.ToString()));
							element.DodajElement("td", wynik.Nazwa);
							element.DodajElement("td", wynik.Wynik + " " + wynik.Jednostka);
							element.DodajElement("td", wynik.WartoscReferencyjna);
							numer++;
						}
					}
				}




			}
			catch(Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}

	/// <summary>
	/// Klasa zawierająca opis wyników konkretnego badania
	/// </summary>
	public class WynikBadania
	{
		public string Nazwa { get; set; }
		public string Wynik { get; set; }
		public string Jednostka { get; set; }
		public string WartoscReferencyjna { get; set; }

		public WynikBadania(string nazwa, string wynik, string jednostka, string wartReferencyjna)
		{
			this.Nazwa = nazwa;
			this.Wynik = wynik;
			this.Jednostka = jednostka;
			this.WartoscReferencyjna = wartReferencyjna;
		}
	}

	///// <summary>
	///// badanie zrealizowane w ramach jednego zlecenia
	///// </summary>
	public class Badanie
	{
		public string MetodaBadań { get; set; }

		/// <summary>
		/// kod badania wg ICD-9
		/// </summary>
		public string Kod { get; set; }

		/// <summary>
		/// id referencji do badania w bloku tekstowym
		/// </summary>
		public string ReferencjaId { get; set; }

		/// <summary>
		/// lista poszczegółnych składowych wyniku w ramach jednej metody badań
		/// </summary>
		public List<WynikBadania> WynikiBadania { get; set; }

		/// <summary>
		/// Konstruktor do tworzenia badania w szablonie Sekcja wyników badań
		/// </summary>
		/// <param name="metodaBadań"></param>
		/// <param name="wynikiBadania"></param>
		public Badanie(string metodaBadań, List<WynikBadania> wynikiBadania)
		{
			MetodaBadań = metodaBadań;
			WynikiBadania = wynikiBadania;
		}


		/// <summary>
		/// konstruktor do tworzenia badania w szablonie Zalecenie wykonania badania
		/// </summary>
		/// <param name="Kod"></param>
		/// <param name="Nazwa"></param>
		public Badanie(string Kod, string Nazwa)
		{
			this.Kod = Kod;
			this.MetodaBadań = Nazwa;
		}
	}
}
