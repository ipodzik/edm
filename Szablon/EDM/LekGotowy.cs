﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.21
	/// </summary>
	[Szablon("[4] Lek gotowy", "2.16.840.1.113883.3.4424.13.10.4.21", "plCdaManufacturedLabeledDrug", WersjaSzablonu.v1_3_1, "manufacturedProduct")]
	internal class LekGotowy : SzablonSkladowy
	{
		internal Slownik.Lek Lek { get; set; }

		public LekGotowy()
		{
			this.Lek = new Slownik.Lek();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);
				var xmlManufacturedLabeledDrug = xmlElement.DodajElement("manufacturedLabeledDrug");

				xmlManufacturedLabeledDrug.DodajElement(Lek);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
