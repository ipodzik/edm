﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.67
	/// </summary>
	[Szablon("[2] Treść karty odmowy przyjęcia do szpitala", "2.16.840.1.113883.3.4424.13.10.2.67", "	plCdaRefusalNoteComponent", WersjaSzablonu.v1_3, "component")]
	internal class TrescKartyOdmowyPrzyjeciaDoSzpitala : SzablonSkladowy
	{
		//do wyboru SekcjaPostaciBinarnejDokumentuKartyIinformacyjnejLeczeniaSzpitalnego lub pozostałe
		internal SekcjaPostaciBinarnejDokumentuKartyIinformacyjnejLeczeniaSzpitalnego SzablonSekcjaPostaciBinarnej { get; set; }

		internal SzablonSekcjaRozpoznan SzablonSekcjaRozpoznan { get; set; } //1 … 1

		internal SekcjaWywiaduLekarskiego SzablonSekcjaWywiaduLekarskiego { get; set; } // 1 … 1

		internal SekcjaBadaniaPrzedmiotowego SzablonSekcjaBadaniaPrzedmiotowego { get; set; } // 1 … 1

		internal SekcjaWynikówBadań SzablonSekcjaWynikowBadan { get; set; } // 0 … 1

		internal SekcjaZastosowanegoLeczenia SzablonSekcjaZastosowanegoLeczenia { get; set; } // 0 … 1

		internal SekcjaPowoduOdmowyPrzyjeciaDoSzpitala SzablonSekcjaPowoduOdmowyPrzyjeciaDoSzpitala { get; set; } // 1 … 1

		internal SekcjaZalecen SzablonSekcjaZalecen { get; set; } // 1 … 1

		// TODO: internal SekcjaZałączników Szablon SekcjaZałączników ;  0 … 1
				

		/// <summary>
		/// Konstruktor
		/// </summary>
		public TrescKartyOdmowyPrzyjeciaDoSzpitala()
		{
			this.SzablonSekcjaRozpoznan = new SzablonSekcjaRozpoznan();
			this.SzablonSekcjaWywiaduLekarskiego = new SekcjaWywiaduLekarskiego();
			this.SzablonSekcjaBadaniaPrzedmiotowego = new SekcjaBadaniaPrzedmiotowego();
			this.SzablonSekcjaWynikowBadan = new SekcjaWynikówBadań();
			this.SzablonSekcjaZastosowanegoLeczenia = new SekcjaZastosowanegoLeczenia();
			this.SzablonSekcjaPowoduOdmowyPrzyjeciaDoSzpitala = new SekcjaPowoduOdmowyPrzyjeciaDoSzpitala();
			this.SzablonSekcjaZalecen = new SekcjaZalecen();
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.UstawAtrybut("typeCode", "COMP");
				//xmlElement.UstawAtrybut("contextConductionInd", "");

				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("2.16.840.1.113883.3.4424.13.10.2.67").Build());

				var structuredBody = xmlElement.DodajElement("structuredBody");

				if(SzablonSekcjaPostaciBinarnej != null)
				{
					//structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaPostaciBinarnej);
				}
				else
				{
					structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaRozpoznan);//wymagane

					structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaWywiaduLekarskiego);//wymagane

					structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaBadaniaPrzedmiotowego);//wymagane

					if(this.SzablonSekcjaWynikowBadan.WynikiZrealizowanychBadań.Count > 0)
					{
						structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaWynikowBadan); //niewymagana
					}

					if(this.SzablonSekcjaZastosowanegoLeczenia.ZastosowaneLeczenie != null)
					{
						structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaZastosowanegoLeczenia); //niewymagana
					}

					if(this.SzablonSekcjaPowoduOdmowyPrzyjeciaDoSzpitala.PowodOdmowyPrzyjeciaDoSzpitala != null)
					{
						structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaPowoduOdmowyPrzyjeciaDoSzpitala); //niewymagana
					}


					structuredBody.DodajElement("component").DodajElement(this.SzablonSekcjaZalecen); //wymagana
				}

				
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
