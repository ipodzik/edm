﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.30
	/// </summary>
	[Szablon("[3] Sekcja wywiadu lekarskiego", "2.16.840.1.113883.3.4424.13.10.3.30", "plCdaHistorySection", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaWywiaduLekarskiego : SzablonSkladowy
	{
		/// <summary>
		/// Nagłówek sekcji; Domyślnie: "Wywiad lekarski"
		/// </summary>
		internal string NaglowekSekcji { get; set; }

		/// <summary>
		/// Treść sekcji - lista tekstów akapitów które wstawiane są do oddzielnych paragrafów
		/// </summary>
		internal List<string> TrescSekcji { get; set; }

		public SekcjaWywiaduLekarskiego()
		{
			this.NaglowekSekcji = "Wywiad lekarski";
			this.TrescSekcji = new List<string>();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("10164-2").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("History of present illness").Build());
				xmlElement.DodajElement("title", this.NaglowekSekcji);

				XmlElement xmlText = xmlElement.DodajElement("text");
				foreach (var item in TrescSekcji)
				{
					xmlText.DodajElement("paragraph", item);	
				}
				

			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
