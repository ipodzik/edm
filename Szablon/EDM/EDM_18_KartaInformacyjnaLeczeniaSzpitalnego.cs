﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.1.3
	/// </summary>
	[Szablon("[1] Karta informacyjna leczenia szpitalnego", "2.16.840.1.113883.3.4424.13.10.1.18", "plCdaDischargeSummary", WersjaSzablonu.v1_3_1, "ClinicalDocument")]
	public class EDM_18_KartaInformacyjnaLeczeniaSzpitalnego : EDM
	{

		/// <summary>
		/// Data wystawienia dokumentu
		/// </summary>		
		public new DateTime? Wystawiono
		{
			get { return base.Wystawiono; }
			set { base.Wystawiono = value; }
		}

		/// <summary>
		/// Numer księgi głównej pobytu szpitalnego - prawdop. nr w księdze/nr księgi/rok
		/// </summary>
		public string NrKsiegiGlownej
		{
			set
			{
				this.szablon10_DaneHospitalizacji.NrKsiegiGlownej = value;
			}
		}

		/// <summary>
		/// Tryb wypisu ze szpitala
		/// </summary>
		public TrybWypisuZeSzpitala TrybWypisu 
		{ 
			set
			{
				this.szablon10_DaneHospitalizacji.TrybWypisu = value;
			}

		}

		/// <summary>
		/// lista pobytów pacjenta na oddziałach w ramach jednego pobytu szpitalnego
		/// - jako ostatni musi być oddział z którego następuje wypis
		/// </summary>
		public List<PobytNaOddziale> ListaPobytów
		{
			set
			{
				this.szablon10_DaneHospitalizacji.ListaPobytow = value;
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SzablonSekcjaPobytow.ListaPobytow = value;
			}
		}


		public Slownik.ICD10 RozpoznanieGlowne
		{
			set
			{
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SekcjaRozpoznan.SzablonRozpoznanieGlowne.RozpoznanieICD10 = value;
			}
		}

		public List<Slownik.ICD10> RozpoznaniaDodatkowe
		{
			set
			{
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SekcjaRozpoznan.ListaRozpoznanDodatkowych = value;
			}
		}

		/// <summary>
		/// Lista wyników wszystkich badań zrealizowanych w ramach jednego zlecenia
		/// </summary>
		public List<Badanie> WynikiZrealizowanychBadań
		{
			set
			{
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SzablonSekcjaWynikowBadan.WynikiZrealizowanychBadań = value;
			}
		}

		
		/// <summary>
		/// Opis zastosowanego leczenia
		/// </summary>
		public string ZastosowaneLeczenie
		{
			set
			{
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SzablonSekcjaZastosowanegoLeczenia.ZastosowaneLeczenie = value;
			}
		}

		public string TrescEpikryzy
		{
			set
			{
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SzablonSekcjaEpikryzy.TrescEpikryzy = value;
			}
		}

		/// <summary>
		/// Zawiera zalecone leki, badania, procedury lub inne zalecenia. Wyświetlane w sekcji zaleceń
		/// </summary>
		public Zalecenie Zalecenie 
		{
			set
			{
				this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego.SzablonSekcjaZalecen.Zalecenie = value;
			}
		}

		//szablony 1-9 dziedziczone z EDM
		private DaneHospitalizacji szablon10_DaneHospitalizacji; // 1 … 1
		private TrescKartyInfLeczeniaSzpitalnego szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego; // 1 … 1

		/// <summary>
		/// Konstruktor
		/// </summary>
		public EDM_18_KartaInformacyjnaLeczeniaSzpitalnego()
		{
			this.szablon10_DaneHospitalizacji = new DaneHospitalizacji();
			this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego = new TrescKartyInfLeczeniaSzpitalnego();
		}

		internal override XmlDocument TworzXml()
		{
			try
			{
				this.xmlDocument.AppendChild(this.xmlDocument.CreateXmlDeclaration("1.0", Encoding.UTF8.HeaderName, null));
				this.xmlDocument.AppendChild(this.xmlDocument.CreateProcessingInstruction("xml-stylesheet", "href=\"CDA_PL_IG_1.3.1.xsl\" type=\"text/xsl\""));

				var hl7Szablon = AttributeHelper<EDM_18_KartaInformacyjnaLeczeniaSzpitalnego>.GetCustomAttribute<SzablonAttribute>();
				var xmlElement = this.xmlDocument.CreateElement(hl7Szablon.XmlElementNazwa);
				this.xmlDocument.AppendChild(xmlElement);

				this.xmlDocument.DocumentElement.SetAttribute("xmlns", "urn:hl7-org:v3");
				this.xmlDocument.DocumentElement.SetAttribute("xmlns:extPL", "http://www.csioz.gov.pl/xsd/extPL/r2");
				this.xmlDocument.DocumentElement.SetAttribute("xmlns:pharm", "urn:ihe:pharm");

				this.xmlDocument.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
				this.xmlDocument.DocumentElement.SetAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", "extPL:ClinicalDocument");

				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "typeId").SetExtension("POCD_HD000040").SetRoot("2.16.840.1.113883.1.3").Build());

				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot(hl7Szablon.Id).SetExtension(hl7Szablon.Wersja).Build());
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement).SetExtension(this.IdentyfikatorDokumentu.Instancja).SetRoot(this.IdentyfikatorDokumentu.IdentyfikatorInstancji.Root).SetDisplayable(true).Build());

				xmlElement.DodajTypCE("18842-5", "2.16.840.1.113883.6.1", "LOINC", "Discharge summary",
					xmlElement.TworzTypCD("translation", "00.20", "2.16.840.1.113883.3.4424.11.1.32", "KLAS_DOK_P1", "Karta informacyjna z leczenia szpitalnego"));

				xmlElement.DodajElement("title", "Karta informacyjna z leczenia szpitalnego");

				if (base.Wystawiono.HasValue)
				{
					xmlElement.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());
				}

				xmlElement.DodajElement("confidentialityCode").UstawAtrybut("code", "N").UstawAtrybut("codeSystem", "2.16.840.1.113883.5.25");
				xmlElement.DodajElement("languageCode").UstawAtrybut("code", "pl-PL");

				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "setId").SetExtension(this.IdentyfikatorDokumentu.Instancja).SetRoot(ZbiorWartosci.ReceptaZbiorWersjiReceptIDokumentowAnulowania.GetKod()).Build());
				xmlElement.DodajElement("versionNumber").UstawAtrybut("value", "1");

				xmlElement.DodajElement(szablon1_pacjent);

				xmlElement.DodajElement(szablon2_autor);

				xmlElement.DodajElement(szablon3_organizacja);

				xmlElement.DodajElement(szablon5_wystawca);

				xmlElement.DodajElement(this.szablon10_DaneHospitalizacji);

				xmlElement.DodajElement(this.szablon11_TrescKartyInformacyjnejLeczeniaSzpitalnego);

			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXML",ex);

			}
			return this.xmlDocument;

		}

	}
}
