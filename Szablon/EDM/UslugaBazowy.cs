﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.51
	/// </summary>
	[Szablon("[2] Dane wykonanej usługi (bazowy)", "2.16.840.1.113883.3.4424.13.10.2.51", "plCdaBaseDocumentationOf", WersjaSzablonu.v1_3, "documentationOf")]
	public class UslugaBazowy : SzablonSkladowy
	{
		/// <summary>
		/// Data wystawienia dokumentu
		/// </summary>
		internal DateTime? Wystawiono { get; set; }

		/// <summary>
		/// Konstruktor
		/// </summary>
		public UslugaBazowy()
		{
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.UstawAtrybut("typeCode", "DOC");
				xmlElement.AppendChild(new InstanceIdentifierBuilder(xmlElement, "templateId").SetRoot("2.16.840.1.113883.3.4424.13.10.2.51").Build());
				var xmlServiceEvent = xmlElement.DodajElement("serviceEvent");
				{
					xmlServiceEvent.UstawAtrybut("classCode", "ACT");
					xmlServiceEvent.UstawAtrybut("moodCode", "EVN");
					//hl7:id - krotność 0 … *
					xmlServiceEvent.DodajElement("code").UstawAtrybut("codeSystem", "2.16.840.1.113883.3.4424.11.2.6");
					xmlServiceEvent.AppendChild(new TimeStampBuilder(xmlElement, "effectiveTime").SetValue(this.Wystawiono.Value).Build());
					//hl7:performer - krotność 0 … * - wewnątrz mozna modać wykonawcę, ale w sumie nie wymagane
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
