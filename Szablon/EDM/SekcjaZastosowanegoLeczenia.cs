﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.36
	/// </summary>
	[Szablon("[3] Sekcja rozpoznań", "2.16.840.1.113883.3.4424.13.10.3.36", "plCdaTreatmentSection	", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaZastosowanegoLeczenia : SzablonSkladowy
	{
		internal string ZastosowaneLeczenie { get; set; }

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<SekcjaZastosowanegoLeczenia>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.3.4424.13.10.3.36");

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("55753-8").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("Treatment information").Build());

				xmlElement.DodajElement("title", "Leczenie");

				XmlElement xmlText = xmlElement.DodajElement("text");
				xmlText.DodajElement("paragraph", this.ZastosowaneLeczenie);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
