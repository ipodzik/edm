﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.71
	/// </summary>
	[Szablon("[3] Sekcja postaci binarnej dokumentu informacji dla lekarza kierującego/POZ", "2.16.840.1.113883.3.4424.13.10.3.71", "	plCdaReferringPhysicianLetterBinaryContent", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaPostaciBinarnejDokumentuInformacjiDlaLekarza : SzablonSkladowy
	{
		// TODO: Uzupełnienie szablonu

		//public string TekstInformacjiDlaLekarza { get; set; }

		public Slownik.ICD10 RozpoznanieGlowneICD10 { get { return this.szablon1_rozpoznanieGlowne.RozpoznanieICD10; } set { this.szablon1_rozpoznanieGlowne.RozpoznanieICD10 = value; } }

		private RozpoznanieGlowne szablon1_rozpoznanieGlowne;	//	1 … 1
		private RozpoznaniaDodatkowe szablon2_rozpoznanieDodatkowe; // 0 … 1
		// TODO: private DokumentWPostaciBinarnej szablon3_dokument_w_postaci_binarnej //1 … *

		/// <summary>
		/// Konstruktor
		/// </summary>
		public SekcjaPostaciBinarnejDokumentuInformacjiDlaLekarza()
		{
			this.szablon1_rozpoznanieGlowne = new RozpoznanieGlowne();
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);
				xmlElement.DodajElement("code").UstawAtrybut("code", "51852-2").UstawAtrybut("codeSystem", "2.16.840.1.113883.6.1").UstawAtrybut("codeSystemName", "LOINC").UstawAtrybut("displayName", "Letter");
				xmlElement.DodajElement(xmlElement, "title", "Informacja dla lekarza POZ");

				var xmlText = xmlElement.DodajElement("text");
				var xmlRenderMultiMedia = xmlText.DodajElement("renderMultiMedia");
				xmlRenderMultiMedia.SetAttribute("referencedObject", "DOCUMENT_CONTENT");
				xmlText.DodajElement(this.szablon1_rozpoznanieGlowne);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}
			return xmlElement;
		}
	}
}
