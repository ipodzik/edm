﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.2
	/// </summary>
	[Szablon("[4] Rozpoznania dodatkowe", "2.16.840.1.113883.3.4424.13.10.4.2", "plCdaSecondaryDiagnosis", WersjaSzablonu.v1_3_1, "entry")]
	internal class RozpoznaniaDodatkowe : SzablonSkladowy
	{
		/// <summary>
		/// Rozpoznanie wg klasyfikacji ICD-10
		/// </summary>
		public Slownik.ICD10 RozpoznanieICD10 { get; set; }

		public RozpoznaniaDodatkowe()
		{
			this.RozpoznanieICD10 = new Slownik.ICD10();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				var xmlOrganizer = xmlElement.DodajElement("organizer").UstawAtrybut("classCode", "BATTERY").UstawAtrybut("moodCode", "EVN");
				//code
				var xmlStatusCode = xmlOrganizer.DodajElement("statusCode").UstawAtrybut("code", "completed");
				var xmlComponent = xmlOrganizer.DodajElement("component");
				var xmlObservation = xmlComponent.DodajElement("observation").UstawAtrybut("classCode", "OBS").UstawAtrybut("moodCode", "EVN");
				xmlObservation.DodajElement(RozpoznanieICD10);
				xmlObservation.DodajElement(new OpisWyrazeniaKlinicznego(this.RozpoznanieICD10.ReferencjaId));
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}
			return xmlElement;
		}
	}
}
