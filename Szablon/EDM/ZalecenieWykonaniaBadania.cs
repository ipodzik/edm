﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.24
	/// </summary>
	[Szablon("[4] Zalecenie wykonania badania", "2.16.840.1.113883.3.4424.13.10.4.24", "plCdaObservationRequest", WersjaSzablonu.v1_3, "observation")]
	internal class ZalecenieWykonaniaBadania : SzablonSkladowy
	{
		internal Badanie ZaleconeBadanie;

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.SetAttribute("classCode", "OBS");
				xmlElement.SetAttribute("moodCode", "RQO");

				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode(ZaleconeBadanie.Kod).SetCodeSystem("2.16.840.1.113883.3.4424.11.2.6").SetCodeSystemName("ICD-9-PL").SetDisplayName(ZaleconeBadanie.MetodaBadań).Build());

				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(ZaleconeBadanie.ReferencjaId));
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
	
}
