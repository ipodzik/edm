﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using BIUINF.Lib4.HL7CDA.Slownik;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 	2.16.840.1.113883.3.4424.13.10.2.66
	/// </summary>
	[Szablon("[2] Dane hospitalizacji", "2.16.840.1.113883.3.4424.13.10.2.66", "plCdaDischargeSummaryNoteComponentOf", WersjaSzablonu.v1_3, "componentOf")]
	internal class DaneHospitalizacji : IElementSzablonu
	{
		/// <summary>
		/// Numer księgi głównej
		/// </summary>
		public string NrKsiegiGlownej { get; set; }

		/// <summary>
		/// Lista pobytów oddziałowych w ramach pobytu szpitalnego
		/// - jako ostatni musi być oddział z którego następuje wypis
		/// </summary>
		public List<PobytNaOddziale> ListaPobytow { get; set; } 

		//TODO: do weryfikacji
		private string KodZbioruWartosciNrKsiegiGlownej = ZbiorWartosci.SystemUslugodawcyMedInfHospitalizacjeNrKsiegiGlownej.GetKod();

		/// <summary>
		/// Tryb wypisu ze szpitala
		/// </summary>
		public TrybWypisuZeSzpitala TrybWypisu { get; set; }

		private SzablonSkladowy szablon1_osoba;	//2.16.840.1.113883.3.4424.13.10.2.49	Ograniczanie	active [2] Osoba przypisana (1.3)	DYNAMICZNE
		private SzablonSkladowy szablon2_miejsce;	//2.16.840.1.113883.3.4424.13.10.2.75	Ograniczanie	active [4] Miejsce (1.3)	DYNAMICZNE
		private OrganizacjaBazowy szablon3_organizacja;	//2.16.840.1.113883.3.4424.13.10.2.2	Ograniczanie	active [2] Organizacja (bazowy) (1.3)

		public DaneHospitalizacji()
		{
		}

		public XmlElement TworzXmlElement(XmlElement parent)
		{
			XmlElement xmlElement = parent.OwnerDocument.CreateElement(AttributeHelper<DaneHospitalizacji>.GetCustomAttribute<SzablonAttribute>().XmlElementNazwa);
			try
			{

				xmlElement.DodajIdentyfikatorSzablonu("2.16.840.1.113883.3.4424.13.10.2.66");

				XmlElement encompassingEncounterElement = xmlElement.DodajElement("encompassingEncounter").UstawAtrybut("classCode", "ENC").UstawAtrybut("moodCode", "EVN");

				//element id (identyfikator hospitalizacji) ktorym jest numer wpisu pacjenta do księgi głównej przyjęć i wypisów. baza hospitalizacje , tabela pobyty_ksiegi
				encompassingEncounterElement.AppendChild(new InstanceIdentifierBuilder(xmlElement).SetExtension(this.NrKsiegiGlownej).SetRoot(KodZbioruWartosciNrKsiegiGlownej).SetDisplayable(true).Build());

				//wyswietlam ostatni oddział na jakimbył pacjent!
				encompassingEncounterElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode(this.ListaPobytow.Last().KomorkaOrganizacyjna.KodResortowy8).SetCodeSystem("2.16.840.1.113883.3.4424.11.2.4").SetDisplayName(this.ListaPobytow.Last().KomorkaOrganizacyjna.Nazwa).Build());

				var effectiveTimeElement = encompassingEncounterElement.DodajElement("effectiveTime");

				var low = encompassingEncounterElement.OwnerDocument.CreateElement("low").UstawAtrybut("value", this.ListaPobytow.First().PoczatekPobytu.ToString("yyyyMMdd"));

				effectiveTimeElement.AppendChild(low);

				var high = encompassingEncounterElement.OwnerDocument.CreateElement("high").UstawAtrybut("value", this.ListaPobytow.Last().KoniecPobytu.Value.ToString("yyyyMMdd"));
				//var high = parent.TworzElementXml(this).UstawAtrybut("value", this.DataZwolnienia.Value.ToString("yyyyMMdd"));
				effectiveTimeElement.AppendChild(high);


				//encompassingEncounter.AppendChild(new TimeStampBuilder)
				//encompassingEncounter.AppendChild(new IVLTimeStampBuilder(encompassingEncounter, "effectiveTime").SetLow(this.DataWpisu).SetHigh(this.DataZwolnienia).Build());

				//encompassingEncounterElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "dischargeDispositionCode")
				//  .SetCode(this.TrybWypisuId.ToString())
				//  .SetCodeSystem("2.16.840.1.113883.3.4424.11.3.21")
				//  .SetDisplayName(this.TrybWypisuNazwa).Build());
				if (this.TrybWypisu != null)
				{
					var builder = new CodedWithEquivalentsBuilder(xmlElement, "dischargeDispositionCode");
					builder.SetCode(this.TrybWypisu.GetKod());
					var systemKodowaniaTrybuWypisu = SystemKodowania.TrybWypisuZeSzpitala.GetOidEnumAttribute();
					builder.SetCodeSystem(systemKodowaniaTrybuWypisu.Kod);
					builder.SetDisplayName(this.TrybWypisu.GetNazwa());
					encompassingEncounterElement.AppendChild(builder.Build());
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}


	}
}
