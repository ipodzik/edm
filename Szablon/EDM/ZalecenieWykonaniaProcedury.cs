﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.4.23
	/// </summary>
	[Szablon("[4] Zalecenie wykonania procedury", "2.16.840.1.113883.3.4424.13.10.4.23", "plCdaProcedureRequest", WersjaSzablonu.v1_3, "procedure")]
	internal class ZalecenieWykonaniaProcedury : SzablonSkladowy
	{
		internal Procedura Procedura;

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.SetAttribute("classCode", "PROC");
				xmlElement.SetAttribute("moodCode", "RQO");

				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode(Procedura.Kod).SetCodeSystem("2.16.840.1.113883.3.4424.11.2.6").SetCodeSystemName("ICD-9-PL").SetDisplayName(Procedura.Nazwa).Build());

				xmlElement.DodajElement(new OpisWyrazeniaKlinicznego(Procedura.ReferencjaId));

			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}
			return xmlElement;
		}
	}

	public class Procedura
	{
		/// <summary>
		/// kod procedury wg ICD-9
		/// </summary>
		public string Kod { get; set; }

		public string Nazwa { get; set; }

		/// <summary>
		/// id referencji do procedury w bloku tekstowym
		/// </summary>
		public string ReferencjaId { get; set; }

		public Procedura(string Kod, string Nazwa)
		{
			this.Kod = Kod;
			this.Nazwa = Nazwa;
		}
	}
}
