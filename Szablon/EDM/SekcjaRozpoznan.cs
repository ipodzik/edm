﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.1
	/// </summary>
	[Szablon("[3] Sekcja rozpoznań", "2.16.840.1.113883.3.4424.13.10.3.1", "plCdaDiagnosisSection", WersjaSzablonu.v1_3, "section")]
	internal class SzablonSekcjaRozpoznan : SzablonSkladowy
	{
		internal RozpoznanieGlowne SzablonRozpoznanieGlowne; //1 … 1
		internal List<Slownik.ICD10> ListaRozpoznanDodatkowych;

		public SzablonSekcjaRozpoznan()
		{
			this.SzablonRozpoznanieGlowne = new RozpoznanieGlowne();
			this.ListaRozpoznanDodatkowych = new List<Slownik.ICD10>();
		}

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.AppendChild(new CodedWithEquivalentsBuilder(xmlElement, "code").SetCode("29548-5").SetCodeSystem("2.16.840.1.113883.6.1").SetCodeSystemName("LOINC").SetDisplayName("Diagnosis").Build());

				xmlElement.DodajElement("title", "Rozpoznania");

				//+ hl7:text
				{
					XmlElement xmlText = xmlElement.DodajElement("text");
					XmlElement xmlTable = xmlText.DodajElement("table"); // kody icd10
					XmlElement xmlTbody = xmlTable.DodajElement("tbody");


					//dodaje rozpoznanie glowne i jesli są to rozpoznania dodatkowe
					XmlElement xmlTrGlowne = xmlTbody.DodajElement("tr");
					xmlTrGlowne.SetAttribute("ID", "DIAG_1");
					xmlTrGlowne.DodajElement("td", this.SzablonRozpoznanieGlowne.RozpoznanieICD10.Opis);
					xmlTrGlowne.DodajElement("td", this.SzablonRozpoznanieGlowne.RozpoznanieICD10.Kod);
					this.SzablonRozpoznanieGlowne.ReferencjaId = "DIAG_1";

					if(this.ListaRozpoznanDodatkowych.Count > 0)
					{
						int nrRozpoznania = 2;
						foreach(var rozpoznanieDodatkowe in this.ListaRozpoznanDodatkowych)
						{
							XmlElement xmlTrDodatkowe = xmlTbody.DodajElement("tr");
							xmlTrDodatkowe.SetAttribute("ID", String.Concat("DIAG_", nrRozpoznania.ToString()));
							xmlTrDodatkowe.DodajElement("td", rozpoznanieDodatkowe.Opis);
							xmlTrDodatkowe.DodajElement("td", rozpoznanieDodatkowe.Kod);
							rozpoznanieDodatkowe.ReferencjaId = String.Concat("DIAG_", nrRozpoznania.ToString());
							nrRozpoznania++;
						}
					}
				}

				//+ hl7:Entry
				{
					XmlElement xmlRozpoznanieGlowne = xmlElement.DodajElement(this.SzablonRozpoznanieGlowne); //element entry dodaje się sam w szablonie rozpoznania glownego

					RozpoznaniaDodatkowe szablonRozpoznaniaDodatkowe = new RozpoznaniaDodatkowe();
					if(this.ListaRozpoznanDodatkowych.Count > 0)
					{
						foreach(var rozpoznanieDodatkowe in this.ListaRozpoznanDodatkowych)
						{
							szablonRozpoznaniaDodatkowe.RozpoznanieICD10 = rozpoznanieDodatkowe;
							XmlElement xmlRozpoznaniaDodatkowe = xmlElement.DodajElement(szablonRozpoznaniaDodatkowe);
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}


}
