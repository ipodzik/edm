﻿using System;
using System.Collections.Generic;
using System.Xml;
using BIUINF.Lib4.HL7CDA.OID;
using System.Xml.Schema;
using System.Windows;
using System.IO;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// Abstrakcyjna klasa dokumentów medycznych - szablonów 1-szego poziomu (Szablony bezpośrednio powiązane z transakcją)
	/// </summary>
	public abstract class EDM
	{
		/// <summary>
		/// Unikalny identyfikator instancji dokumentu medycznego, który zawiera Identyfikator instancji dokumentu oraz Identyfikator zbioru wersji dokumentu
		/// </summary>
		public IdentyfikatorDokumentu IdentyfikatorDokumentu { get; protected set; }

		/// <summary>
		/// Data wystawienia dokumentu
		/// </summary>
		public DateTime? Wystawiono
		{
			get { return this.wystawiono; }
			set { this.wystawiono = szablon2_autor.UtworzenieDokumentu = szablon5_wystawca.Wystawiono = szablon8_usluga.Wystawiono = value; }
		}

		/// <summary>
		/// Identyfikator pacjenta w systemie MedInf
		/// </summary>
		public int MedinfPacjentId
		{
			set
			{
				this.szablon1_pacjent.PobierzDane(value);
				this.szablon6_ubezpieczyciel.Platnik.Kod = this.szablon1_pacjent.KodOddzialuNFZ;
			}
		}


		/// <summary>
		/// Identyfikator autora w systemie MedInf
		/// </summary>
		public int MedinfAutorId
		{
			set
			{
				szablon2_autor.PobierzDane(value,true);
				szablon5_wystawca.Identyfikator = this.szablon2_autor.Identyfikator;
			}
		}

		/// <summary>
		/// Identyfikator jednostki organizacyjnej autora w systemie MedInf
		/// </summary>
		public int MedinfAutorJednostkaOrganizacyjnaId
		{
			set
			{
				szablon2_autor.JednostkaOrganizacyjna.PobierzDane(value);
			}
		}

		protected readonly XmlDocument xmlDocument = new XmlDocument();

		private DateTime? wystawiono;
		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.3 [2] Dane pacjenta (bazowy)
		/// </summary>
		protected DanePacjentaBazowy szablon1_pacjent;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.4 [2] Autor dokumentu (bazowy)
		/// </summary>
		protected AutorDokumentuBazowy szablon2_autor;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.5 [2] Organizacja odpowiedzialna za dokument (bazowy) 
		/// </summary>
		protected OrganizacjaOdpowiedzialnaZaDokumentBazowy szablon3_organizacja;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.61 [2] Odbiorca informacji (bazowy) - Krotność 0 … * - nie wymagane
		/// </summary>
		protected SzablonSkladowy szablon4_odbiorca;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.6 [2] Wystawca dokumentu (bazowy) 
		/// </summary>
		protected WystawcaDokumentuBazowy szablon5_wystawca;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.19 [2] Dane ubezpieczyciela publicznego (bazowy) 
		/// </summary>
		protected DaneUbezpieczycielaPublicznegoBazowy szablon6_ubezpieczyciel;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.53 [2] Dane zlecenia (bazowy) (Krotność 0 … * - nie wymagane)
		/// </summary>
		protected SzablonSkladowy szablon7_zlecenie;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.51 [2] Dane wykonanej usługi (bazowy) (DYNAMICZNE)
		/// </summary>
		protected UslugaBazowy szablon8_usluga;

		/// <summary>
		/// 2.16.840.1.113883.3.4424.13.10.2.7 [2] Dokument powiązany (bazowy) (Krotność 0 … * - nie wymagane)
		/// </summary>
		/// 

		protected SzablonSkladowy szablon9_powiazanie;

		/// <summary>
		/// Lista błędów walidacji pliku xml
		/// </summary>
		private static List<string> ValidationErrors;

		/// <summary>
		/// Konstruktor
		/// </summary>
		public EDM()
		{
			this.IdentyfikatorDokumentu = new IdentyfikatorDokumentu(ZbiorWartosci.SystemUslugodawcyEDMIdentyfikatorInstancji, true, ZbiorWartosci.SystemUslugodawcyEDMZbiorWersji);

			szablon1_pacjent = new DanePacjentaBazowy();
			szablon2_autor = new AutorDokumentuBazowy();
			szablon2_autor.JednostkaOrganizacyjna = new BIUINF.Lib4.HL7CDA.Szablon.JednostkaPodmiotuWykonujacegoDzialanoscLecznicza();
			szablon3_organizacja = new OrganizacjaOdpowiedzialnaZaDokumentBazowy();
			//szablon4_powiazanie=new();
			szablon5_wystawca = new WystawcaDokumentuBazowy();
			szablon6_ubezpieczyciel = new DaneUbezpieczycielaPublicznegoBazowy();
			//szablon7_powiazanie=new();
			szablon8_usluga = new UslugaBazowy();
			//szablon9_powiazanie=new();
			ValidationErrors = new List<string>();
		}

		/// <summary>
		/// Metoda tworząca xml wg szablonu
		/// </summary>
		/// <returns></returns>
		internal virtual XmlDocument TworzXml()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Metoda tworzy dokument wg szablonu i go pokazuje
		/// </summary>
		/// <returns></returns>
		public bool Utworz()
		{
			bool czyOK = false;
			XmlDocument myDoc;
			try
			{
				myDoc = this.TworzXml();
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w tworzeniu dokumentu hl7", MergeInnerExceptions(ex));
			}

			//Waliduj(myDoc);
			Presentation.PrezentacjaWindow win = null;
			try
			{
				win = new Presentation.PrezentacjaWindow(myDoc, ValidationErrors);
				if (win.ShowDialog() ?? false)
				{
					czyOK = true;
				}
			}
			catch (Exception ex)
			{
				Library.AppCore.ExceptionHelper.Show(ex);
				if (win != null)
					win.Close();
				return false;
			}
			return czyOK;
		}

		private Exception MergeInnerExceptions(Exception ex)
		{
			List<Exception> lista = new List<Exception>();
			while (ex != null)
			{
				lista.Add(ex);
				ex = ex.InnerException;
			}
			string opis = "";
			for (int i = lista.Count-1; i >= 0; i--)
			{
				ex = lista[i];
				opis += ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine; 
			}
			return new Exception(opis);
		}


		private void Waliduj(XmlDocument doc)
		{


			#region nie działa

			//Assembly myAssembly = Assembly.GetCallingAssembly();
			//Stream myStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.extPL_r2.xsd");
			//string childXSD = new StreamReader(myStream).ReadToEnd();
			//var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));

			//var resourceNames = myAssembly.GetManifestResourceNames().Where(name => name.StartsWith(@"BIUINF.Lib4.HL7CDA.Schemas"));

			//XmlSchemaSet schemas = new XmlSchemaSet();

			// Iterate through all the resources and add only the xsd's
			//foreach(var name in resourceNames)
			//{
			//   using(Stream schemaStream = myAssembly.GetManifestResourceStream(name))
			//   {
			//      using(XmlReader schemaReader = XmlReader.Create(schemaStream))
			//      {
			//         schemas.Add("urn:hl7-org:v3", schemaReader);
			//      }
			//   }
			//}




			//var schemaSet = new XmlSchemaSet { XmlResolver = new ResourceXmlResolver() };
			//schemaSet.Add(null, xmlReader);

			//schemaSet.Compile();

			//var settings = new XmlReaderSettings { ValidationType = ValidationType.Schema, Schemas = schemas };

			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.xmldsig-core-schema.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("http://www.w3.org/2000/09/xmldsig#", xmlReader);
			//}

			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.infrastructureRoot.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}
			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.NarrativeBlock.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}
			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.voc.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}

			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.datatypes-base.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}

			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.datatypes.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}
			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.COCT_MT440001UV_ext_pharmacy.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:ihe:pharm", xmlReader);
			//}
			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.COCT_MT230100UV_ext_pharmacy.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:ihe:pharm", xmlReader);
			//}
			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.POCD_MT000040_extended_pharmacy.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}
			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.CDA_extended_pharmacy.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("urn:hl7-org:v3", xmlReader);
			//}




			//using(Stream schemaStream = myAssembly.GetManifestResourceStream(@"BIUINF.Lib4.HL7CDA.Schemas.extPL_r2.xsd"))
			//{
			//   string childXSD = new StreamReader(schemaStream).ReadToEnd();
			//   var xmlReader = XmlReader.Create(new MemoryStream(Encoding.Default.GetBytes(childXSD)));
			//   //schemaCollection.Add(null, xmlReader);

			//   //XmlSchema schema = XmlSchema.Read(schemaStream, null);
			//   settings.Schemas.Add("http://www.csioz.gov.pl/xsd/extPL/r2", xmlReader);
			//}


			//Wczytuje wszystkie pliki 
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.ValidationType = ValidationType.Schema;
			settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

			Uri extPL_r2_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/extPL_r2.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo extPL_r2_File = Application.GetResourceStream(extPL_r2_URI);
			settings.Schemas.Add("http://www.csioz.gov.pl/xsd/extPL/r2", XmlReader.Create(extPL_r2_File.Stream));

			Uri CDA_extended_pharmacy_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/CDA_extended_pharmacy.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo CDA_extended_pharmacy_File = Application.GetResourceStream(CDA_extended_pharmacy_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(CDA_extended_pharmacy_File.Stream));

			Uri xmldsig_core_schema_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/xmldsig-core-schema.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo xmldsig_core_schema_File = Application.GetResourceStream(xmldsig_core_schema_URI);
			settings.Schemas.Add("http://www.w3.org/2000/09/xmldsig#", XmlReader.Create(xmldsig_core_schema_File.Stream));

			Uri POCD_MT000040_extended_pharmacy_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/POCD_MT000040_extended_pharmacy.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo POCD_MT000040_extended_pharmacy_File = Application.GetResourceStream(POCD_MT000040_extended_pharmacy_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(POCD_MT000040_extended_pharmacy_File.Stream));

			Uri COCT_MT230100UV_ext_pharmacy_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/COCT_MT230100UV_ext_pharmacy.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo COCT_MT230100UV_ext_pharmacy_File = Application.GetResourceStream(COCT_MT230100UV_ext_pharmacy_URI);
			settings.Schemas.Add("urn:ihe:pharm", XmlReader.Create(COCT_MT230100UV_ext_pharmacy_File.Stream));

			Uri COCT_MT440001UV_ext_pharmacy_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/COCT_MT440001UV_ext_pharmacy.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo COCT_MT440001UV_ext_pharmacy_File = Application.GetResourceStream(COCT_MT440001UV_ext_pharmacy_URI);
			settings.Schemas.Add("urn:ihe:pharm", XmlReader.Create(COCT_MT440001UV_ext_pharmacy_File.Stream));

			////coreschemas
			Uri datatypes_base_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/coreschemas/datatypes-base.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo datatypes_base_File = Application.GetResourceStream(datatypes_base_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(datatypes_base_File.Stream));

			Uri datatypes_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/coreschemas/datatypes.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo datatypes_File = Application.GetResourceStream(datatypes_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(datatypes_File.Stream));

			Uri infrastructureRoot_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/coreschemas/infrastructureRoot.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo infrastructureRoot_File = Application.GetResourceStream(infrastructureRoot_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(infrastructureRoot_File.Stream));

			Uri NarrativeBlock_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/coreschemas/NarrativeBlock.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo NarrativeBlock_File = Application.GetResourceStream(NarrativeBlock_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(NarrativeBlock_File.Stream));

			Uri voc_URI = new Uri(@"BIUINF.Lib4.HL7CDA;component/Schemas/coreschemas/voc.xsd", UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo voc_File = Application.GetResourceStream(voc_URI);
			settings.Schemas.Add("urn:hl7-org:v3", XmlReader.Create(voc_File.Stream));




			//// Assembly object to find the embedded resources
			//Assembly myAssembly = Assembly.GetExecutingAssembly();

			//// Get all the xsd resoruces
			//var resourceNames = myAssembly.GetManifestResourceNames().Where(name => name.StartsWith(@"BIUINF.Lib4.HL7CDA.Schemas"));


			//try
			//{
			//   // Load the XML
			//   XDocument xmlDoc = XDocument.Load(@"C:\Users\Monika\Documents\KartaInformacyjnaDlaLekarza.xml");

			//   // Create new schema set
			//   XmlSchemaSet schemas = new XmlSchemaSet();

			//   // Iterate through all the resources and add only the xsd's
			//   foreach(var name in resourceNames)
			//   {
			//      using(Stream schemaStream = myAssembly.GetManifestResourceStream(name))
			//      {
			//         using(XmlReader schemaReader = XmlReader.Create(schemaStream))
			//         {
			//            schemas.Add(null, schemaReader);
			//         }
			//      }
			//   }

			//   // Call to the validate method
			//   xmlDoc.Validate(schemas, (o, e) => { this.ErrorMessage = string.Format("File failure: There was an error validating the XML document: {0} ", e.Message); }, true);


			//}
			//catch(Exception ex)
			//{

			//   this.ErrorMessage = string.Format("File failure: There was an error validating the XML document: {0}", ex.ToString());

			//}

			#endregion


			

			//var buildDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
			//var filePath = buildDir + @"\Schemas\extPL_r2.xsd";

			//settings.Schemas.Add(null, XmlReader.Create(filePath));

			try
			{
				XmlReader reader = XmlReader.Create(new XmlNodeReader(doc), settings);
				while(reader.Read())	;
			}
			catch (Exception ex)
			{
				throw new Exception("Błąd w trakcie walidacji", ex);
			}

		}


		private static void ValidationCallBack(object sender, ValidationEventArgs e)
		{
			ValidationErrors.Add(e.Message);
		}

		
	}

	//public class ResourceXmlResolver : XmlResolver
	//{
	//   /// <summary>
	//   /// When overridden in a derived class, maps a URI to an object containing the actual resource.
	//   /// </summary>
	//   /// <returns>
	//   /// A System.IO.Stream object or null if a type other than stream is specified.
	//   /// </returns>
	//   /// <param name="absoluteUri">The URI returned from <see cref="M:System.Xml.XmlResolver.ResolveUri(System.Uri,System.String)"/>. </param><param name="role">The current version does not use this parameter when resolving URIs. This is provided for future extensibility purposes. For example, this can be mapped to the xlink:role and used as an implementation specific argument in other scenarios. </param><param name="ofObjectToReturn">The type of object to return. The current version only returns System.IO.Stream objects. </param><exception cref="T:System.Xml.XmlException"><paramref name="ofObjectToReturn"/> is not a Stream type. </exception><exception cref="T:System.UriFormatException">The specified URI is not an absolute URI. </exception><exception cref="T:System.ArgumentNullException"><paramref name="absoluteUri"/> is null. </exception><exception cref="T:System.Exception">There is a runtime error (for example, an interrupted server connection). </exception>
	//   public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
	//   {
	//      // If ofObjectToReturn is null, then any of the following types can be returned for correct processing:
	//      // Stream, TextReader, XmlReader or descendants of XmlSchema
	//      var result = this.GetType().Assembly.GetManifestResourceStream(string.Format("BIUINF.Lib4.HL7CDA.{0}",
	//                                                                                   Path.GetFileName(absoluteUri.ToString())));
	//      // set a conditional breakpoint "result==null" here
	//      return result;
	//   }

	//   /// <summary>
	//   /// When overridden in a derived class, sets the credentials used to authenticate Web requests.
	//   /// </summary>
	//   /// <returns>
	//   /// An <see cref="T:System.Net.ICredentials"/> object. If this property is not set, the value defaults to null; that is, the XmlResolver has no user credentials.
	//   /// </returns>
	//   public override ICredentials Credentials
	//   {
	//      set { throw new NotImplementedException(); }
	//   }
	//}
}
