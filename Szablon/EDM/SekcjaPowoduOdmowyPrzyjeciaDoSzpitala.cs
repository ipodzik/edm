﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Xml;

namespace BIUINF.Lib4.HL7CDA.Szablon.EDM
{
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.3.40
	/// </summary>
	[Szablon("[3]  Sekcja powodu odmowy przyjęcia do szpitala", "2.16.840.1.113883.3.4424.13.10.3.40", "plCdaRefusalReasonSection", WersjaSzablonu.v1_3, "section")]
	internal class SekcjaPowoduOdmowyPrzyjeciaDoSzpitala : SzablonSkladowy
	{
		public string PowodOdmowyPrzyjeciaDoSzpitala { get; set; }

		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);
			try
			{
				xmlElement.DodajIdentyfikatorSzablonu(this);

				xmlElement.DodajElement("title", "Powód odmowy przyjęcia");

				XmlElement xmlText = xmlElement.DodajElement("text");

				XmlElement xmlParagraph = xmlText.DodajElement("paragraph", this.PowodOdmowyPrzyjeciaDoSzpitala);
			}
			catch (Exception ex)
			{
				throw new Exception("Nierozpoznany błąd w TworzXMLElement", ex);
			}

			return xmlElement;
		}
	}
}
