﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using BIUINF.Lib4.HL7CDA.Slownik;




namespace BIUINF.Lib4.HL7CDA.Szablon
{
	[HL7XmlName("asOrganizationPartOf")]
	public class CzescOrganizacji : IElementSzablonu
	{
		public PraktykaZawodowa PraktykaZawodowa
		{
			get { return this.praktykaZawodowa; }
			set { this.praktykaZawodowa = value; }
		}
		private PraktykaZawodowa praktykaZawodowa;

		public PelnaOrganizacja PelnaOrganizacja
		{
			get { return this.pelnaOrganizacja; }
			set { this.pelnaOrganizacja = value; }
		}
		private PelnaOrganizacja pelnaOrganizacja;



		public CzescOrganizacji()
		{
			//this.PraktykaZawodowa = new PraktykaZawodowa();
			this.PelnaOrganizacja = new PelnaOrganizacja();
		}

		public CzescOrganizacji(KomorkaPodmiotuWykonujacegoDzialalnoscLeczniczaBazowy komorkaOrganizacyjna)
		{
			this.PelnaOrganizacja = new PelnaOrganizacja(komorkaOrganizacyjna);
		}



		public XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.DodajElement(this.PraktykaZawodowa);
			xmlElement.DodajElement(this.PelnaOrganizacja);

			return xmlElement;
		}
	}
}
