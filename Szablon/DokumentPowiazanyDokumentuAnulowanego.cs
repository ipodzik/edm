﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;



namespace BIUINF.Lib4.HL7CDA.Szablon
{
	/*
				https://www.csioz.gov.pl/HL7POL-1.3.1.2/plcda-1.3.1.2/plcda-html-1.3.1.2/plcda-html-1.3.1.2/tmp-2.16.840.1.113883.3.4424.13.10.2.46-2018-06-30T000000.html 
	*/
	/// <summary>
	/// 2.16.840.1.113883.3.4424.13.10.2.46
	/// </summary>
	[Szablon("[2] Dokument powiązany dokumentu anulującego", "2.16.840.1.113883.3.4424.13.10.2.46", "plCdaNullificationRelatedDocument", WersjaSzablonu.v1_3, "relatedDocument")]
	public class DokumentPowiazanyDokumentuAnulowanego : SzablonSkladowy
	{
		internal IdentyfikatorDokumentu Identyfikator { get { return this.identyfikator; } }
		private readonly IdentyfikatorDokumentu identyfikator;



		public DokumentPowiazanyDokumentuAnulowanego(IdentyfikatorDokumentu dokumentDoAnulowaniaIdentyfikator)
		{
			this.identyfikator = dokumentDoAnulowaniaIdentyfikator;
		}



		public override XmlElement TworzXmlElement(XmlElement parent)
		{
			var xmlElement = parent.TworzElementXml(this);

			xmlElement.UstawAtrybut("typeCode", "RPLC");

			xmlElement.DodajIdentyfikatorSzablonu(this);

			var xmlParentDocument = xmlElement.DodajElement("parentDocument");

			xmlParentDocument.DodajTypII(this.identyfikator.IdentyfikatorInstancji);
			xmlParentDocument.DodajTypII(this.identyfikator.IdentyfikatorZbioruWersji);
			xmlParentDocument.DodajElement("versionNumber").UstawAtrybut("value", this.identyfikator.Wersja.ToString());

			return xmlElement;
		}
	}
}
