﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows;
using System.Xml.Xsl;
using System.IO;

namespace BIUINF.Lib4.HL7CDA.Transformaty
{
	internal class Konwerter
	{
		internal static readonly string URI_INFORMACJA_O_RECEPTACH = @"BIUINF.Lib4.eRecepta;component/Transformaty/pl_informacja_o_receptach_1.3.1.xsl";
		internal static readonly string URI_RECEPTA = @"BIUINF.Lib4.eRecepta;component/Transformaty/CDA_PL_IG_1.3.1.xsl";
		internal static readonly string URI_EDM = @"BIUINF.Lib4.HL7CDA;component/Transformaty/CDA_PL_IG_1.3.1.xsl";

		/// <summary>
		/// Wykonuje transformację z użyciem XSLT i generuje treść HTML podglądu dokumentu
		/// </summary>
		/// <param name="eDok">dokument xml zawierający dokument do podgladu</param>
		/// <returns></returns>
		internal static string GenerujPodglad(XmlDocument eDok)
		{
			XslCompiledTransform xslt = WczytajTransformate(URI_EDM);
			eDok = NowyDokument(eDok);
			string trescHTML = "";

			using (MemoryStream ms = new MemoryStream())
			{
				// własne ustawienia Encodingu do prawidłowej Transformacji (oryginalnie tworzy UTF-16)
				XmlWriterSettings xws = xslt.OutputSettings.Clone();
				xws.Encoding = Encoding.UTF8;

				XmlWriter xw = XmlWriter.Create(ms, xws);
				StreamReader sd = new StreamReader(ms);
				xslt.Transform(eDok, xw);
				ms.Position = 0;
				trescHTML = sd.ReadToEnd();
				sd.Close();
				ms.Close();
			}

			return trescHTML;
		}

		private static XmlDocument NowyDokument(XmlDocument xmlDocument)
		{
			var doc = new XmlDocument();
			doc.LoadXml(xmlDocument.OuterXml);
			return doc;
		}

		private static XslCompiledTransform WczytajTransformate(string uriResourceXsl)
		{
			// Bierzemy transformatę z Resource  
			Uri uri = new Uri(uriResourceXsl, UriKind.Relative);
			System.Windows.Resources.StreamResourceInfo xslFile = Application.GetResourceStream(uri);
			XmlReader reader = XmlReader.Create(xslFile.Stream);

			XsltSettings settings = new XsltSettings(false, true);
			// Ładujemy z readera
			XslCompiledTransform xslt = new XslCompiledTransform();
			xslt.Load(reader, settings, new XmlUrlResolver());
			return xslt;
		}
	}
}
