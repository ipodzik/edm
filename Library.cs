﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIUINF.Lib4.Core;
using System.Data.Common;
using BIUINF.Lib4.Common;
using BIUINF.Lib4.Data;
using System.Diagnostics;
using System.Reflection;



namespace BIUINF.Lib4.HL7CDA
{
	/// <summary />
	public static class Library
	{
		private static string numerKontaP1Swiadczeniodawcy;

		/// <summary />
		public static ApplicationCore AppCore
		{
			internal get { return Library.appCore; }
			set { Library.appCore = value; }
		}
		private static ApplicationCore appCore;



		internal const string DbIdSlowniki          = "slowniki";
		internal const string DbIdKadry             = "kadry_2s";
		internal const string DbIdMedInfPacjenci    = "MedInf_pacjenci";
		internal const string DbIdSIoK              = "siok";
		internal const string DbIdStart							= "start";


		/// <summary />
		public static string NumerKontaP1Swiadczeniodawcy
		{
			get
			{
				if (string.IsNullOrEmpty(Library.numerKontaP1Swiadczeniodawcy))
				{
					int nr = 0;
					if (!Library.appCore.Parameters.FindValue("MedInf", "NumerKontaP1Swiadczeniodawcy", out nr))
					{
						throw new ExclamationException("Brak parametru 'NumerKontaP1Swiadczeniodawcy'!");
					}

					if (nr == 0)
					{
						throw new ExclamationException("Brak numeru konta świadczeniodawcy w systemie P1 w parametrach systemu!");
					}

					Library.numerKontaP1Swiadczeniodawcy = nr.ToString();
				}

				return Library.numerKontaP1Swiadczeniodawcy;
			}
		}



		internal static string CreateConnectionString(string databaseId)
		{
			return Library.GetDatabase(databaseId).ConnectionString;
		}

		/// <summary />
		public static DbConnection CreateOpenedConnection(string databaseId)
		{
			var conn = Library.GetDatabase(databaseId).Provider.CreateConnection();
			conn.ConnectionString = Library.CreateConnectionString(databaseId);

			conn.Open();

			return conn;
		}

		public static string GetInitialCatalog(string databaseId)
		{
			return Library.GetDatabase(databaseId).InitialCatalog;
		}

		private static DbConnectionBuilder GetDatabase(string databaseId)
		{
			if (String.IsNullOrEmpty(databaseId))
			{
				throw new ErrorException("Identyfikator bazy jest pusty!");
			}
			else if (!Library.appCore.Databases.Contains(databaseId))
			{
				throw new ErrorException(string.Format("Brak w kolekcji bazy o podanym identyfikatorze '{0}'!", databaseId));
			}
			else
			{
				return Library.appCore.Databases[databaseId];
			}
		}


		private static string assemblyVersion = null;
		public static string AssemblyVersion
		{
			get
			{
				if (string.IsNullOrEmpty(assemblyVersion))
				{
					Assembly assembly = Assembly.GetExecutingAssembly();
					FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

					assemblyVersion = fvi.FileVersion;
				}

				return assemblyVersion;
			}
		}
	}
}
